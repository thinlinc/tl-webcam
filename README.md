tl-webcam
=========

tl-webcam provides a virtual forwarding of local webcam on a linux client (only Ubuntu 22.04 tested) to a thinlinc server (vsmagent). The forwarding will work only if the client and the vsm-agent has a direct connection to each other (no firewall in between). This is a requirement of the WebRTC protocol. Currently using of STUN/TURN services is not implemented.

The webcam is captured at resolution 640x480, which can be modified in `src/constants/video.py` (this file must coincide on both client and server!). MJPEG format is preferred with a raw stream as the second option. This resolution has been picked, because the client encodes the video stream into h264 and our terminals cannot handle higher resolutions.

Video loopback devices on the server side are fed with video in raw format yuv420p / I420. The default image is saved `./src/images/splash-imath.yuv` (see `SPLASH_IMAGE` in `src/constants/thinagent.py`) - this is a raw image data.

Installation on vsmagent
------------------------

1) Install tl version of v4l-loopback kernel module.

```bash
wget https://git.math.uzh.ch/thinlinc/tl-webcam/-/raw/master/thinagent/install-tl-v4l-kernel-module.sh -O /tmp/install-tl-v4l-kernel-module.sh 
bash /tmp/install-tl-v4l-kernel-module.sh
```

2) Install tlwebcam service

```bash
wget https://git.math.uzh.ch/thinlinc/tl-webcam/-/raw/master/thinagent/install-tlwebcam-service.sh -O /tmp/install-tlwebcam-service.sh 
bash /tmp/install-tlwebcam-service.sh
```

On thinlinc client
-----------------

Install: 


Documentation
-------------

* Checkout `doc`
* Commands::

```bash
  $ tlwebcam-ctl list-cameras
  
   id  camera id     label
  ---  ------------  -------------
    1  d3bg4QLazf00  Video Capture 4
```
    
* `/var/log/tlwebcam.log`

    
