#!/bin/python3
"""
ThinLinc webcam
author: Krzysztof Putyra
date: 29/03/2023

A program to communicate with tlwsnder process
"""

from constants import VIDEO_CHANNEL
import commands as cmd
from app import CtlApp, Command, Argument
import app.formatter as formatter
from components.storage import ClientStorage
from constants import CLIENT_DATA_FILE

storage = ClientStorage(CLIENT_DATA_FILE)

app = CtlApp('tswebcam-ctl', "A controller for streaming from cameras on a ThinStation.")
app.forward_requests({
    "list-cameras": Command(cmd.LIST_CAMERAS,
        help="Prints available cameras",
        formatter=formatter.camera_list,
        onempty="No available camera"
    ),
    "list-formats": Command(cmd.LIST_FORMATS,
        help="Print supported formats of a camera",
        arguments=(
            Argument("camera-id", help="the id of the camera"),
        ),
        formatter=formatter.format_list,
        onempty="No compatible formats"
    ),
    "status": Command(cmd.STATUS,
        help="streaming status"
    ),
    "stop": Command(cmd.STOP,
        help="stops the current stream"
    ),
    "connect": Command(cmd.CONNECT,
        help="connects to a thinlinc session",
        arguments=(
            Argument("port", help="the port number forwarded to the session", type=int),
        ),
        preaction=storage.store
    ),
    "disconnect": Command(cmd.DISCONNECT,
        help="disconnects from a thinlinc session",
        arguments=(
            Argument("port", help="the port number forwarded to the session", type=int, default=0),
        ),
        preaction=storage.delete
    ),
    "unload": Command(cmd.UNLOAD,
        help="unloads the service",
        arguments=(
            Argument("exit-code", help="the exit code that should be returned by the service", type=int, default=0),
        )
    ),
    "reload": Command(cmd.RELOAD,
        help="reloads the service"
    )
}).to(VIDEO_CHANNEL)

exit(app.run())
