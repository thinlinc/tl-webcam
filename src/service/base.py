import asyncio
from typing import Optional
from logging import Logger, getLogger
from pyee.asyncio import AsyncIOEventEmitter

class Service(AsyncIOEventEmitter):

    STOPPED = 0
    LOADING = 1
    RUNNING = 2
    UNLOADING = 3

    EV_START = 'start'
    EV_STOP = 'stop'

    __service_id: int = 0

    @staticmethod
    def next_service_id() -> int:
        Service.__service_id += 1
        return Service.__service_id

    def __init__(
        self,
        *,
        name: Optional[str] = None,
        logger: Optional[Logger] = None
    ):
        super().__init__()
        self.id = self.next_service_id()
        self.name = name or self.__class__.__name__
        self.logger = logger or getLogger(name or f'service#{self.id}')
        self.__state = self.STOPPED
        self.__state_changed: Optional[asyncio.Event] = None

    @property
    def is_running(self) -> bool:
        return self.__state == self.RUNNING

    @property
    def state(self) -> int:
        return self.__state

    def __resolve_state(self, new_state: int):
        self.__state = new_state
        self.__state_changed.set()


    async def start(self):
        self.logger.debug(f'starting the service')
        if self.__state_changed is None:
            self.__state_changed = asyncio.Event()
        elif not self.__state_changed.is_set():
            # Wait for the transition to finish if in a transition state
            self.logger.debug(f'waiting for the status to be resolved')
            await self.__state_changed.wait()

        if self.__state == self.RUNNING:
            self.logger.debug(f'already running')
            return

        # self.__state == self.STOPPED
        try:
            self.__state = self.LOADING
            self.__state_changed.clear()
            await self.load()
            self.logger.info(f'the service has started successfully')
            self.__resolve_state(self.RUNNING)
            self.emit(self.EV_START)
        except Exception as err:
            self.__resolve_state(self.STOPPED)
            self.logger.error(f"the service has failed to start: {err!r}")


    async def stop(self):
        self.logger.debug(f'terminating the service')
        if self.__state_changed is None:
            self.logger.debug(f'the service was never started')
            return
        elif not self.__state_changed.is_set():
            # Wait for the transition to finish if in a transition state
            self.logger.debug(f'waiting for the status to be resolved')
            await self.__state_changed.wait()

        if self.__state == self.STOPPED:
            self.logger.debug(f'already stopped')
            return

        # self.__state == self.RUNNING
        try:
            self.__state = self.UNLOADING
            self.__state_changed.clear()
            await self.unload()
            self.logger.info(f'the service is terminated')
            self.__resolve_state(self.STOPPED)
            self.emit(self.EV_STOP)
        except Exception as err:
            self.__resolve_state(self.RUNNING)
            self.logger.error(f"the service has failed to stop: {err!r}")


    async def load(self):
        pass

    async def unload(self):
        pass

    async def __aenter__(self):
        await self.start()
        return self
    
    async def __aexit__(self, exc_type, exc_value, traceback):
        await self.stop()



class ActiveService(Service):
    """
    A service that runs a task in a background
    """

    def __init__(
        self,
        *,
        name: Optional[str] = None,
        logger: Optional[Logger] = None
    ):
        super().__init__(name=name, logger=logger)
        self._service_task: Optional[asyncio.Task] = None

    @property
    def is_running(self) -> bool:
        return bool(self._service_task) and not self._service_task.done()

    async def _run(self):
        try:
            self.logger.debug(f'starting the service task')
            await self.run()
        except asyncio.CancelledError:
            pass
        except Exception as err:
            self.logger.error(f'the service task has failed')
            self.logger.exception(err)
        finally:
            self.logger.debug(f'the service task has stopped')
            self._service_task = None
            if self.state == self.RUNNING:
                await self.stop()

    async def load(self):

        self.logger.debug(f'starting the background task')
        self._service_task = asyncio.create_task(
            self._run(),
            name=f'{self.name} #{self.id}'
        )


    async def unload(self):
        # This should never happen, but it's better to be safe
        if self._service_task is None: return
        # Cancel the task if not done yet. Note that this task
        # never raises and the task clears the reference when done
        self._service_task.done() or self._service_task.cancel()
        await self._service_task


    async def run(self):
        raise NotImplementedError()



class PeriodicService(ActiveService):
    """
    A service that runs a task periodically
    """

    def __init__(
        self,
        interval: int,
        *,
        name: Optional[str] = None,
        logger: Optional[Logger] = None
    ):
        super().__init__(name=name, logger=logger)
        self.interval: int = interval

    async def _run(self):
        loop = asyncio.get_event_loop()
        spawn_time = loop.time()
        wait_time = 0
        self.logger.debug(f'starting the service task')
        while True:
            try:
                await asyncio.sleep(wait_time)
                await self.run()
            except asyncio.CancelledError:
                break
            except Exception as err:
                self.logger.error(f'the periodic task has failed')
                self.logger.exception(err)
            wait_time = spawn_time - loop.time()
            while wait_time < 0:
                wait_time += self.interval
                spawn_time += self.interval
        self.logger.debug(f'the service task has stopped')
        self._service_task = None
        if self.state == self.RUNNING:
            await self.stop()
