import asyncio
from typing import Optional, List, Iterator, Set
from logging import Logger
from pathlib import Path

from dataclassy import dataclass
from asyncinotify import Inotify, Mask,  Watch

from .base import Service
from video import loopback
from utils import logged_users
from constants import SYSDEVICE_PATH

STREAMING_DELAY=0.1

def bool2str(value):
    return 'yes' if value else 'no'

class LoopbackDevice:
    __slots__ = (
        'index', 'owner', 'openers', 'streaming',
        'streaming_start_time', 'streaming_time',
        'watch', 'set_streaming_handle'
    )

    def __init__(self, index: int, owner: str):
        self.index = index
        self.owner = owner
        self.openers: int = 0
        self.streaming: bool = False
        self.streaming_start_time: float = 0.0
        self.streaming_time: float = 0.0
        self.watch: Optional[Watch] = None
        self.set_streaming_handle: Optional[asyncio.TimerHandle] = None

    @property
    def path(self) -> str:
        return f"/dev/video{self.index}"
    
    def mark_closed(self):
        self.openers = 0
        self.streaming = False
        self.watch = None
        if self.set_streaming_handle:
            self.set_streaming_handle.cancel()
            self.set_streaming_handle = None

    def update(self):
        try:
            current_openers = self.openers
            with open(SYSDEVICE_PATH + f'video{self.index}/active_readers', mode='r') as f:
                self.openers = int(f.read())
                return self.openers != current_openers
        except Exception:
            return False


@dataclass(slots=True)
class UpdateResult:
    new: int = 0
    orphaned: int = 0
    deleted: int = 0
    monitoring: int = 0
    ignored: int = 0
    removed: int = 0


class LoopbackService(Service):

    POLL_INTERVAL = 10*60

    EV_STREAM_ON = 'streamon'
    EV_STREAM_OFF = 'streamoff'

    def __init__(
        self,
        *,
        name: Optional[str] = None,
        logger: Optional[Logger] = None
    ):
        super().__init__(name=name, logger=logger)
        self.devices: List[LoopbackDevice] = []
        self.watcher = Inotify()
        self.__lock: Optional[asyncio.Lock] = None


    def get_device_by_index(self, index: int) -> LoopbackDevice:
        return next(filter(lambda device: device.index == index, self.devices))

    def get_device_by_path(self, path: str) -> LoopbackDevice:
        return next(filter(lambda device: device.path == path, self.devices))
    
    def get_devices_by_owner(self, username: str) -> Iterator[LoopbackDevice]:
        return filter(lambda device: device.owner == username, self.devices)
    

    async def update(self) -> UpdateResult:

        report = UpdateResult()
        report.removed = len(self.devices)

        async with self.__lock:
            self.logger.info(f'refreshing the list of loopback devices')

            # update the list of devices
            logged = logged_users()
            current_devices = {
                device.index : device for device in self.devices
            }
            current_users = set(device.owner for device in self.devices)
            self.devices.clear()

            for index, userid, username in loopback.enumerate():
                if username in logged:
                    device = current_devices.get(index)
                    if device:
                        self.devices.append(device)
                    elif username not in current_users:
                        report.new += 1
                        self.logger.info(f'found /dev/video{index} owned by {username}')
                        device = LoopbackDevice(index, username)
                        device.watch = self.watcher.add_watch(device.path, Mask.OPEN | Mask.CLOSE)
                        self.devices.append(device)
                elif username in current_users:
                    try:
                        report.orphaned += 1
                        self.logger.info(f'removing an orphaned device /dev/video{index} of {username}')
                        await loopback.delete(index)
                        report.deleted += 1
                    except Exception as err:
                        self.logger.warning(f'failed to remove /dev/video{index}: {err!r}')

            report.monitoring = len(self.devices)
            report.removed += report.new - report.monitoring
            for device in self.devices:
                self._check_streaming(device)

        self.logger.info(
            f'update report: new/orphaned/deleted: {report.new}/{report.orphaned}/{report.deleted}, '
            f'monitoring/removed/ignored: {report.monitoring}/{report.removed}/{report.ignored}'
        )
        return report


    async def load(self):
        self.__lock = asyncio.Lock()
        self.loop = asyncio.get_running_loop()
        await self.update()
        self.__monitor_task = asyncio.create_task(self.access_monitor())
        self.__polling_task = asyncio.create_task(self.access_polling(self.POLL_INTERVAL))

    async def unload(self):
        self.__monitor_task.cancel()
        self.__polling_task.cancel()
        await asyncio.gather(self.__monitor_task, self.__polling_task)
        for device in self.devices:
            try:
                await loopback.delete(device.index)
            except Exception as err:
                self.logger.warning(f"failed to delete {device.path}: {err!r}")
        self.devices.clear()


    async def access_polling(self, interval, exc_pause=1):
        # This controlls how many exceptions can happed in a loop
        # The loops pauses if skip_waits <= 0
        # An exception resets skip_waits to 2 if it is negative and drops by one until zero.
        # This way first 2 exceptions will trigger a quicker repetition of the loop.
        quick_repeats = -1
        while True:
            try:
                await self.update()
                quick_repeats = -1
            except asyncio.CancelledError:
                break
            except Exception as e:
                self.logger.critical(f'a leaked exception {e}', exc_info=1)
                quick_repeats = 2 if quick_repeats < 0 else max(0, quick_repeats-1)
            await asyncio.sleep(interval if quick_repeats <= 0 else exc_pause)

    def _check_streaming(self, device: LoopbackDevice):
        if device.set_streaming_handle:
            device.set_streaming_handle.cancel()
            device.set_streaming_handle = None
        if not device.update():
            self.logger.debug(f'no change in capture status of {device.path}')
        self.logger.debug('{} update: captured by {}, streaming: {}'.format(device.path, device.openers, bool2str(device.streaming)))
        if device.openers and not device.streaming:
            device.streaming = True
            device.streaming_start_time = self.loop.time()
            self.logger.info(f'device {device.path} is opened for streaming')
            self.emit(self.EV_STREAM_ON, device)
        elif device.openers == 0 and device.streaming:
            device.streaming = False
            device.streaming_time += self.loop.time() - device.streaming_start_time
            self.logger.info(f'streaming from {device.path} has stopped')
            self.emit(self.EV_STREAM_OFF, device)

    def _onopened(self, device: LoopbackDevice):
        self.logger.debug('{} has been opened. Streaming: {}'.format(device.path, device.openers, bool2str(device.streaming)))
        # delay checking active readers unless already streaming or a check is scheduled
        if not device.streaming and device.set_streaming_handle is None:
            self.logger.debug(f'{device.path} might be captured - scheduling a status update in {STREAMING_DELAY} seconds')
            device.set_streaming_handle = self.loop.call_later(
                STREAMING_DELAY, self._check_streaming, device
            )

    def _onclosed(self, device: LoopbackDevice):
        self.logger.debug('{} has been closed. Streaming: {}'.format(device.path, device.openers, bool2str(device.streaming)))
        # check active readers only if streaming and no check is scheduled
        if device.streaming and device.set_streaming_handle is None:
            self.logger.debug(f'{device.path} may no longer be captured - checking the status')
            self._check_streaming(device)


    async def access_monitor(self):

        event_type = {
            Mask.OPEN: "open",
            Mask.CLOSE_NOWRITE: "close",
            Mask.CLOSE_WRITE: "close"
        }

        while True:
            try:
                event = await self.watcher.get()
                self.logger.debug(f'got an event {event_type.get(event.mask, event.mask)} for {str(event.path)}')
                device = self.get_device_by_path(str(event.path))
                if event.mask == Mask.OPEN:
                    self._onopened(device)
                else:
                    self._onclosed(device)
            except asyncio.CancelledError:
                break
            except StopIteration:
                # This happens when a loopback device is removed
                # from the collection before all events are parsed
                pass
            except Exception as err:
                self.logger.error(repr(err))

        # remove watchers and clear notifiers
        self.watcher.close()
        self.watcher = None
        for device in self.devices:
            device.mark_closed()


    async def get_device(self, username: str) -> Optional[LoopbackDevice]:
        async with self.__lock:
            device = next(self.get_devices_by_owner(username), None)
            if device is None:
                self.logger.debug(f'creating a device for {username}')
                device = LoopbackDevice(await loopback.create(username), username)
                self.devices.append(device)
                # Wait for system to process the new device
                await asyncio.sleep(0.1)
                device.watch = self.watcher.add_watch(device.path, Mask.OPEN | Mask.CLOSE)
                self.logger.debug(f'created {device.path} for {username}')
        return device
    
    async def remove_device(self, username: str) -> Optional[LoopbackDevice]:
        async with self.__lock:
            device: Optional[LoopbackDevice] = next(self.get_devices_by_owner(username), None)
            if device is None: return None
            self.devices.remove(device)
            self.logger.debug(f'stops monitoring {device.path} owned by {username}')
            self.watcher.rm_watch(device.watch)
            self.logger.debug(f'deleting {device.path} owned by {username}')
            await loopback.delete(device.index)
            self.logger.debug(f'removed {device.path} owned by {username}')
            return device
