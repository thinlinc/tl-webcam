import asyncio
import logging

from typing import Optional, Set, Callable
from aiortc import RTCPeerConnection, \
    RTCSessionDescription, \
    RTCConfiguration, \
    MediaStreamTrack

from components.session import AsyncSession
from components.channel import Channel
from service.base import ActiveService

from video.capture import CameraStreamTrack

import commands as cmd


STATE_DISCONNECTED = 0
STATE_CONNECTING = 1
STATE_CONNECTED = 2
STATE_DISCONNECTING = 3

class StreamProvider(ActiveService):
    """
    API:
      stream_on(path=None)  opens RTC connection; starts streaming
                            if a source is set
      stream_off            closes RTC connection
      set_source(path=None) None == mute, but keep connection opened
      connected(port)       connects to the signaling channel
      disconnect            closes the signaling channel
    Session is used only to send requests/notifications
    Request handles are assigned via set_handlers or on('message')
    The latter is used for messages that have no handlers.

    The class uses MultiCameraTrack as the input for RTC stream,
    so that the stream is silent if nothing is attached to it.

    set_source sets the source of the track when the provider is streaming.
    """

    def __init__(
            self,
            capture: Callable[[], MediaStreamTrack],
            name: Optional[str] = None,
            logger: Optional[logging.Logger] = None
        ):
        super().__init__(name=name, logger=logger)
        self.handlers = None
        self.__capture = capture
        self.track: Optional[CameraStreamTrack] = None
        self.host = 'localhost'
        self.port = None
        self.channel_token = None
        self.rtc: Optional[RTCPeerConnection] = None
        self.__current_connection_state = STATE_DISCONNECTED
        self.__desired_connection_state = STATE_DISCONNECTED
        self.session: Optional[AsyncSession] = None

    def set_handlers(self, handlers: dict):
        self.handlers = handlers

    @property
    def is_streaming(self) -> bool:
        return self.rtc and self.rtc.connectionState == 'connected'

    @property
    def is_connected(self) -> bool:
        return self.session and not self.session.is_closed
    
    async def wait_connected(self):
        return self.__connected.wait()
    
    async def wait_disconnected(self):
        return self.__disconnected.wait()

    async def load(self):
        self.__connected = asyncio.Event()
        self.__disconnected = asyncio.Event()
        self.__connection_state_changed = asyncio.Event()
        self.__disconnected.set()
        await super().load()

    async def unload(self):
        await self.stream_off()
        await self.__disconnect()
        await super().unload()

    async def run(self):
        # The main loop of the service: it synchronizes the actual connection
        # state with the desired one.

        next_connect_try = 0
        loop = asyncio.get_running_loop()
        while True:
            if self.__current_connection_state == self.__desired_connection_state:
                # Wait until any of the states is changed
                self.logger.debug('waiting for a change in a connection state')
                await self.__connection_state_changed.wait()
                self.__connection_state_changed.clear()
            # At this moment the current state is not the desired one
            if self.__desired_connection_state is STATE_CONNECTED:
                try:
                    self.__current_connection_state = STATE_CONNECTING
                    # Make sure that we do not reconnect to quickly
                    wait_time = max(next_connect_try - loop.time(), 0)
                    self.logger.debug(f'retrying to connect after {wait_time} seconds')
                    await asyncio.sleep(wait_time)
                    self.logger.info(f"connecting to {self.host}:{self.port}")
                    # Connect
                    next_connect_try = loop.time() + 1.0
                    await self.__connect()
                    self.logger.info(f"connected {self.host}:{self.port}")
                    self.__current_connection_state = STATE_CONNECTED
                    self.__connected.set()
                    self.__disconnected.clear()
                except ConnectionRefusedError as  err:
                    self.logger.debug(f'Failed to connect: {err}')
                except Exception as err:
                    # report the problem
                    self.logger.exception(err)
            else:
                self.logger.info(f"disconnecting from {self.host}:{self.port}")
                self.__current_connection_state = STATE_DISCONNECTING
                await self.__disconnect()
                self.logger.info(f"disconnected from {self.host}:{self.port}")
                self.__current_connection_state = STATE_DISCONNECTED
                self.__disconnected.set()
                self.__connected.clear()

    def connect(self, port: int):
        if self.__desired_connection_state == STATE_CONNECTED: return
        self.port = port
        self.__desired_connection_state = STATE_CONNECTED
        self.__connection_state_changed.set()

    def disconnect(self, port: Optional[int] = None):
        if port and self.port != port: return
        if self.__desired_connection_state == STATE_DISCONNECTED: return
        self.__desired_connection_state = STATE_DISCONNECTED
        self.__connection_state_changed.set()

    async def __connect(self):
        if self.session: await self.session.close()
        if self.rtc: await self.rtc.close()
        reader, writer = await asyncio.open_connection(self.host, self.port)
        
        channel = Channel(reader, writer)
        if self.channel_token:
            try:
                await channel.send(b'auth' + self.channel_token)
                response = await channel.recv()
                assert response == self.channel_token
            except Exception:
                await channel.close()
                raise

        self.session = AsyncSession(
            channel,
            AsyncSession.CLIENT_SIDE,
            handlers=self.handlers,
            logger=self.logger.getChild('session'),
            ping_interval=10.0
        )

        @self.session.on(self.session.EV_CLOSE)
        def onclosed():
            if self.__current_connection_state in (
                STATE_DISCONNECTING, STATE_DISCONNECTED
            ): return
            self.__current_connection_state = STATE_DISCONNECTED
            self.__connection_state_changed.set()


    async def __disconnect(self):
        if self.session: await self.session.close()
        if self.rtc: await self.rtc.close()

    async def stream_on(self):
        if not self.is_connected:
            raise RuntimeError('not connected')
        if self.rtc:
            if self.rtc.connectionState in ('new', 'connecting', 'connected'):
                return
            else:
                await self.rtc.close()

        self.logger.debug("initiating a connection")
        rtc = RTCPeerConnection(RTCConfiguration(iceServers=list()))

        @rtc.on('connectionstatechange')
        async def on_connectionstatechange():
            self.logger.info(f'connection state is {rtc.connectionState}')
            if rtc.connectionState == 'failed' and self.rtc == rtc:
                await self.stream_off()

        @rtc.on('iceconnectionstatechange')
        async def on_iceconnectionstatechange():
            self.logger.info("ICE connection state is %s" % rtc.iceConnectionState)
            if rtc.iceConnectionState == "failed" and self.rtc == rtc:
                await self.stream_off()

        @rtc.on('icegatheringstatechange')
        async def on_icegatheringstatechange():
            self.logger.info("ICE gathering state is %s" % rtc.iceGatheringState)

        @rtc.on('icecandidate')
        async def on_ice_candidate(candidate):
            self.logger.info(f"Found an ICE candidate: {candidate}")


        self.rtc = rtc

        if self.track is None or self.track.readyState != 'live':
            self.track = self.__capture()
#        try:
#            await self.track.set_source(self.source)
#        except Exception as err:
#            self.logger.error(f"Failed to set the source to {self.source}: {err}")
#            self.source = None

        self.logger.debug("creating an offer")
        self.rtc.addTrack(self.track)
        offer = await self.rtc.createOffer()
        await self.rtc.setLocalDescription(offer)

        self.logger.debug("waiting for an answer")
        answer = await self.session.request(cmd.TAKE_OFFER, self.rtc.localDescription.sdp.encode())
        await self.rtc.setRemoteDescription(
            RTCSessionDescription(answer.decode(), "answer")
        )

        self.logger.info("connection initiated")
        # TODO: check that the connection is created


    async def stream_off(self):
        if self.rtc is None: return

        rtc = self.rtc
        self.rtc = None
        # This stops the camera stream track
        await rtc.close()


#    async def select_source(self, path: Optional[str] = None):
#        if self.is_streaming:
#            # TODO: empty path -> default source
#            await self.track.set_source(path)
#        self.source = path

    async def send(self, notification, data):
        if self.is_connected:
            await self.session.notify(notification, data)
            