import asyncio
import threading
import logging
import errno
import time
from typing import Optional, Any, Union, Coroutine, Set, Tuple
from queue import Queue
from concurrent.futures import ThreadPoolExecutor

from service.base import ActiveService
from service.video_monitor import CameraCollection, CameraInfo

from av import Packet, VideoFrame, FFmpegError, open as av_open
from av.container import InputContainer
from aiortc.mediastreams import MediaStreamError, MediaStreamTrack

DEFAULT_VIDEO_WIDTH = 640
DEFAULT_VIDEO_HEIGHT = 480
DEFAULT_VIDEO_FPS = 15

FRAME_QUEUE_SIZE = 5

# Notes on implementation
#
# Frames are captured and decoded in a separate thread that runs the function
# decode_video_source(). The frames are exchanged between this and the main
# thread with a queue. For some reason the last frame stays available after
# a source is closed and accessing it raises segmantation fault. In order to
# avoid this, a frame is put on a queue together with a source id that is
# incremented each time a source is switched. The method recv() then reads
# frames from the queue until the source id matches the current one.

DECODER_EVENT_SOURCE = 0
DECODER_EVENT_QUIT = 1

class VideoSourceOpener:

    __opener_id = 0
    
    @staticmethod
    def __get_id() -> int:
        VideoSourceOpener.__opener_id += 1
        return VideoSourceOpener.__opener_id

    def __init__(self, logger: logging.Logger):
        self.executor = ThreadPoolExecutor(max_workers=2)
        self.opener_tasks: Set[asyncio.Task] = set()
        self.logger = logger

    async def join(self):
        self.logger.debug(f'waiting for {len(self.opener_tasks)} tasks to stop')
        await asyncio.gather(*self.opener_tasks, return_exceptions=True)

    async def open(
        self,
        path: str,
        width: int,
        height: int, *,
        format: str = '',
        framerate: Any = None
    ) -> Tuple[InputContainer, int]:

        opener_id = self.__get_id()
        logger = self.logger.getChild(f'opener#{opener_id}')

        cancelled = False

        def _open(options: dict) -> InputContainer:
            container = None
            try:
                logger.info(f"opening {path}")
                container = av_open(path, mode='r', format='v4l2', options=options)
                logger.debug(f'{path} is opened')
                if cancelled: raise asyncio.CancelledError
                frame = next(container.decode(container.streams.video[0]))
                logger.debug(f'{path} provided frame of type {frame.format.name} {frame.width}x{frame.height}')
                if frame.width != width or frame.height != height:
                    raise RuntimeError(f'invalid frame size: {frame.width}x{frame.height} when expecting {width}x{height}')
                if cancelled: raise asyncio.CancelledError
                return container
            except asyncio.CancelledError:
                logger.debug(f'closing {path} due to a previous cancellation')
                container and container.close()
                return container
            except Exception as err:
                cancelled and logger.warning(
                    f'failed to open {path} while cancelled: {err!r}'
                )
                container and container.close()
                raise

        # Prepare options for av.open
        options = { 'video_size': f"{width}x{height}" }
        if format: options['input_format'] = format
        if framerate: options['framerate'] = str(framerate)

        # Try to open the device in a separate thread and decode
        # the first frame. We then check that the size of the frame
        # matches the requested one.
        async def async_open():
            logger.debug(f"delegating opener for {path} to a separate thread")
            loop = asyncio.get_running_loop()
            return await loop.run_in_executor(self.executor, _open, options)
        opener_task = asyncio.create_task(async_open(), name=f"opener:{path}")
        # Cancelling loop.run_in_executor() does not stop the thread,
        # so that the camera will be opened anyway and not closed.
        # Therefore we surpress CancelError, but we set a flag to close
        # the device at the end.
        self.opener_tasks.add(opener_task)
        opener_task.add_done_callback(self.opener_tasks.remove)

        try:
            container = await asyncio.shield(opener_task)
            return container, opener_id
        except asyncio.CancelledError:
            cancelled = True
            raise
        except Exception as err:
            logger.error(f'failed to open {path}: {err!r}')
            raise


class DropFrameMonitor:
    def __init__(self, logger: logging.Logger, interval: int = 30):
        self.logger = logger
        self.dropped = 0
        self.recently_dropped = 0
        self.next_warning_time = 0
        self.warning_interval = interval
    def reset(self):
        self.log_stats()
        self.dropped = 0
        self.recently_dropped = 0
        self.start_time = time.time()
        self.next_warning_time = self.start_time + self.warning_interval
    def mark(self):
        self.recently_dropped += 1
        cur_time = time.time()
        if self.next_warning_time < cur_time:
            self.dropped += self.recently_dropped
            self.logger.warning(f"{self.recently_dropped} frames dropped ({self.dropped} in total)")
            self.recently_dropped = 0
            self.next_warning_time = cur_time + 30
    def log_stats(self):
        self.dropped += self.recently_dropped
        if self.dropped == 0: return
        elapsed_time = time.time() - self.start_time
        if elapsed_time < 1: return
        drop_rate = self.dropped / elapsed_time
        self.logger.info(f"{self.dropped} frames dropped, {drop_rate:.2f} per second on average")


def decode_video_source(
        target: asyncio.Queue,
        events: Queue,
        loop: asyncio.AbstractEventLoop,
        logger: logging.Logger
):

    logger.info('starts the decoding thread')

    source = None
    source_id = 0
    start_pts = 0

    drop_monitor = DropFrameMonitor(logger)

    while not loop.is_closed():
        
        # wait for a source if none
        logger.info(f"waiting for a source")
        while source is None:
            event, *data = events.get()
            # Parse the event
            if event == DECODER_EVENT_QUIT:
                logger.info(f'stops the decoding thread')
                return
            # event == DECODER_EVENT_SOURCE
            source, source_id = data

        # At this moment source is not empty
        logger.info(f"event: set source to {source.name} (#{source_id})")
        assert source.streams != None
        assert len(source.streams.video) == 1

        stream = source.streams.video[0]
        forward = stream.codec_context.name == 'h264'
        frames = source.demux(stream) if forward else source.decode(stream)
        # do not keep a reference
        stream = None

        first_frame = True
        logger.debug(f'parsed update #{source_id}')

        # At this moment source is not empty, so that we can read frames

        if forward:
            logger.info(f"demuxing {source.name}")
        else:
            logger.info(f"decoding {source.name}")
        while events.empty() and not loop.is_closed():
            try:
                if first_frame:
                    logger.debug(f"reading the first frame")
                    drop_monitor.reset()
                frame = next(frames)
                if frame.pts is None:
                    logger.debug(f"skipping a frame/packet with no pts")
                    continue
                if first_frame:
                    if forward:
                        logger.debug("format: encoded")
                    else:
                        logger.debug(f"format: {frame.width}x{frame.height} ({frame.format.name})")
                    first_frame = False
                    start_pts = frame.pts - start_pts
                if target.qsize() < FRAME_QUEUE_SIZE:
                    frame.pts -= start_pts
                    asyncio.run_coroutine_threadsafe(target.put((source_id, frame)), loop)
                else:
                    drop_monitor.mark()

            except Exception as err:
                if isinstance(err, FFmpegError) and err.errno == errno.EAGAIN:
                    logger.warning(str(err))
                    time.sleep(0.01)
                else:
                    logger.error(f"failed to process a video frame: {err}")
                    break

        # There is an event. Prepare by closing the current source
        logger.info(f"closing {source.name}")
        logger.debug(f"frames left: {target.qsize()}")
        drop_monitor.log_stats()

        frames.close()
        del frames
        source.close()
        source = None



class VideoInputTrack(MediaStreamTrack):
    kind = "video"

    def __init__(self, media: 'VideoInput'):
        super().__init__()
        self.__media = media

    async def recv(self) -> Coroutine[Any, Any, Union[VideoFrame,Packet]]:
        try:
            return await asyncio.shield(self.__media.future_packet)
        except Exception:
            self.stop()
            raise MediaStreamError


V4L2_PIX_FMT_TO_AVLIB = {
    "H264" : "h264",
    "MJPEG": "mjpeg",
    "YUYV": "rawvideo",
    "GREY": "gray"
}


class VideoInput(ActiveService):

    def __init__(
            self, *,
            width: int = DEFAULT_VIDEO_WIDTH,
            height: int = DEFAULT_VIDEO_HEIGHT,
            framerate: Any = DEFAULT_VIDEO_FPS,
            name: Optional[str] = None,
            logger: Optional[logging.Logger] = None
    ):
        super().__init__(name=name, logger=logger)
        # the requested frame size
        self.__width = width
        self.__height = height
        self.__framerate = framerate
        self.__current_width = 0
        self.__current_height = 0
        # the list of created tracks
        self.__tracks: Set[VideoInputTrack] = set()
        # the currently opened video device
        self.__container: Optional[InputContainer] = None
        # an integer counting opened cameras so far used to drop
        # expired video frames - see the notes at the top
        self.__source_id = 0

        self.__source: Optional[CameraInfo] = None

        self.__opener = VideoSourceOpener(logger=self.logger.getChild('opener'))
        # If true, the input is not closed when all tracks are stopped
        self.keep_alive = False



    async def load(self):
        self.__loop = asyncio.get_running_loop()
        self.__frames = asyncio.Queue(FRAME_QUEUE_SIZE)
        self.__thread_events = Queue()
        self.__future_packet: asyncio.Future = self.__loop.create_future()
        self.__decoder = threading.Thread(
            target=decode_video_source,
            name='video-decoder',
            args=(self.__frames, self.__thread_events, self.__loop, self.logger.getChild('decoder')),
            daemon=True
        )
        self.__decoder.start()
        await super().load()

    async def unload(self):
        self.__thread_events.put((DECODER_EVENT_QUIT,))
        await super().unload()
        await self.__opener.join()
        if self.__decoder.is_alive:
            # A nonblocking wait for the decoder to stop
            await asyncio.sleep(0.1)
        self.__decoder.join()

    async def run(self):
        while True:
            try:
                source_id, data = await self.__frames.get()
                if source_id == self.__source_id:
                    self.__future_packet.set_result(data)
                    self.__future_packet = self.__loop.create_future()
                else:
                    self.logger.debug(f'dropping a frame from source #{source_id}')
            except BaseException as err:
                if not isinstance(err, asyncio.CancelledError):
                    self.logger.exception(err)
                # stop all tracks and make sure the future is awaited
                # in order to avoid errors
                try:
                    self.__future_packet.set_exception(MediaStreamError())
                    await self.__future_packet
                except MediaStreamError:
                    pass
                break

    @property
    def is_capturing(self) -> bool:
        return self.__container is not None
    
    @property
    def source(self) -> Optional[CameraInfo]:
        return self.__source

    @property
    def width(self) -> int:
        return self.__width
    
    @property
    def height(self) -> int:
        return self.__height
    
    @property
    def future_packet(self) -> asyncio.Future:
        return self.__future_packet
    
    def __remove_track(self, track: VideoInputTrack):
        self.__tracks.remove(track)
        if not self.keep_alive and len(self.__tracks) == 0:
            self.close()

    def capture(self) -> MediaStreamTrack:
#        if len(self.__tracks) == 0 and self.__source:
#            self.logger.debug('starts capturing video')
#            await self.open()
        track = VideoInputTrack(self)
        track.add_listener('ended', lambda: self.__remove_track(track))
        self.__tracks.add(track)
        return track
    
    async def open(
            self,
            camera: CameraInfo, *,
            width: int = 0,
            height: int = 0,
            framerate: int = 0
    ):
        width = width or self.__width
        height = height or self.__height
        framerate = framerate or self.__framerate

        self.logger.info(f'opening {camera.path}: {width}x{height}@{framerate}')

        if all((
            self.__container and camera.path == self.__container.name,
            width == self.__width,
            height == self.__height,
            framerate == self.__framerate
        )):
            self.logger.info(f'the camera is already opened in the desired mode')
            return

        # serach for an ideal format first
        format = next(
            (x for x in ('H264', 'YUYV', 'MJPEG') if camera.supports(width, height, pixel_format=x)), None
        )
        if format:
            self.logger.debug(f'selected {format} as input format for {camera.path}')
            fwidth = width
            fheight = height
        else:
            best_matches = camera.get_closest_formats(width, height)
            format = next(
                (x for x in ('H264', 'YUYV', 'MJPEG') if x in best_matches), None
            ) or next(iter(best_matches))
            frame_size = best_matches[format]
            fwidth = frame_size['width']
            fheight = frame_size['height']
            self.logger.debug(f'no exact format match for {camera.path}; selected {format} {fwidth}x{fheight}')


        container, source_id = await self.__opener.open(
            str(camera.path),
            fwidth,
            fheight,
            format=V4L2_PIX_FMT_TO_AVLIB.get(format),
            framerate=framerate
        )
        self.logger.debug(f'opener returned {container} (id: {source_id})')
        self.__container = container
        self.__source_id = source_id
        self.__source = camera
        self.__width = width
        self.__height = height
        self.__current_width = fwidth
        self.__current_height = fheight
        self.__framerate = framerate
        self.__thread_events.put((DECODER_EVENT_SOURCE, container, source_id))

    def close(self):
        if self.__container:
            self.logger.debug('stops capturing video')
            self.__container = None
            self.__source = None
            self.__thread_events.put((DECODER_EVENT_SOURCE, None, 0))
