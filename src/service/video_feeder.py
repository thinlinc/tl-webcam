import asyncio
import time
from logging import Logger, getLogger
from typing import Optional, Union
from fractions import Fraction
from v4l2py.device import Device as V4L2Device, \
    BufferType as V4L2BufferType

from service.base import ActiveService

from aiortc import MediaStreamTrack
from aiortc.mediastreams import MediaStreamError

from av import VideoFrame

logger = getLogger('video.output')
   
SPLASH_WIDTH=640
SPLASH_HEIGHT=480

class SingleFrameTrack(MediaStreamTrack):
    
    def __init__(self, frame: VideoFrame = None, width: int = 0, height: int = 0):
        if frame is None and (width == 0 or height == 0):
            raise ValueError("either a frame or both dimensions must be provided")
        super().__init__()
        self.frame = frame or VideoFrame(width, height, format='yuv420p')
        self.sent = False
        self.__size = sum(plane.buffer_size for plane in self.frame.planes)
        self.__loop = asyncio.get_running_loop()
        self.__next_frame = 0

    def update_raw(self, buffer: bytes):
        if len(buffer) != self.__size:
            ValueError(f"provided {len(buffer)} bytes, but {self.__size} where expected")
        offset = 0
        for plane in self.frame.planes:
            plane.update(buffer[offset:offset+plane.buffer_size])
            offset += plane.buffer_size

    def reset(self):
        self.sent = False

    async def recv(self) -> VideoFrame:
        if self.sent:
            raise MediaStreamError
        cur_time = self.__loop.time()
        if cur_time < self.__next_frame:
            await asyncio.sleep(self.__next_frame - cur_time)
        else:
            self.__next_frame = cur_time
        self.__next_frame += 0.1
        return self.frame

        

def dummy_frame(width: int, height: int) -> VideoFrame:
    frame = VideoFrame(width, height)
    for plane in frame.planes:
        plane.update(bytes(plane.buffer_size))
    return frame


class FrameFormatMonitor:
    def __init__(self, logger: Logger, interval: int = 30):
        self.processed = 0
        self.logger = logger
        self.next_message_time = 0
        self.message_interval = interval
        self.start_time = 0
    def reset(self):
        self.log_stats()
        self.start_time = time.time()
        self.next_message_time = self.start_time + self.message_interval
    def mark(self, frame: VideoFrame):
        self.processed += 1
        cur_time = time.time()
        if self.next_message_time < cur_time:
            self.logger.debug(f"frame #{self.processed}: {frame.width}x{frame.height} ({frame.format.name})")
            self.next_message_time = cur_time + 30
    def log_stats(self):
        elapsed_time = time.time() - self.start_time
        if elapsed_time < 1 or self.start_time == 0: return
        frame_rate = self.processed / elapsed_time
        self.logger.info(f"has received {self.processed} frames, {frame_rate:.2f} per second on average")


class VideoOutput:

    def __init__(self):
        self.device: Optional[V4L2Device] = None
        self.width = 0
        self.height = 0
        self.input_width = 0
        self.input_height = 0
        self.ratio = Fraction(0,1)
        self.buffer = None

    # TODO: make it robust
    # This method raises OSError: device busy when a frame has been already
    # sent to the loopback device or when the loopback device is accessed
    # for streaming. The proper way to set the format goes as follows:
    # - make sure that the loopback device is not used
    # - close and reopen the device
    # Perhaps it is worth to consider setting the format on open
    def set_format(self, width: int, height: int, pix_fmt: str = 'YU12'):
        result = self.device.set_format(
            V4L2BufferType.VIDEO_OUTPUT,
            width,
            height,
            pix_fmt
        )
        if result == 0:
            logger.debug(f'setting format to {width}x{height}')
            self.width = width
            self.height = height
            self.ratio = Fraction(width, height)
            self.yplane_size = width*height
            self.uvplane_size = self.yplane_size // 4
            self.uoffset = self.yplane_size
            self.voffset = self.uoffset + self.uvplane_size
            self.buffer = bytearray(self.yplane_size + 2*self.uvplane_size)
            logger.debug(f'created a buffer of size {len(self.buffer)}')
            # This resets input paramaters, so that they will be
            # updated on the next frame
            self.input_width = 0
        return result
    
    def set_input_format(self, frame: VideoFrame):
        self.input_width = frame.width
        self.input_height = frame.height
        self.input_format = frame.format.name
        self.input_reformat = self.input_format != 'yuv420p'
        input_ratio = Fraction(frame.width, frame.height)
        if input_ratio == self.ratio:
            self.scale_width = self.width
            self.scale_height = self.height
            self.input_reformat = self.input_reformat or self.input_width != self.width
            self.yskip = 0
            self.uvskip = 0
            self.yindent = 0
            self.uvindent = 0
        elif input_ratio > self.ratio:
            self.scale_width = int(self.height * input_ratio) & 0xFFFE
            self.scale_height = self.height
            self.input_reformat = self.input_reformat or self.input_height != self.height
            self.yskip = 0
            self.uvskip = 0
            self.uvindent = (self.scale_width - self.width) // 4
            self.yindent = self.uvindent * 2
        else:
            self.scale_width = self.width
            self.scale_height = int(self.width / input_ratio) & 0xFFFE
            self.input_reformat = self.input_reformat or self.input_width != self.width
            self.uvskip = ((self.input_height - self.height) * self.width) // 8
            self.yskip = self.uvskip * 4
            self.yindent = 0
            self.uvindent = 0

    
    def write_raw(self, raw: bytes):
        self.device.write(raw)

    def process_frame(self, frame: VideoFrame):
        if frame.width != self.input_width or frame.height != self.input_height or frame.format.name != self.input_format:
            logger.info('detected a new frame format')
            self.set_input_format(frame)
            if self.yskip:
                trunc = f" then truncated to {self.width}x{self.height} by cutting {self.yskip} lines"
            elif self.yindent:
                trunc = f" then truncated to {self.width}x{self.height} by cutting {self.yindent} columns"
            else:
                trunc = ""
            logger.info(f'frames will be scaled {frame.width}x{frame.height} -> {self.scale_width}x{self.scale_height}{trunc}')

        if self.input_reformat:
            frame = frame.reformat(width=self.scale_width, height=self.scale_height, format='yuv420p')

        if self.yskip:
            self.buffer[:self.uoffset] = bytes(frame.planes[0])[self.yskip:self.yskip+self.yplane_size]
            self.buffer[self.uoffset:self.voffset] = bytes(frame.planes[1])[self.uvskip:self.uvskip+self.uvplane_size]
            self.buffer[self.voffset:] = bytes(frame.planes[2])[self.uvskip:self.uvskip+self.uvplane_size]
        elif self.yindent:
            line_width = self.width
            buf_start = 0
            data_start = self.yindent
            for plane in frame.planes:
                data = bytes(plane)
                for data_chunk in range(data_start, plane.buffer_size, plane.line_size):
                    buf_end = buf_start + line_width
                    self.buffer[buf_start:buf_end] = data[data_chunk:data_chunk+line_width]
                    buf_start = buf_end
                data_start = self.uvindent
                line_width = self.width // 2
        else:
            self.buffer[:self.uoffset] = bytes(frame.planes[0])
            self.buffer[self.uoffset:self.voffset] = bytes(frame.planes[1])
            self.buffer[self.voffset:] = bytes(frame.planes[2])


    def write_frame(self, frame: VideoFrame):
        self.process_frame(frame)
        self.device.write(self.buffer)



    async def acquire(self) -> str:
        raise NotImplementedError()

    async def release(self) -> None:
        pass

    async def __aenter__(self):
        path = await self.acquire()
        self.device = V4L2Device(path)
        self.device.__enter__()
#        self.buffer = BufferedWriter(self.device._fobj, self.buffer_size)
#        self.buffer.__enter__()
        return self
    
    async def __aexit__(self, exc_type, exc, tb):
#        self.buffer.__exit__(exc_type, exc, tb)
        self.device.__exit__(exc_type, exc, tb)
        self.buffer = None
        await self.release()


class VideoOutputDevice(VideoOutput):
    def __init__(self, path: str):
        super().__init__()
        self.path = path

    async def acquire(self):
        return self.path

class SyncEvent:
    def __init__(self):
        self.__is_set = False
    def is_set(self):
        return self.__is_set
    def set(self):
        self.__is_set = True
    def clear(self):
        self.__is_set = False
    async def wait(self):
        raise NotImplementedError()
    def upgrade(self) -> asyncio.Event:
        result = asyncio.Event()
        if self.__is_set: result.set()
        return result


class CameraFeeder(ActiveService):

    def __init__(
            self,
            target: VideoOutput, *,
            name: Optional[str] = None,
            logger: Optional[Logger] = None,
            width,
            height,
            splash: Optional[str] = None
        ):
        super().__init__(name=name, logger=logger)
        self.width = width
        self.height = height
        self.target = target
        self.logger.debug("Creating a buffer")
        frame_size = height * width
        self.splash = bytearray(frame_size  + (frame_size >> 1))
        self.logger.debug("Got a buffer of size {}".format(len(self.splash)))
        if splash:
            try:
                self.logger.debug(f"Reading {splash}")
                with open(splash, 'rb') as splash_file:
                    bytes_read = splash_file.readinto(self.splash)
                    self.logger.info(f"Loaded splash screen from {splash} ({bytes_read} bytes)")
            except Exception as err:
                self.logger.warning(f"Failed to read the splash screen {splash}: {err!r}")
        self.track: Optional[MediaStreamTrack] = None
        self.__captured: Union[asyncio.Event,SyncEvent] = SyncEvent()

    @property
    def captured(self) -> bool:
        return self.__captured.is_set()
    
    @captured.setter
    def captured(self, value: bool):
        if value:
            self.__captured.set()
        else:
            self.__captured.clear()

    async def run(self):

        self.splash_frame = SingleFrameTrack(
            width=SPLASH_WIDTH,
            height=SPLASH_HEIGHT
        )
        self.splash_frame.update_raw(self.splash)
        self.__captured = self.__captured.upgrade()

        self.frame_monitor = FrameFormatMonitor(self.logger)

        async with self.target:
            self.target.set_format(self.width, self.height)
            self.track = self.splash_frame
            frame = await self.track.recv()
            failed_recv = 0
            while True:
                try:
                    self.target.write_frame(frame)
                    if not self.__captured.is_set():
                        self.logger.debug("Paused until the camera is captured")
                        await self.__captured.wait()
                        self.logger.debug("Resumed")
                    frame = await asyncio.wait_for(self.track.recv(), timeout=0.2)
                    self.frame_monitor.mark(frame)
                    failed_recv = 0
                except asyncio.TimeoutError:
                    failed_recv += 1
                    if failed_recv == 25:
                        self.logger.warning("The track failed to deliver a frame for over 5sec")
                        frame = await self.splash_frame.recv()
                except MediaStreamError:
                    self.logger.debug("The track has stopped")
                    self.track = self.splash_frame
                except Exception as err:
                    self.logger.exception(err)


    def set_track(self, track):
        if self.track == track: return
        if track:
            self.logger.debug("Got a new track")
            self.track = track
            self.frame_monitor.reset()
        else:
            self.logger.debug("Stops capturing a track - switching to the splash screen")
            self.track = self.splash_frame
            self.frame_monitor.log_stats()
