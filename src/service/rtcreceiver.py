import asyncio

from typing import Optional
from aiortc import RTCPeerConnection, \
    RTCSessionDescription, \
    RTCConfiguration, \
    MediaStreamTrack

from components.errors import AuthenticationError
from components.session import Session, AsyncSession
from components.channel import Channel
from service.server import TcpServer
from service.video_feeder import CameraFeeder

import commands as cmd


class StreamReceiver(TcpServer):

    channel_token = None
    session: Optional[Session] = None
    rtc: Optional[RTCPeerConnection] = None
    source: Optional[str] = None
    feeder: Optional[CameraFeeder] = None

    def __init__(self, host: str, port: int):
        super().__init__(host, port, name='webrtc')
        self.add_handlers({
            cmd.TAKE_OFFER: self.accept_offer
        })

    @property
    def is_connected(self) -> bool:
        return self.session and not self.session.is_closed
    
    @property
    def is_streaming(self) -> bool:
        return self.rtc and self.rtc.connectionState == 'connected'

    async def create_session(
            self,
            reader: asyncio.StreamReader,
            writer: asyncio.StreamWriter
    ) -> Session:
#        if self.channel_token is None:
#            raise AuthenticationError('a connection must be pre-registered')

        channel = Channel(reader, writer)
        if self.is_connected:
            await channel.send(b'authbusy')
            await channel.close()
            raise AuthenticationError('already connected')

        self.logger.debug(f'creating a session')
        self.session = AsyncSession(
            channel,
            side = AsyncSession.SERVER_SIDE,
            logger = self.logger.getChild('session'),
            handlers = self.handlers
        )

#        try:
#            # authenticate
#            authorization = await session.channel.recv(wait_time=1.0)
#            if validate_token(authorization):
#                # connection is OK - clear the secret key
#                self.secret_key = None
#                await session.channel.send(b'ok')
#            else:
#                await session.channel.send(b'err')
#                raise AuthenticationError('invalid message')
#        except nacl.exceptions.CryptoError as err:
#            raise AuthenticationError(str(err))
        return self.session
    
    async def accept_offer(self, session: Session, offer: bytes) -> bytes:
        if self.rtc: await self.rtc.close()

        rtc = RTCPeerConnection(RTCConfiguration(iceServers=list()))

        @rtc.on('connectionstatechange')
        async def on_connectionstatechange():
            self.logger.info(f"connection state is {rtc.connectionState}")
            if rtc.connectionState == "failed":
                if self.rtc == rtc: self.rtc = None
                await rtc.close()    

        @rtc.on('iceconnectionstatechange')
        async def on_iceconnectionstatechange():
            self.logger.info("ICE connection state is %s" % rtc.iceConnectionState)
            if rtc.iceConnectionState == "failed":
                if self.rtc == rtc: self.rtc = None
                await rtc.close()

        @rtc.on('icegatheringstatechange')
        async def on_icegatheringstatechange():
            self.logger.info("ICE gathering state is %s" % rtc.iceGatheringState)

        @rtc.on('icecandidate')
        async def on_ice_candidate(candidate):
            self.logger.info(f"Found an ICE candidate: {candidate}")

        @rtc.on('track')
        def on_track(track: MediaStreamTrack):
            self.logger.info(f'received a {track.kind} track')
            if track.kind != 'video': return
            self.feeder.set_track(track)

            @track.on('ended')
            async def on_ended():
                self.logger.info('track had ended')
                # only one track per stream: no track, no stream
                if self.rtc == rtc:
                    self.rtc = None
                    await rtc.close()

        self.rtc = rtc

        self.logger.info('accepting an offer')
        await self.rtc.setRemoteDescription(
            RTCSessionDescription(offer.decode(), 'offer')
        )
        answer = await self.rtc.createAnswer()
        await self.rtc.setLocalDescription(answer)

        return self.rtc.localDescription.sdp.encode()
    

    async def stop_streaming(self):
        if self.rtc:
            rtc = self.rtc
            self.rtc = None
            # This should not be necessary
            await self.session.notify(cmd.STOP)
            await rtc.close()

    async def unload(self):
        await self.stop_streaming()
        await super().unload()
