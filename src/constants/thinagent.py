from os.path import dirname, abspath, expanduser, join
from os import environ
from sys import stdout


# https://docs.python.org/3/library/logging.html#logging-levels

#  0 NOTSET - inherit (on root: all)
# 10 DEBUG
# 20 INFO
# 30 WARNING
# 40 ERROR
# 50 CRITICAL

LOG_STREAM = False
LOG_LEVEL = 20
LOG_FILE = '/var/log/tlwebcam.log'

MAIN_DIR = dirname(dirname(abspath(__file__)))
USER_DIR = join(expanduser('~'), ".tlwebcam")

RUNNING_DIR = "/run/tlwebcam"
USER_RUNNING_DIR = "/var/opt/thinlinc/sessions/{}/last/tlwebcam"

SPLASH_IMAGE = join(MAIN_DIR, 'images/splash.yuv')
"""The path to the splash screen. This must be a raw image matching the camera resolution"""

SERVICE_CHANNEL = join(RUNNING_DIR, "channel")
"""The location of the unix socket for communication with the main daemon service"""

SERVICE_AUTH_TOKEN = join(RUNNING_DIR, "authkey")
"""The path to the file with the authentication key"""

USER_CHANNEL = join(USER_RUNNING_DIR, "channel")
"""The location of the unix socket for communication with the user daemon service"""

USER_AUTH_TOKEN = join(USER_RUNNING_DIR, "authkey")
"""The string pattern for the path to the session key"""


V4L2LOOPBACK_CTL = "v4l2loopback-ctl"
SYSDEVICE_PATH = "/sys/devices/virtual/video4linux/"
