from os.path import join

# https://docs.python.org/3/library/logging.html#logging-levels

#  0 NOTSET - inherit (on root: all)
# 10 DEBUG
# 20 INFO
# 30 WARNING
# 40 ERROR
# 50 CRITICAL

LOG_STREAM = False
LOG_LEVEL = 20
LOG_FILE = '/var/log/tlwebcam.log'

VIDEO_RUNNING_DIR = '/run/tswebcam'
VIDEO_CHANNEL = join(VIDEO_RUNNING_DIR, "channel")
VIDEO_AUTH_TOKEN = join(VIDEO_RUNNING_DIR, "authkey")

CLIENT_DATA_FILE = join(VIDEO_RUNNING_DIR, "storage")
