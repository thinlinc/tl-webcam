#!/bin/python


# Import libraries for image processing

from PIL import Image
from av import VideoFrame

file = "webcam.png"

# Load the image and convert to yuv420p
converted = VideoFrame.from_image(Image.open(file)).reformat(format='yuv420p')

# Save the image
with open(file + ".yuv", mode='wb+') as yuv:
    for plane in converted.planes:
        yuv.write(plane)
        
print("Done")        
