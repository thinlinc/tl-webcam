"""
project: ThinLink remote webcam
author: Krzysztof Putyra
date: 8/03/2023

A simple logger used by all components of the project.
"""

import logging
from logging.handlers import WatchedFileHandler, SysLogHandler
from typing import Union, Tuple

from constants import LOG_FILE, LOG_LEVEL, LOG_STREAM

STREAM_LOG_FORMAT = '%(asctime)s %(levelname)-7s {}:%(name)s - %(message)s'
"""The default formatter for logs sent to a stream"""

FILE_LOG_FORMAT = '%(asctime)s {} %(levelname)s {}:%(name)s - %(message)s'
"""The default formatter for logs sent to a log file"""

SYSLOG_FORMAT = '%(asctime)s {} %(levelname)s {}:%(name)s - %(message)s'
"""The default formatter for logs sent to syslog"""

LOG_FILE_PATH = '/var/log/tlwebcam.log'
"""The path to the log file"""


class Logger(logging.LoggerAdapter):
    """A simple logger with methods to add some handlers"""

    def __init__(self, logger: logging.Logger):
        super().__init__(logger, None)
        self.logger: logging.Logger = logger

    def add_stream_target(
        self,
        stream = None, /,
        level: int = -1,
        format: str = STREAM_LOG_FORMAT
    ):
        """
        Creates a stream handler for the logger.

        Parameters
        ----------
        stream : StreamWriter
            the target of the handler; stderr by default
        level : int
            the minimal level of accepted logs (default: all)
        format : str
            the formatter string for logs
        """

        handler = logging.StreamHandler(stream)
        handler.setFormatter(logging.Formatter(format))
        if level >= 0: handler.setLevel(level)
        self.logger.addHandler(handler)
        return self


    def add_file_target(
        self,
        path: str, /,
        level: int = -1,
        format: str = FILE_LOG_FORMAT
    ):
        """
        Creates a file handler for the logger.

        Parameters
        ----------
        path : str
            the path to the log file
        level : int
            the minimal level of accepted logs (default: all)
        format : str
            the formatter string for logs
        """

        # Make sure that the target file exists
        # If it cannot be created then a handler
        # is not created and the method fails silently
        try:
            with open(path, mode="a"): pass
            # Create a handler
            handler = WatchedFileHandler(path)
            handler.setFormatter(logging.Formatter(format))
            if level >= 0: handler.setLevel(level)
            self.logger.addHandler(handler)
        except Exception:
            pass
        return self


    def add_syslog_target(
        self, /,
        addr: Union[str, Tuple[str,int]] = '/dev/log',
        level: int = -1,
        format: str = SYSLOG_FORMAT
    ):
        """
        Creates a syslog handler for the logger.

        Parameters
        ----------
        addr : str
            the location for the syslog; /dev/log by default
        level : int
            the minimal level of accepted logs (default: all)
        format : str
            the formatter string for logs
        """

        handler = SysLogHandler(addr)
        handler.setFormatter(logging.Formatter(format))
        if level >= 0: handler.setLevel(level)
        self.logger.addHandler(handler)
        return self
    

def create(
        level: int = LOG_LEVEL,
        name: str = None,
        username: str = None,
        hostname: str = None,
        appname: str = None
) -> Logger:

    # Optimization
    logging._srcfile = None
    logging.logThreads = False
    logging.logProcesses = False
    logging.logMultiprocessing = False

    # Configure the default logger
    logger = Logger(logging.getLogger(name))
    logger.setLevel(level)
    source = f"{username}@{hostname}" if username else hostname
    if LOG_FILE: logger.add_file_target(LOG_FILE, format=FILE_LOG_FORMAT.format(source, appname or ''))
    if LOG_STREAM: logger.add_stream_target(LOG_STREAM, format=SYSLOG_FORMAT.format(source, appname or ''))

    # Return the wrapper
    return logger
