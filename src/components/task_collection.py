"""
project: ThinLink remote webcam
author: Krzysztof Putyra
date: 03/04/2023

Provides the class TaskManager that collects and manages asynchronous tasks.
"""

import asyncio
from typing import Optional, Set, Dict

class TaskCollection:
    """
    A simple manager for asyncio tasks that ensures that all tasks
    are finished and awaited.
    """

    # This class keeps references to asyncio tasks, preventing them
    # from being gargabe collected prematurily

    def __init__(self):
        self._tasks: Set[asyncio.Task] = set()

    def __len__(self) -> int:
        return len(self._tasks)
    
    def add_task(self, task: asyncio.Task) -> asyncio.Task:
        """
        Adds a running task to the collection.
        """

        self._tasks.add(task)
        # Remove the reference to the task when done
        task.add_done_callback(self._tasks.remove)
        return task
    
    def remove_task(self, task: asyncio.Task) -> bool:
        """
        Removes a task from the collection and returns `True` if one exists.
        Otherwise returns `False.`
        """

        try:
            task.remove_done_callback(self._tasks.remove)
            self._tasks.remove(task)
            return True
        except KeyError:
            return False


    def create_task(self, awaitable, /, name: str = None) -> asyncio.Task:
        """
        Creates an asyncio task and keeps a reference to it.
        """

        return self.add_task(asyncio.create_task(awaitable, name=name))


    async def clear(self) -> Optional[Dict[asyncio.Task, Exception]]:
        """
        Cancels all active tasks and awaits them.
        Returns `None` if all tasks has been cancelled properly
        and a dictionary of failed tasks otherwise with tasks as keys
        and errors as values.
        """

        # A helper function that awaits cancelled tasks
        # and surpresses all exceptions.
        async def cancel(task: asyncio.Task, errors: dict):
            try:
                task.cancel()
                await task
            except asyncio.CancelledError:
                pass
            except Exception as err:
                errors[task] = err

        if len(self._tasks) == 0: return None

        errors = {}
        cancellations = [cancel(task, errors) for task in self._tasks]
        await asyncio.gather(*cancellations)
        return errors or None

    async def __aenter__(self):
        return self
    
    async def __aexit__(self, exc, exc_type, tb):
        await self.clear()