import asyncio
from logging import Logger, getLogger
from typing import Any, Dict, Callable, Coroutine, NamedTuple, Optional
from struct import pack, unpack_from, calcsize
from pyee.asyncio import AsyncIOEventEmitter

from .task_collection import TaskCollection
from .channel import Channel
from .errors import ApplicationError, UnknownCommand, RequestFailedError, AuthenticationError, TokenLoadingError

class RequestInfo(NamedTuple):
    cmd: bytes
    id: int
    data: Any

RequestHandler = Callable[
    [RequestInfo, bytes],
    Coroutine[Any, Any,  Optional[bytes]]
]


class Session(AsyncIOEventEmitter):

    onclosed: Optional[Callable[['Session'], None]] = None

    EV_CLOSE = 'close'

    def __init__(
            self,
            channel: Channel, /,
            logger: Logger=None,
            **kwargs
    ):
        super().__init__()
        self.__data = kwargs
        self.channel = channel
        self.logger = logger or getLogger(self.__class__.__name__)
        self.logger.info('created a session')

        @channel.on(Channel.EV_CLOSE)
        def onclosed():
            self.logger.info("session is closed")
            self.emit(self.EV_CLOSE, self)
        

    def __getitem__(self, key) -> Any:
        return self.__data.get(key)
    
    def __setitem__(self, key, value) -> None:
        self.__data[key] = value

    def __contains__(self, key) -> bool:
        return key in self.__data

    def __delitem__(self, key):
        if key in self.__data:
            del self.__data[key]

    def get(self, key, default=None):
        return self.__data.get(key, default)
    
    def set(self, key, value):
        self.__data[key] = value

    @property
    def is_closed(self) -> bool:
        return self.channel.is_closed

    async def close(self):
        if self.is_closed: return
        # This emits 'closed'
        await self.channel.close()
    
    async def notify(self, cmd: bytes, data: Optional[bytes] = None) -> None:
        raise NotImplementedError()

    async def request(self, cmd: bytes, data: Optional[bytes] = None, /, timeout: float = 5.0) -> bytes:
        raise NotImplementedError()

    async def respond(self, req: RequestInfo, response: bytes) -> None:
        raise NotImplementedError()

    async def reject(self, req: RequestInfo, error: ApplicationError) -> None:
        raise NotImplementedError()

    async def __aenter__(self):
        return self
    
    async def __aexit__(self, exc, exc_type, tb):
        await self.close()


class ActiveSession(Session):
    def __init__(
            self,
            channel: Channel, /,
            logger: Logger=None,
            handlers: Dict[bytes, RequestHandler]={},
            **kwargs
    ):

        super().__init__(channel, logger=logger, **kwargs)
        self.__listening_task = asyncio.create_task(
            self.__listen_and_close(handlers)
        )

    async def __listen_and_close(self, handlers: Dict[bytes, RequestHandler]):
        try:
            await self.listen(handlers)
        except asyncio.CancelledError:
            pass
        except BaseException as err:
            self.logger.exception(err)
        await super().close()

    async def listen(self, handlers: Dict[bytes, RequestHandler]):
        raise NotImplementedError()

    async def close(self):
        await super().close()
        if self.__listening_task:
            task = self.__listening_task
            self.__listening_task = None
            task.done() or task.cancel()
            await task
        

class SimpleServerSession(ActiveSession):
    """
    A session used for a simple client-server communication:
    - requests are processed sequentially
    - the server only responses to requests
    - a response starts with a error code (0 for no error)
    """

    __next_msg_id: int = 0

    @classmethod
    def msg_id(cls) -> int:
        id = cls.__next_msg_id
        cls.__next_msg_id += 1
        return id

    async def listen(self, handlers: Dict[bytes, RequestHandler]):
        async for msg in self.channel:
            try:
                req = self.process(msg)
                self.logger.debug("[msg {}] received {}...".format(req.id, repr(msg[:24])))
                handler = handlers.get(req.cmd)
                if not callable(handler): raise UnknownCommand(req.cmd)
                self.logger.debug("[msg {}] processing with {}".format(req.id, handler.__name__))
                response = await handler(self, req.data)
                if response is not None:
                    await self.respond(req, response)
            except (RequestFailedError, AuthenticationError, TokenLoadingError) as err:
                await self.reject(req, err)

    def process(self, msg: bytes) -> RequestInfo:
        return RequestInfo(cmd=msg[:4], id=self.msg_id(), data=msg[4:])
        
    async def respond(self, req: RequestInfo, response: bytes):
        self.logger.debug("[msg {}] responds with {}...".format(req.id, repr(response[:24])))
        await self.channel.send(b'\x00' + response)

    async def reject(self, req: RequestInfo, error: ApplicationError):
        self.logger.debug("[msg {}] rejects with code {}: {}".format(
            req.id, error.code, str(error)
        ))
        await self.channel.send(error.code.to_bytes(1, 'little') + str(error).encode())


class SimpleClientSession(Session):
    """
    A session used for a simple client-server communication:
    - requests are processed sequentially
    - the server only responses to requests
    - a response starts with a error code (0 for no error)
    """

    async def notify(self, cmd: bytes, data: Optional[bytes] = None) -> None:
        msg = cmd if data is None else cmd + data
        await self.channel.send(msg)

    async def request(self, cmd: bytes, data: Optional[bytes] = None, /, timeout: float = 5.0) -> bytes:
        await self.notify(cmd, data)
        response = await self.channel.recv(wait_time=timeout)
        if response[0]:
            raise RequestFailedError(response[0], response[1:].decode())
        return response[1:]


REQ_HEADER = '<H4s'
REQ_HEADER_SIZE = calcsize(REQ_HEADER)
RESP_HEADER = '<Hb'
RESP_HEADER_SIZE = calcsize(RESP_HEADER)
MIN_HEADER_SIZE = min(REQ_HEADER_SIZE, RESP_HEADER_SIZE)


class AsyncSession(ActiveSession):
    """
    A session used to a client-server communicatation where
    - a server can send notifications beyond responses
    - notifications are sent in parallel to responses
    - messages are prefixed with incremented id's
    """

    SERVER_SIDE = 0
    CLIENT_SIDE = 1

    def __init__(
            self,
            channel: Channel,
            side: int = SERVER_SIDE,
            ping_interval: float = 5.0,
            **kwargs
    ):
        super().__init__(channel, **kwargs)
        self.__side = side
        self.__next_msg_id = side
        self.__last_req_id = -1
        self.__fut_responses: Dict[int, asyncio.Future] = {}
        self.__request_tasks = TaskCollection()

    def new_msg_id(self) -> int:
        id = self.__next_msg_id
        self.__next_msg_id += 2
        return id
    
    def is_valid_request_id(self, req_id: int) -> bool:
        valid = req_id > self.__last_req_id
        if valid:
            self.__last_req_id = req_id
        return valid
    
    async def close(self):

        async def cancel(waiter: asyncio.Future):
            waiter.cancel()
            await waiter

        # Close the session
        await super().close()
        # Cancel all awaited responses
        await asyncio.gather(
            *map(cancel, self.__fut_responses.values()),
            return_exceptions=True
        )
        self.__fut_responses.clear()
        # Let request processing tasks react to cancellations
        await asyncio.sleep(0)
        await self.__request_tasks.clear()

    async def listen(self, handlers: Dict[bytes, RequestHandler] = {}):

        async def handle(req: RequestInfo):
            try:
                handler = handlers.get(req.cmd)
                if not callable(handler):
                    self.logger.debug(f'unknown command: {req.cmd}')
                else:
                    response = await handler(self, req.data)
                    if response is not None: await self.respond(req, response)
            except RequestFailedError as err:
                await self.reject(req, err)
            except Exception as err:
                self.logger.exception(err)
                await self.reject(req, RequestFailedError(500, "internal error"))


        async for msg in self.channel:
            req = self.process(msg)
            if req: self.__request_tasks.create_task(
                handle(req),
                name = "{}({})".format(req.cmd.decode(), req.id)
            )
                
    def process(self, msg: bytes) -> Optional[RequestInfo]:
        # Recall that the first bytes encode the message id
        # in little endian convention
        self.logger.debug('processing {}...'.format(msg[:24]))
        if len(msg) < RESP_HEADER_SIZE:
            return None
        elif (msg[0] & 0x01 == self.__side):
            # a response or an error
            msg_id, error_code = unpack_from(RESP_HEADER, msg)
            # validate
            response_waiter = self.__fut_responses.pop(msg_id, None)
            if response_waiter:
                self.logger.debug(f'received a response to {msg_id}: {error_code}')
                if error_code == 0:
                    response_waiter.set_result(msg[RESP_HEADER_SIZE:])
                else:
                    response_waiter.set_exception(
                        RequestFailedError(error_code, msg[RESP_HEADER_SIZE:].decode())
                    )
            else:
                self.logger.debug(f'ignoring a message - bad response id')
        elif len(msg) >= REQ_HEADER_SIZE:
            msg_id, cmd = unpack_from(REQ_HEADER, msg)
            if self.is_valid_request_id(msg_id):
                self.logger.debug(f'received a request {msg_id}: {cmd}')
                return RequestInfo(
                    id=msg_id,
                    cmd=cmd,
                    data=msg[REQ_HEADER_SIZE:]
                )
            else:
                self.logger.debug(f'ignoring a message - bad request id')
        return None
    
    async def respond(self, req: RequestInfo, response: bytes):
        self.logger.debug(f'responding to {req.cmd}, id={req.id}')
        await self.channel.send(pack(RESP_HEADER, req.id, 0) + response)

    async def notify(self, cmd: bytes, data: Optional[bytes] = None):
        msg_id = self.new_msg_id()
        msg = pack(REQ_HEADER, msg_id, cmd)
        if data: msg += data
        self.logger.debug(f'sending notification {cmd}, id={msg_id}')
        await self.channel.send(msg)

    async def reject(self, req: RequestInfo, error: ApplicationError):
        self.logger.debug(f'rejecting {req.cmd}, id={req.id} with {error}')
        await self.channel.send(
            pack(RESP_HEADER, req.id, error.code) + str(error).encode()
        )

    async def request(self, cmd: bytes, data: Optional[bytes] = None, /, timeout: float = 5.0) -> bytes:
        msg_id = self.new_msg_id()
        future_response = asyncio.Future()
        self.__fut_responses[msg_id] = future_response
        msg = pack(REQ_HEADER, msg_id, cmd)
        if data: msg += data
        self.logger.debug(f'requesting {cmd}, id={msg_id}')
        await self.channel.send(msg)
        return await future_response
