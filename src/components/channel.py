"""
project: ThinLink remote webcam
author: Krzysztof Putyra
date: 03/04/2023

Implementation of raw communication channels. These classes ensures that
whole messages are sent and received, but they do not process them.
"""

import asyncio
from pyee.asyncio import AsyncIOEventEmitter
import struct
from components.errors import ChannelClosedError, ProtocolError

from contextlib import asynccontextmanager


HEADER_SIGNATURE = b'TLW1'
HEADER_FORMAT = '=4sI'
HEADER_SIZE = struct.calcsize(HEADER_FORMAT)


class Channel(AsyncIOEventEmitter):
    """
    A two-way channel following a basic protocol to ensure
    that all messages are sent and received completely.
    """

    EV_CLOSE = 'close'

    def __init__(
        self,
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter
    ):
        super().__init__()
        self.reader = reader
        self.writer = writer
        self.__closed = asyncio.Event()

    @property
    def remote_name(self):
        return self.writer.get_extra_info('peername')

    @property
    def local_name(self):
        return self.writer.get_extra_info('sockname')

    @property
    def is_closing(self):
        """Returns `True` if the channel is closing or has been closed."""
        return self.writer is None
    
    @property
    def is_closed(self):
        """Returns `True` if the channel has been closed."""
        return self.__closed.is_set()
    
    @property
    async def wait_closed(self):
        await self.__closed.wait()

    async def close(self):
        """
        Closes the channel and waits until it is closed.
        This method does not raise.
        """

        if self.is_closing:
            await self.__closed.wait()
        else:
            writer = self.writer
            self.writer = None
            self.reader = None
            try: # This may fail (BrokenPipeError)
                writer.close()
                await writer.wait_closed()
            except BaseException:
                pass
            self.__closed.set()
            self.emit(self.EV_CLOSE)


    async def recv(
            self, /,
            wait_time: float = 0,
            message_timeout: float = 1.0
    ) -> bytes:
        """
        Waits for a message on the channel and raises ChannelClosedError
        when the channel is closed before a message is read.
        May raise other exceptions.
        """

        if self.reader is None:
            raise ChannelClosedError()
        # Read the message header. TimeoutError does not close the channel
        try:
            # The value of header is used to determine how to handle TimeoutError:
            # the channel is closed if header is not None
            #
            # Note: it is possible that readexactly() start receiving data
            # before the timeout happens. In such a case the channel becomes
            # unusable and it should be closed. With the current approach we
            # cannot detect it and the channel is kept open instead. The next
            # read will raise ProtocolError and close the channel.
            # Such an event is very rare to happen, so that we prefer to keep
            # the code easier to read instead of fighting it.
            header = None
            future_header = self.reader.readexactly(HEADER_SIZE)
            if wait_time > 0:
                future_header = asyncio.wait_for(future_header, wait_time)
            header = await future_header
            signature, msg_size = struct.unpack(HEADER_FORMAT, header)
            if signature != HEADER_SIGNATURE:
                raise ProtocolError("invalid header")
            # The message should be received immediately with the header.
            # Hence, we do not wait long for it in case it was broken.
            return await asyncio.wait_for(
                self.reader.readexactly(msg_size),
                message_timeout
            )
        except Exception as err:
            # Propagate TimeoutError when received during reading a header
            # If a header has been read, the TimeoutError indicates a serious
            # problem with the connection that breaks the protocol, so that
            # the channel will be closed.
            if isinstance(err, asyncio.TimeoutError) and header is None:
                raise
            await self.close()
            raise ChannelClosedError() if isinstance(err, (
                asyncio.IncompleteReadError,
                asyncio.TimeoutError,
                ConnectionResetError
            )) else err


    async def send(self, data: bytes):
        """
        Sends a message on the channel and raises ChannelClosedError
        if the channel is closed before a message is sent completely.
        """

        if self.writer is None: raise ChannelClosedError()
        header = struct.pack(HEADER_FORMAT,
                             HEADER_SIGNATURE,
                             len(data))
        self.writer.write(header)
        self.writer.write(data)
        try: # This may fail (BrokenPipeError)
            await self.writer.drain()
        except Exception:
            await self.close()
            raise ChannelClosedError()

    def __aiter__(self):
        return self

    async def __anext__(self):
        if self.is_closed: raise StopAsyncIteration
        try:
            return await self.recv()
        except ChannelClosedError:
            raise StopAsyncIteration

    async def __aenter__(self):
        return self
    
    async def __aexit__(self, exc_type, exc, tb):
        await self.close()



@asynccontextmanager
async def connect_host(host: str, port: int) -> Channel:
    reader, writer = await asyncio.open_connection(host, port)
    channel = Channel(reader, writer)
    try:
        yield channel
    finally:
        await channel.close()

@asynccontextmanager
async def connect_usocket(path: str) -> Channel:
    reader, writer = await asyncio.open_unix_connection(path)
    channel = Channel(reader, writer)
    try:
        yield channel
    finally:
        await channel.close()

