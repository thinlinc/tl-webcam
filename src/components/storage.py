from typing import Iterator
from os.path import exists
from struct import pack_into, iter_unpack

class ClientStorage:
    def __init__(self, path: str):
        self.path = path

    def store(self, port: int):
        if port <= 0 or port > 65535:
            raise ValueError('invalid port number')
        data = bytearray(16)
        if exists(self.path):
            with open(self.path, 'rb+') as storage:
                storage.readinto(data)
                for offset in range(0, 16, 2):
                    if (data[offset] | data[offset+1]) == 0:
                        pack_into("=H", data, offset, port)
                        storage.seek(0)
                        storage.write(data)
                        return
            raise Exception('too many connections')
        else:
            pack_into('=H', data, 0, port)
            with open(self.path, 'wb') as storage:
                storage.write(data)

    def delete(self, port: int = 0):
        if not exists(self.path): return
        with open(self.path, 'rb+') as storage:
            data = bytearray(16)
            if port == 0:
                storage.write(bytearray(16))
            else:
                storage.readinto(data)
                storage.seek(0)
                for offset, saved_port in zip(range(0,16,2), iter_unpack("=H", data)):
                    if saved_port[0] == port:
                        data[offset:offset+2] = b'\x00\x00'
                        storage.write(data)
                        break

    def __iter__(self) -> Iterator[int]:
        try:
            with open(self.path, 'rb') as storage:
                data = storage.read()
            for port in iter_unpack("=H", data):
                if port[0]: yield port[0]
        except Exception:
            pass
