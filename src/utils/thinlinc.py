from typing import Optional, List, Dict, Any
from xmlrpc.client import ServerProxy

from .hiveconf import open_hive
from constants import THINLINC_PATH
from components.errors import ConnectionError, ApplicationError

HIVECONF_PATH = THINLINC_PATH + "/thinlinc.hconf"
THINLINC_VAR_PATH = '/var/opt/thinlinc/'
THINLINC_SESSION_DIR = '/var/opt/thinlinc/sessions/{}/last/'
THINLINC_SESSION_TOKEN = '/var/opt/thinlinc/sessions/{}/last/sessionkey'

SessionInfo = Dict[str, Any]

def get_session_info(username: str, hive=None) -> List[SessionInfo]:
    hive = hive or open_hive(HIVECONF_PATH)
    master = hive.get_string("/vsmagent/master_hostname", "localhost")
    port = hive.get_integer("/vsm/vsm_server_port", 9000)
    server = None
    try:
        server = ServerProxy(f"http://{master}:{port}")
        response = server.get_public_sessioninfo(username, "", -1)
    except Exception as err:
        raise ConnectionError(master, str(err))
    finally:
        if server: server('close')()
    if not isinstance(response, dict):
        raise ApplicationError('bad response from VSM server')
    error_code = response['errcode']
    if error_code:
        raise ApplicationError('VSM server responded with {error_code}')
    return response['result']

def get_client_ip(username: str) -> Optional[str]:
    info = get_session_info(username)
    return None if len(info) == 0 else info[0]['client_ip']

def get_session_dir(username: str) -> str:
    return THINLINC_SESSION_DIR.format(username)

def get_session_token(username: str) -> str:
    return THINLINC_SESSION_TOKEN.format(username)
