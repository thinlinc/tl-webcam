"""
ThinLinc Webcam
aythor: Krzysztof Putyra
date: 29/03/2023

A collection of functions performing small tasks.
"""

import os
import os.path
import subprocess
from stat import S_IRUSR, S_IWUSR
from typing import Set

#********************************#
# Properties of the current user #
#********************************#

if hasattr(os, 'getuid'):
    import pwd

    def current_user() -> str:
        """Returns the name of the current user"""
        return pwd.getpwuid(os.getuid()).pw_name

else:

    def current_user() -> str:
        """Returns the name of the current user"""
        return os.getlogin() # getpwuid(getuid()).pw_name


def logged_users() -> Set[str]:
    output = subprocess.run(['who', '-q'], text=True, stdout=subprocess.PIPE)
    return set(output.stdout.split('\n')[0].split())

def hostname() -> str:
    return subprocess.run(['hostname'], text=True, capture_output=True).stdout.strip()

#***************************#
# Loading and saving tokens #
#***************************#

def load_token(path: str) -> bytes:
    """Reads a token from a file. Raises FieNotFoundError or PermissionError on a failure."""
    with open(path, "rb") as file:
        return file.read()

def save_token(path: str, token: bytes) -> None:
    # create a token
    with open(path, "wb") as file:
        file.write(token)
    # The file can be accessed only by this user
    os.chmod(path, S_IRUSR | S_IWUSR)


#***************************#
# TCP/IP network properties #
#***************************#

# This works only in Linux!!!
def own_ip():
    output = subprocess.run(
        'ip a | egrep -o "([[:digit:]]{1,3}\.){3}[[:digit:]]{1,3}"',
        shell=True,
        capture_output=True,
        text=True
    )
    return output.stdout.strip().split("\n")
