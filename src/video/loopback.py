"""
project: ThinLink remote webcam
author: Krzysztof Putyra
date: 8/03/2023

Managing loopback devices with v4l2loopback-ctl
"""

import asyncio
from asyncio.subprocess import create_subprocess_shell, PIPE, STDOUT
from shutil import chown
from os import chmod, stat, listdir
from stat import S_IRUSR, S_IWUSR, S_IXUSR
from pwd import getpwuid
from typing import Iterable, Tuple, Dict, List, NamedTuple
import logging

from constants import V4L2LOOPBACK_CTL, SYSDEVICE_PATH

logger = logging.getLogger('v4l2')


class V4L2Error(Exception):
    pass


def get_owner(path: str) -> Tuple[int, str]:
    """Returns the name of the owner of the file"""
    uid = 0
    name = ''
    try:
        uid = stat(path).st_uid
        name = getpwuid(uid).pw_name
    except Exception:
        pass
    return uid, name


async def v4l2loopback_ctl(cmd: str, *args) -> str:
    """
    Calls v4l2loopback-ctl with given parameters and returns the result.
    Raises V4L2Error on an error
    """

    logger.debug(f"running v4l2loopback {cmd}")
    process = await asyncio.subprocess.create_subprocess_exec(
        V4L2LOOPBACK_CTL, cmd, *args,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE
    )
    stdout, stderr = await process.communicate()
    output = stdout.decode().strip() if stdout else ""
    error = stderr.decode().strip() if stderr else ""
    #if process.returncode == 0:
    if process.returncode == 0 or (output and output.startswith("/dev/video")):
        logger.info(f"{cmd} -> {output} (code: {process.returncode})")
        return output
    else:
        error = stderr.decode().strip()
        logger.warning(f"{cmd} -> {error} (code: {process.returncode}).")
        raise V4L2Error(process.returncode, error)


async def create(username: str) -> int:
    """
    Creates a video loopback device for a user and returns its id.
    Raises V4L2Exception on an error
    """

    # The result is a path to the newly created loopback device
    device = await v4l2loopback_ctl('add',
        "-n", "Remote camera",
        "-x", "1",
        "-o", "10"
    )
    # Only the user can access the device
    chown(device, username, username)
    chmod(device, S_IRUSR | S_IWUSR)
    # Only the user can access the device folder
    sysdev = SYSDEVICE_PATH + device[5:]
    chown(sysdev, username, username)
    chmod(sysdev, S_IRUSR | S_IWUSR | S_IXUSR)
    return int(device[10:])


async def delete(device_id: int):
    """
    Deletes a video loopback device with a given id.
    Raises V4L2Exception on an error
    """

    await v4l2loopback_ctl('delete', str(device_id))


def enumerate() -> Iterable[Tuple[int, int, str]]:
    """
    Enumerates video loopback devices together with their owners.
    """

    # Each loopback device has a folder under
    #    /sys/devices/virtual/video4linux
    # owned by the associated user. However, the folder
    # does not exists if there are no loopack devices created,
    # even when the module v4l2loopback is loaded.
    try:
        # This raises an exception when the directory does not exist
        for path in listdir(SYSDEVICE_PATH):
            owner_uid, owner_name = get_owner(SYSDEVICE_PATH + path)
            yield int(path[5:]), owner_uid, owner_name
    except FileNotFoundError:
        pass


class DeviceAccessor(NamedTuple):
    process: str
    pid: int
    user: str
    access: str


async def accessors() -> Dict[int, List[DeviceAccessor]]:

    try:
        logger.debug('polling loopback devices with fuser')
        fuser = await create_subprocess_shell('fuser -v /dev/video*', stdout=PIPE, stderr=STDOUT)
        stdout, _ = await fuser.communicate()
    except Exception as err:
        logger.warning(f'polling has failed: {err!r}')
        raise

    try:
        files = {}
        if fuser.returncode == 0:
            current_list = None
            for line in stdout.decode().splitlines():
                if line.startswith('/dev/video'):
                    device_name, user, pid, access, command = line.split()
                    device_index = int(device_name[10:-1])
                    current_list = files.get(device_index)
                    if not current_list:
                        current_list = list()
                        files[device_index] = current_list
                elif current_list:
                    user, pid, access, command = line.split()
                else:
                    continue
                current_list.append(DeviceAccessor(command, int(pid), user, access))
        return files
    except Exception as err:
        logger.warning(f'invalid format of fuser output: {err!r}. The output of fuser:\n{stdout.decode(errors="ignore")}')
        raise
