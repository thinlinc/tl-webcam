#!/bin/python3
"""
ThinLinc webcam
author: Krzysztof Putyra
date: 29/03/2023

A program to communicate with tlwebcam daemon processes
- loopback device manager (tlwservc)
  - create/delete a loopback device for a user
  - list current loopback devices
  - unload the daemon process
  - service status
- webrtc streamer (tlwrecvr)
  - enumerate available cameras
  - list formats of a camera
  - stream from a camera
  - stop streaming
  - streaming status
"""

from constants import SERVICE_CHANNEL, SERVICE_AUTH_TOKEN,\
                      USER_CHANNEL, USER_AUTH_TOKEN
import commands as cmd
from app import CtlApp, Command, Argument
import app.formatter as formatter

app = CtlApp('tlwebcam-ctl', "A controller for ThinLink Webcam.")
app.forward_requests({
    "create-device": Command(cmd.GET_DEVICE,
        help="Returns the index of a video loopback device associated with the user. "
              "A device is created when none exists.",
        arguments=(
          Argument("username",
                    help="the owner of the device (default: the current user)",
                    default=app.username
          ),
        ),
        formatter=lambda b: f"/dev/video{b[0]}"
    ),
    "delete-device": Command(cmd.DELETE_DEVICE,
        help="Deletes a loopback device associated with the user and does nothing "
            "if no such device exists.",
        arguments=(
          Argument("username",
                  help="the owner of the device to be deleted (default: the current user)",
                  default=app.username
          ),
        ),
        formatter=lambda b: f"removed /dev/video{b[0]}",
        onempty="no device to delete"
    ),
    "list-devices": Command(cmd.LIST_DEVICES,
        help="Returns a list of all loopback devices associated with a user (all users in the admin mode).",
        arguments=(
          Argument("username",
                  help="the owner of the devices (default: the current user)",
                  default=app.username
          ),
        ),
        formatter=formatter.device_list,
        onempty="no device detected"
    ),
    "update-service": Command(cmd.UPDATE,
        help="Requests the service to updates its data.",
        formatter=formatter.packed("=BBB", "active", "found new", "removed")
    ),
    "reload": Command(cmd.RELOAD,
        help="reloads the user service",
        arguments=(
          Argument("username",
                   help="the user, whose service should be reloaded",
                   default=app.username
          ),
        )
    ),
    "unload-service": Command(cmd.UNLOAD,
        help="Requests to stop the service.",
        restricted=True
    )
}).to(
    addr=SERVICE_CHANNEL,
    admin_token=SERVICE_AUTH_TOKEN,
    user_token=USER_AUTH_TOKEN.format(app.username)
)
app.forward_requests({
    "list-cameras": Command(cmd.LIST_CAMERAS,
        help="Lists available cameras",
        formatter=formatter.camera_list,
        onempty="No available camera"
    ),
    "reload-cameras": Command(cmd.RELOAD_CAMERAS,
        help="reloads the camera service on the connected client"
    ),
    "list-formats": Command(cmd.LIST_FORMATS,
        help="Lists formats supported by a specified camera",
        formatter=formatter.format_list,
        arguments=(
            Argument("camera-id", help="the id of the camera"),
        )
    ),
    "status": Command(cmd.STATUS,
        help="streaming status"
    ),
    "stream": Command(cmd.STREAM,
        help="starts a stream from a specified camera",
        arguments=(
            Argument("camera-id", help="the id of the camera to be streamed", default=''),
        ),
        timeout=10.0
    ),
    "stop": Command(cmd.STOP,
        help="stops the current stream"
    ),
    "unload": Command(cmd.UNLOAD,
        help="unloads the service"
    )
}).to(USER_CHANNEL.format(app.username))
exit(app.run())
