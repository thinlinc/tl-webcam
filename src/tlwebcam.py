#!/bin/python3
"""
A service for writing a camera stream into a loopback device.
This service runs in a user session and communicates with
- tlwsrvc: for creating and deleting a loopback device
- tswebcam: for gathering camera info and streaming

"""

import logging

from app import ServiceApp
from app.controller import make_request

from components.errors import *
from components.session import Session
from service.rtcreceiver import StreamReceiver
from service.video_feeder import CameraFeeder, VideoOutput

from utils import current_user, load_token

import commands as cmd
from constants import USER_RUNNING_DIR, USER_CHANNEL, SERVICE_CHANNEL, USER_AUTH_TOKEN, SPLASH_IMAGE
from constants.video import CAMERA_HEIGHT, CAMERA_WIDTH


import argparse

def bool2str(value):
    return 'yes' if value else 'no'

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument(
    "port",
    help="The port on which the service listens for camera providers",
    type=int
)
args = parser.parse_args()

TLWEBCAM_HOST = 'localhost'
TLWEBCAM_PORT = args.port

del parser
del args

logging.getLogger('aiortc').setLevel(logging.INFO)
logging.getLogger('aioice').setLevel(logging.INFO)


class TargetProvider(VideoOutput):
    def __init__(self, username: str, channel: str):
        super().__init__()
        self.username = username.encode()
        self.token = USER_AUTH_TOKEN.format(username)
        self.channel = channel

    async def acquire(self) -> str:
        response = await make_request(
            self.channel,
            cmd.GET_DEVICE,
            self.username,
            load_token(self.token)
        )
        return f"/dev/video{response[0]}"

#    async def release(self) -> None:
#        await make_request(
#            self.channel,
#            cmd.DELETE_DEVICE,
#            self.username,
#            self.token
#        )

username=current_user()

app = ServiceApp('tlwebcam',
    running_dir=USER_RUNNING_DIR.format(username),
    token_path=USER_AUTH_TOKEN.format(username),
    username=username
)
camera_feeder = CameraFeeder(
    TargetProvider(username, SERVICE_CHANNEL),
    name='feeder',
    width=CAMERA_WIDTH,
    height=CAMERA_HEIGHT,
    splash=SPLASH_IMAGE
)
stream_receiver = StreamReceiver(TLWEBCAM_HOST, TLWEBCAM_PORT)
stream_receiver.feeder = camera_feeder

async def list_cameras(session: Session, data: bytes) -> bytes:
    if stream_receiver.is_connected:
        return await stream_receiver.session.request(cmd.LIST_CAMERAS)
    else:
        return b''

async def list_formats(session: Session, cam_id: bytes) -> bytes:
    if stream_receiver.is_connected:
        return await stream_receiver.session.request(cmd.LIST_FORMATS, cam_id)
    else:
        raise RequestFailedError(40, 'not connected')

async def update_captured_status(session: Session, data: bytes) -> bytes:
    if data[0] == 0:
        camera_feeder.captured = False
        await stop_stream(session, b'')
    elif data[0] == 1:
        camera_feeder.captured = True
        await stream_camera(session, b'')

async def stop_stream(session: Session, data: bytes) -> bytes:
    app.stream_requested = False
    if stream_receiver.is_connected:
        await stream_receiver.stop_streaming()
    return b''

async def stream_camera(session: Session, data: bytes) -> bytes:
    app.stream_requested = True
    if stream_receiver.is_connected:
        return await stream_receiver.session.request(cmd.STREAM, data)
    else:
        raise RequestFailedError(40, 'not connected')

async def stream_status(session: Session, data: bytes) -> bytes:
    return b''

async def unload_service(session: Session, data: bytes) -> bytes:
    app.stop(data[0] if data else 0)
    return b''

async def new_camera(session: Session, data: bytes) -> None:
    app.logger.debug(f'handling a new camera (pending stream request: {bool2str(app.stream_requested)})')
    if app.stream_requested and not stream_receiver.is_streaming:
        await stream_receiver.session.request(cmd.STREAM, data)

async def reload_cameras(session: Session, data: bytes) -> bytes:
    if stream_receiver.is_connected:
        await stream_receiver.session.notify(cmd.RELOAD)
        return b'\x01'
    return b'\x00'

@stream_receiver.on(StreamReceiver.EV_CONNECTION)
async def onconnected(session: Session):
    app.logger.debug(f'handling a new connection (pending stream request: {bool2str(app.stream_requested)})')
    if app.stream_requested:
        app.logger.info('new connection when a stream has been requested')
        await stream_receiver.session.request(cmd.STREAM)

app.stream_requested = False

app.services.add(camera_feeder)
app.services.add(stream_receiver)
app.add_controller(USER_CHANNEL.format(username), handlers={
    cmd.LIST_CAMERAS: list_cameras,
    cmd.LIST_FORMATS: list_formats,
    cmd.RELOAD_CAMERAS: reload_cameras,
    cmd.STREAM: stream_camera,
    cmd.STATUS: stream_status,
    cmd.STOP: stop_stream,
    cmd.UNLOAD: unload_service,
    cmd.CAPTURED: update_captured_status
}, private=True)

stream_receiver.add_handlers({
    cmd.CAMERA_ATTACHED: new_camera
})

exit(app.run())

