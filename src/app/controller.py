import asyncio
import argparse
import logging

from typing import Optional, Iterable, Callable, Dict, List, Tuple, NamedTuple, Any

from dataclassy import dataclass

from utils import current_user, load_token, hostname
from commands import AUTHENTICATE
from components import Channel, create_logger
from components.session import SimpleClientSession
from components.errors import ApplicationError, ConnectionError, TokenLoadingError


class Argument(NamedTuple):
    name: str
    required: bool = True
    default: Any = None
    help: str = ""
    encoder: Any = None
    type: Any = str

class Command(NamedTuple):
    request: bytes
    restricted: bool = False
    arguments: Optional[Tuple[Argument]] = None
    formatter: Callable[[bytes], str] = lambda x: x.decode()
    onempty: str = ""
    timeout: float = 2.0
    help: str = ""
    preaction: Optional[Callable[[Any], None]] = None


RequestDict = Dict[str, Command]

@dataclass(slots=True)
class RequestContext:
    addr: str = ""
    admin_token: Optional[str] = None
    user_token: Optional[str] = None
    handler: Optional[Callable] = None


class RequestChannelSetter:
    def __init__(self, ctx: RequestContext):
        self.ctx = ctx

    def to(self, addr, *, admin_token=None, user_token=None):
        self.ctx.addr = addr
        self.ctx.admin_token = admin_token
        self.ctx.user_token = user_token
        return self
    
    def using(self, handler: Callable):
        self.ctx.handler = handler
        return self


class RequestCollection(List[Tuple[RequestContext, RequestDict]]):
    def items(self) -> Iterable[Tuple[str, Command]]:
        for _, requests in self:
            for name, request in requests.items():
                yield name, request

    def get(self, request_name: str) -> Tuple[Optional[Command], Optional[RequestContext]]:
        for ctx, requests in self:
            request = requests.get(request_name)
            if request: return request, ctx
        return None, None
    
    def keys(self) -> Iterable[str]:
        for _, requests in self:
            for request in requests:
                yield request

    def append(self, ctx: RequestContext, requests: RequestDict):
        super().append((ctx, requests))

    def __contains__(self, name: str) -> bool:
        return any(name in requests for _, requests in self)


class CtlApp:

    def __init__(
            self,
            name: str,
            description: str
    ):
        self.name = name
        self.requests = RequestCollection()
        self.description = description
        self.logger = logging.getLogger('app')
        self.username = current_user()
        self.hostname = hostname()

    def forward_requests(self, requests: RequestDict) -> RequestChannelSetter:
        request_ctx = RequestContext()
        self.requests.append(request_ctx, requests)
        return RequestChannelSetter(request_ctx)
    
    def parse_cli_arguments(self) -> object:

        # create a parser
        parser = argparse.ArgumentParser(
            prog=self.name,
            description=self.description
        )
        parser.add_argument(
            "-a", "--admin",
            help="Make the request as an admin. This requires read permission for the file with the app token.",
            action="store_true"
        )

        commands = parser.add_subparsers(
            title="A list of allowed requests",
            metavar="request",
            dest="request",
            required=True,
            parser_class=argparse.ArgumentParser
        )

        for ctx, req_list in self.requests:
            for name, command in req_list.items():
                cmd_parser = commands.add_parser(
                    name, help=command.help #, parents=[common_options]
                )
                cmd_parser.set_defaults(command=command, context=ctx)
                if command.arguments is None: continue
                for arg in command.arguments:
                    req_arg = cmd_parser.add_argument(
                        arg.name,
                        help=arg.help,
                        type=arg.type or str
                    )
                    if arg.default is not None:
                        req_arg.default = arg.default
                        req_arg.nargs = '?'
                    elif not arg.required:
                        req_arg.nargs = '?'

        # parse command-line arguments
        return parser.parse_args()


    def run(self):
        # Parse arguments
        args = self.parse_cli_arguments()
        as_admin: bool = args.admin
        command: Command = args.command
        context: RequestContext = args.context
        argument = b''

        if command.arguments:
            raw_data = list()
            data = list()
            for arg_def in command.arguments:
                value = getattr(args, arg_def.name)
                data.append(value)
                if not isinstance(value, str):
                    value = str(value)
                raw_data.append(value.encode())
            argument = b''.join(raw_data)

        # create logger
        create_logger(
            username=self.username,
            hostname=self.hostname,
            appname=self.name
        )
        try:
            self.logger.info("processing '{} {}' as {}".format(
                command.request.decode(),
                argument,
                self.username
            ))
            
            # Load the authentication token
            try:
                if as_admin or command.restricted:
                    token = context.admin_token and load_token(context.admin_token)
                else:
                    token = context.user_token and load_token(context.user_token)
            except (FileNotFoundError, PermissionError) as err:
                raise TokenLoadingError(str(err))
            
            # run a preaction
            callable(command.preaction) and command.preaction(*data)

            # run with the arguments
            response = asyncio.run(make_request(
                context.addr,
                command.request,
                argument,
                token,
                command.timeout
            ))
            # print the response and return the code
            self.logger.info(f"got response: {repr(response)}")
            if response:
                print(command.formatter(response))
            elif command.onempty:
                print(command.onempty)
            return 0
        
        except ApplicationError as err:
            self.logger.warning("the request has failed with exit code {}: {}".format(err.code, str(err)))
            print("error: " + str(err))
            return err.code
        except (TimeoutError, asyncio.TimeoutError) as err:
            self.logger.warning(f'the request has timed out: {err}')
            print("error: no response from the service")
            return 254
        except Exception as err:
            self.logger.exception(err)
            print("error: the application has crushed. Please contact IT support")
            return 255
    

async def make_request(
        path: str,
        name: bytes,
        argument: bytes,
        token: Optional[bytes] = None,
        timeout: float = 2.0
) -> bytes:
    try:
        reader, writer = await asyncio.open_unix_connection(path)
    except FileNotFoundError:
        raise ConnectionError(path, "the service is unreachable")
    except PermissionError:
        raise ConnectionError(path, "permission denied")
    
    async with SimpleClientSession(
        Channel(reader, writer),
        logger=logging.getLogger('request')
    ) as session:
        if token: await session.notify(AUTHENTICATE, token)
        response = await session.request(name, argument, timeout=timeout)
        return response
