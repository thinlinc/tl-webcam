#!/bin/sh

LOG_FILE="/var/log/tlwebcam.log"

touch $LOG_FILE
chmod a+rw $LOG_FILE

exec 1>>$LOG_FILE
exec 2>&1

if lsmod | grep videodev > /dev/null; then
  echo "[tlwsrvc] INFO videodev already loaded"
elif modprobe videodev > /dev/null; then
  echo "[tlwsrvc] INFO videodev successully loaded"
else
  echo "[tlwsrvc] ERROR Failed to load videodef"
  exit 1
fi

if lsmod | grep v4l2loopback > /dev/null; then
  echo "[tlwsrvc] INFO v4l2loopback already loaded"
elif modprobe v4l2loopback devices=0 > /dev/null; then
  [ -c /dev/v4l2loopback ] && echo "[tlwsrvc] INFO v4l2loopback successfully loaded" || \
  { echo "[tlwsrvc] ERROR invalid version of v4l2loopback module"; rmmod v4l2loopback; exit 1; }
else
  echo "[tlwsrvc] ERROR Failed to load v4l2loopback"
  exit 1
fi
echo "[tlwsrvc] INFO Starting the service application"
exec /usr/bin/python -s /opt/tlwebcam/tlwsrvc.py
