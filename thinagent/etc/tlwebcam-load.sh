#!/bin/bash

# ThinLinc Webcam
# author: Krzysztof Putyra
# date: 09/03/2023
#
# This script makes a request to the tlwebcam daemon to create
# a video loopback device for the current user. It should be
# located in /opt/thinlinc/etc/xstartup.d

LOG_FILE="/var/log/tlwebcam.log"

USERNAME=`id -un`
SLOTS=`tl-config /vsm/tunnelslots_per_session 2>/dev/null`
SERVICE_SLOT=`tl-config /vsm/tunnelservices/tlwebcam 2>/dev/null`
TLDISPLAY=`tl-session-param display 2>/dev/null`
TLWEBCAM_SOCKET=/var/opt/thinlinc/session/$USERNAME/last/tlwebcam/channel

[ "$TLDISPLAY" ] || {
  echo "This script can be run only from an active ThinLinc session"
  exit 1
}

[ -S $TLWEBCAM_SOCKET ] || {
  PROCESS=`ps -u $USERNAME -o pid,cmd --no-headers | grep tlwebcam | grep python`
  [ "$PROCESS" ] && {
    PID=`echo $PROCESS | cut -d ' ' -f 1`
    PORT=`echo $PROCESS | cut -d ' ' -f 4`
    echo "The service is already listening on port $PORT (pid: $PID)"
    exit 0
  }
}

[ "$SERVICE_SLOT" ] || {
  echo "No tunneling configured!"
  exit 1
}


echo "Initializing the service. Session display: $TLDISPLAY, service slot: $SERVICE_SLOT"

if [[ $TLDISPLAY -lt 100 ]]; then
  # Formula for tunnel port number for session with diplay < 100 from
  # https://www.cendio.com/resources/docs/tag/tcp-ports_agent.html
  BASE=`tl-config /vsm/tunnel_bind_base`
  PORT=$(($BASE + $TLDISPLAY * $SLOTS + $SERVICE_SLOT))
else
  # Formula for tunnel port number for session with display >= 100
  # https://www.cendio.com/resources/docs/tag/tcp-ports_agent.html
  BASE=`tl-config /vsmagent/max_session_port`
  PORT=$(($BASE - ($TLDISPLAY - 100) * ($SLOTS + 1) - 9 + $SERVICE_SLOT))
fi

echo "Starting to listen on port $PORT"
while [ 1 ]; do {
  /bin/python -s /opt/tlwebcam/tlwebcam.py $PORT >> $LOG_FILE 2>&1
  [ $? == 0 ] && exit 0
  sleep 1
}; done &
