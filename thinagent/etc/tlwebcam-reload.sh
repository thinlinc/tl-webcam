#!/bin/bash
USERID=`id -u`
if [ $USERID == 0 ]; then
    if [ -z "$1" ]; then
        USERNAMES=`who | grep thinlinc | cut -d ' ' -f 1`
    else
        USERNAMES=$@
    fi
    for USER in $USERNAMES; do
        tlwebcam-ctl -a reload $USER
    done
else
    tlwebcam-unload
    tlwebcam-load
fi
