#!/bin/bash
#
# Install: tl-webcam on vsmagent
#

TL_WEBCAM_MASTER=tl-webcam-master
TL_WEBCAM_REPO=https://git.math.uzh.ch/thinlinc/tl-webcam/-/archive/master/

SERVICE_SLOT=7
SERVICE_NAME=tlwebcam

REPO_DIR=/usr/src/tl-webcam-master
BIN_DIR=/usr/local/bin
SYSTEM_DIR=/etc/systemd/system
THINLINC_DIR=/opt/thinlinc
TARGET_DIR=/opt/$SERVICE_NAME

# Check if run by root
[[ `id -u` == '0' ]] || {
    echo "You must be a root to run this script!"
    exit 1
}

# Required packages. Ubuntu 22.04 needs 'python-is-python3'
apt install python-is-python3 pip

# Check if python, pip, v4l2loopback-ctl, and tl-config are available
for PROG in python pip v4l2loopback-ctl; do
    which $PROG > /dev/null || {
      echo "Missing $PROG - please check your OS and/or TL v4l-loopback installation."
      exit 1
    }
done

[ -f $THINLINC_DIR/bin/tl-config ] || {
    echo "Missing tl-config - please check your OS"
    exit 1
}

# Temp download dir
DIR=`mktemp -d '/tmp/tl-webcam.XXXX'`

#
# tlwebcam
#
cd $DIR
wget ${TL_WEBCAM_REPO}/${TL_WEBCAM_MASTER}.zip
unzip ${TL_WEBCAM_MASTER}.zip

# If old version exist: move it away
[ -d /usr/src/${TL_WEBCAM_MASTER} ] && mv /usr/src/${TL_WEBCAM_MASTER} /usr/src/${TL_WEBCAM_MASTER}.`date "+%Y%m%d.%H%M%S"`
mv ${TL_WEBCAM_MASTER} /usr/src/${TL_WEBCAM_MASTER}

# Update the configuration for vsm
# We first check that the slot is not already occupied
echo "Updating ThinLinc configuration"
CURRENT_SLOT=`tl-config /vsm/tunnelservices/tlwebcam 2>/dev/null`
if [ "$CURRENT_SLOT" != "$SERVICE_SLOT" ]; then
    for TAKEN_SLOT in `$THINLINC_DIR/bin/tl-config -a /vsm/tunnelservices | cut -d ' ' -f 3`; do
        [[ "$SLOT" == "$SERVICE_SLOT" ]] && {
            echo "The service slot $SERVICE_SLOT is already taken"
            exit 1
        }
    done
    $THINLINC_DIR/bin/tl-config /vsm/tunnelservices/$SERVICE_NAME=$SERVICE_SLOT
fi


# Stop the tlwebcam service
echo "Stopping the tlwebcam service"
systemctl stop $SERVICE_NAME

# Install python libraries
echo "Installing required python libraries"
for MODULE in `cat $REPO_DIR/thinagent/dependencies`; do
    echo "Installing $MODULE..."
    pip install -q $PYTHON3_LIB $MODULE
done

# Copy python files
echo "Installing tlwebcam files"
[ -d $TARGET_DIR ] && \rm -rf $TARGET_DIR/* || mkdir -p $TARGET_DIR
for FILE in `cat $REPO_DIR/thinagent/files`; do
    DIR=`dirname $TARGET_DIR/$FILE`
    mkdir -p $DIR
    cp $REPO_DIR/src/$FILE $DIR
done

# Copy the configuration file
mkdir -p $TARGET_DIR/constants
cp $REPO_DIR/src/constants/thinagent.py $TARGET_DIR/constants/__init__.py

# Copy special files and ensure that script files are executable
cp -r $REPO_DIR/thinagent/etc $TARGET_DIR/
chmod a+x $TARGET_DIR/etc/*.sh

# Create thinlinc hooks
echo "Creating ThinLinc xstartup and xlogout hooks"
ln -snf $TARGET_DIR/etc/tlwebcam-load.sh \
        $THINLINC_DIR/etc/xstartup.d/91-$SERVICE_NAME-load
ln -snf $TARGET_DIR/etc/tlwebcam-unload.sh \
        $THINLINC_DIR/etc/xlogout.d/91-$SERVICE_NAME-unload

# Create symbolic links for commands
echo "Installing console commands"
chmod a+x $TARGET_DIR/tlwebcam-ctl.py
ln -snf $TARGET_DIR/tlwebcam-ctl.py $BIN_DIR/$SERVICE_NAME-ctl

for CMD in load unload reload; do
    ln -snf $TARGET_DIR/etc/tlwebcam-$CMD.sh $BIN_DIR/$SERVICE_NAME-$CMD
done

# Install the service file
echo "Installing tlwebcam sevice"
ln -snf $TARGET_DIR/etc/tlwebcam.service $SYSTEM_DIR/$SERVICE_NAME.service

echo "Restarting the service"
systemctl daemon-reload
systemctl start $SERVICE_NAME

echo "Configuring autostart of the service"
systemctl enable tlwebcam.service

# That's it!
echo "Ready!"

