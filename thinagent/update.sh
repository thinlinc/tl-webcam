#!/bin/bash

SERVICE_SLOT=7
SERVICE_NAME=tlwebcam

# The installation script for tlwebcam on a ThinAgent

# Check if run by root
[[ `id -u` == '0' ]] || \
  { echo "You must be a root to run this script!"; exit 1; }

# Get script's directory
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR=`cd -P "$(dirname "$SOURCE")" && pwd`
  SOURCE=`readlink "$SOURCE"`
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIR=`cd -P "$(dirname "$SOURCE")" && pwd`

SOURCE_DIR=`dirname $SCRIPT_DIR`/src
ETC_DIR=$SCRIPT_DIR/etc
TARGET_DIR=/opt/$SERVICE_NAME
BIN_DIR=/usr/local/bin
SYSTEM_DIR=/etc/systemd/system
THINLINC_DIR=/opt/thinlinc

# Stop the tlwebcam service
echo "Stopping the tlwebcam service"
systemctl stop $SERVICE_NAME

# Install python libraries
#echo "Installing required python libraries"
#for MODULE in `cat $SCRIPT_DIR/dependencies`; do
#    echo "Installing $MODULE..."
#    pip install -q $PYTHON3_LIB $MODULE
#done

# Copy python files
echo "Installing tlwebcam files"
[ -d $TARGET_DIR ] && \rm -rf $TARGET_DIR/* || mkdir -p $TARGET_DIR
for FILE in `cat $SCRIPT_DIR/files`; do
    DIR=`dirname $TARGET_DIR/$FILE`
    mkdir -p $DIR
    cp $SOURCE_DIR/$FILE $DIR
done
# Copy the configuration file
mkdir -p $TARGET_DIR/constants
cp $SOURCE_DIR/constants/thinagent.py $TARGET_DIR/constants/__init__.py
# Copy special files
mkdir $TARGET_DIR/etc
cp $SCRIPT_DIR/etc/* $TARGET_DIR/etc

# Create thinlinc hooks
#echo "Creating ThinLinc xstarup and xlogout hooks"
#ln -snf $TARGET_DIR/etc/tlwebcam-load.sh \
#        $THINLINC_DIR/etc/xstartup.d/91-$SERVICE_NAME-load
#ln -snf $TARGET_DIR/etc/tlwebcam-unload.sh \
#        $THINLINC_DIR/etc/xlogout.d/91-$SERVICE_NAME-unload

# Create symbolic links
#echo "Installing console commands"
#ln -snf $TARGET_DIR/tlwebcam-ctl.py $BIN_DIR/$SERVICE_NAME-ctl
#for CMD in load unload reload; do
#    ln -snf $TARGET_DIR/etc/tlwebcam-$CMD.sh $BIN_DIR/$SERVICE_NAME-$CMD
#done

# Update the configuration for vsm
#echo "Updating ThinLinc configuration"
#tl-config /vsm/tunnelservices/$SERVICE_NAME=$SERVICE_SLOT

# Install the service file
#echo "Installing tlwebcam sevice"
#ln -snf $TARGET_DIR/etc/tlwebcam.service $SYSTEM_DIR/$SERVICE_NAME.service

echo "Restarting the service"
systemctl daemon-reload
systemctl start $SERVICE_NAME

# That's it!
echo "Ready!"
