#!/bin/bash

#
# Install: v4l-loopback kernel module, specific for tlwebcam
#

V4L_LOOPBACK_MASTER=v4l2loopback-master
V4L_LOOPBACK_REPO=https://git.math.uzh.ch/thinlinc/v4l2loopback/-/archive/master
V4L_LOOPBACK_VERSION=0.12.7

apt install dkms

# Temp download dir
DIR=`mktemp -d '/tmp/tl-v4l.XXXX'`

#
# V4L Loopback
#
cd $DIR
wget ${V4L_LOOPBACK_REPO}/${V4L_LOOPBACK_MASTER}.zip

# If old version exist: move it away
[ -d /usr/src/v4l2loopback-${V4L_LOOPBACK_VERSION} ] && mv /usr/src/v4l2loopback-${V4L_LOOPBACK_VERSION} /usr/src/v4l2loopback-${V4L_LOOPBACK_VERSION}.`date "+%Y%m%d.%H%M%S"`
unzip ${V4L_LOOPBACK_MASTER}.zip
mv ${V4L_LOOPBACK_MASTER} /usr/src/v4l2loopback-${V4L_LOOPBACK_VERSION}


# build and install with dkms
dkms add -m v4l2loopback -v ${V4L_LOOPBACK_VERSION}

# Creating symlink /var/lib/dkms/v4l2loopback/0.12.7/source -> /usr/src/v4l2loopback-0.12.7
ln -s /usr/src/v4l2loopback-${V4L_LOOPBACK_VERSION} /var/lib/dkms/v4l2loopback/${V4L_LOOPBACK_VERSION}/source

dkms build -m v4l2loopback -v ${V4L_LOOPBACK_VERSION}
dkms install -m v4l2loopback -v ${V4L_LOOPBACK_VERSION}

# check that the module is properly installed
modprobe v4l2loopback
modinfo v4l2loopback

# Check if device exist
ls /dev/v4l2loopback

# Load module during boot
echo "v4l2loopback" > /etc/modules-load.d/v4l2loopback.conf
echo "options v4l2loopback devices=0" > /etc/modprobe.d/v4l2loopback.conf

#
# Create v4l2loopback-ctl
#
cd /usr/src/v4l2loopback-${V4L_LOOPBACK_VERSION}
make
cd /usr/local/bin
[ -f v4l2loopback-ctl ] && rm v4l2loopback-ctl
ln -s /usr/src/v4l2loopback-${V4L_LOOPBACK_VERSION}/utils/v4l2loopback-ctl


# Done
echo -e "\Ready!\n"
