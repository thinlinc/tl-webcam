import unittest

from v4l2py.device import read_capabilities, iter_video_files, Capability
from v4l2py.io import fopen

import testutils
from testutils.mock_camera_info import mock_cameras
from video.v4l2utils import CameraInfo


class TestCameraInfo(unittest.TestCase):

    def find_video_devices(self):
        self.video_input = None
        self.video_output = None
        to_find = 2
        for device_file in iter_video_files():
            with fopen(device_file, True) as fd:
                caps = read_capabilities(fd)
                dev_caps = Capability(caps.device_caps)
                if Capability.VIDEO_CAPTURE in dev_caps:
                    if self.video_input is None:
                        self.video_input = str(device_file)
                        to_find -= 1
                elif self.video_output is None:
                    self.video_output = str(device_file)
                    to_find -= 1
            if to_find == 0:
                break

    def requires_input(self):
        if self.video_input is None:
            self.skipTest("No capturable video device found")
    
    def requires_output(self):
        if self.video_output is None:
            self.skipTest("No non-capturable video device found")

    def setUp(self) -> None:
        self.cameras = mock_cameras
        self.find_video_devices()

    def test_from_path_non_input(self):
        self.requires_output()
        cam_info = CameraInfo.from_path(self.video_output)
        self.assertIsNone(cam_info)

    def test_from_path(self):
        self.requires_input()
        cam_info = CameraInfo.from_path(self.video_input)
        self.assertIsNotNone(cam_info)
        self.assertIsCameraInfo(cam_info)

    def assertIsCameraInfo(self, value):
        self.assertIsInstance(value.id, str)
        self.assertEqual(value.path, self.video_input)
        self.assertIsInstance(value.label, str)
        self.assertIsInstance(value.formats, dict)
        pix_fmt, formats = next(iter(value.formats.items()))
        self.assertIsInstance(pix_fmt, str)
        self.assertIsInstance(formats, list)
        format = formats[0]
        self.assertIsInstance(format, dict)
        self.assertIn('width', format)
        self.assertIn('height', format)
        self.assertIn('fps', format)
        self.assertIsInstance(format['width'], int)
        self.assertIsInstance(format['height'], int)
        self.assertIsInstance(format['fps'], tuple)

    def test_instance(self):
        self.assertIsCameraInfo(self.cameras[0][0])

    def test_supports(self):
        for cam_info, supported_sizes, supported_widths, supported_heights in self.cameras:
            for width, height in supported_sizes:
                self.assertTrue(cam_info.supports(width, height))
            for width in supported_widths:
                self.assertTrue(cam_info.supports(width=width))
            for height in supported_heights:
                self.assertTrue(cam_info.supports(height=height))
            width, height = next(iter(supported_sizes))
            self.assertFalse(cam_info.supports(height, width))
            self.assertFalse(cam_info.supports(width, width))
            self.assertFalse(cam_info.supports(width=width+1))
            self.assertFalse(cam_info.supports(height=height+1))

if __name__ == "__main__":
    unittest.main(verbosity=2)
