import asyncio
import v4l2py
import traceback
from asyncio import subprocess as sp
from typing import NamedTuple, List, Optional
import av

import testutils

from video.capture import CameraStreamTrack

TARGET_INDEX=10

def iprint(*args, **kwargs):
    print(*args, **kwargs, flush=True)

class Status:
    first_frame = True

async def wait(tenth_seconds: int):
    iprint("".ljust(tenth_seconds, "."))
    while tenth_seconds > 0:
        await asyncio.sleep(0.1)
        iprint("`", end="")
        tenth_seconds -= 1
    iprint("\n", end="")


def print_frame(frame: av.VideoFrame):
    total_size = sum(plane.buffer_size for plane in frame.planes)
    iprint(f"OUTPUT {frame.width}x{frame.height} ({frame.format.name}) - {total_size}b in {len(frame.planes)} planes")

async def streaming(
        track: CameraStreamTrack,
        ready: asyncio.Event
):
    iprint(f"OUTPUT opening target")
    target = v4l2py.Device.from_id(TARGET_INDEX)
    target.open()
    target.set_format(v4l2py.device.BufferType.VIDEO_OUTPUT, 640, 480, 'YUYV')
    # write dummy frame
    target.write(bytes(614400))
    # set as ready
    ready.set()
    try:
        frame = None
        frame_id = 0
        while True:
            frame = await track.recv()
            if Status.first_frame:
                print_frame(frame)
                Status.first_frame = False
                frame_id = 0
            frame_id += 1
            if target.closed:
                iprint("OUTPUT ERROR: target is closed when writing frame #{frame_id}!")
                break
#            if frame_id < 4: print(f"OUTPUT: skipping {frame_id}: {frame}")
            if frame_id < 4: print(f"OUTPUT: writing {repr(frame)} with {len(frame.planes)} planes")
            if frame_id < 4: print([repr(plane) for plane in frame.planes])
            if len(frame.planes) == 1:
                data = bytes(frame.planes[0])
            else:
                data = bytes(frame.planes[0]) + bytes(frame.planes[1]) + bytes(frame.planes[2])
            written = target._fobj.write(data)
            if written != len(data):
                iprint(f"OUTPUT ERROR: failed to write the frame #{frame_id}: written {written} instead of {len(data)}.\nTarget status is closed: {target.closed}")
                break
    except av.FFmpegError as err:
        iprint(f"OUTPUT FFMPEG ERROR at frame #{frame_id}: {err}")
        print_frame(frame)
    except Exception:
        iprint(f"OUTPUT ERROR at frame #{frame_id}: " + traceback.format_exc())
        print_frame(frame)
    except asyncio.CancelledError:
        pass
    finally:
        iprint(f"OUTPUT closing target")
        target.close()
        iprint(f"OUTPUT stopped")


class Source(NamedTuple):
    path: Optional[str]
    time: int = 0


async def switching(track: CameraStreamTrack, sources: List[Source]):
    for source in sources:
        try:
            iprint(f"SET SOURCE {source.path}")
            await track.set_source(source.path)
            Status.first_frame = True
            iprint(f"SET SOURCE OK")
        except FileNotFoundError as err:
            iprint(f"SET SOURCE ERROR: {err}")
        except av.FFmpegError as err:
            iprint(f"SET SOURCE FFMPEG ERROR: {err}")
        except Exception as err:
            iprint(f"SET SOURCE ERROR: {err}")
            iprint(traceback.format_exc())
        except asyncio.CancelledError:
            break
        if source.time: await wait(source.time)


async def main():
    try:
        ffplay = None

        track = CameraStreamTrack(640, 480, 'rawvideo')
        stream_started = asyncio.Event()

        # Run the streaming task
        streaming_task = asyncio.create_task(streaming(track, stream_started))
        await stream_started.wait()

        # start player
        ffplay = await sp.create_subprocess_exec(
            'ffplay', f'/dev/video{TARGET_INDEX}', '-hide_banner', '-nostats',
            stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE
        )

        # switch sources
        switch_task = asyncio.create_task(switching(track, [
            Source('/dev/video0', 20),
            Source('/dev/video4', 20),
            Source('/dev/video0', 20),
            Source(None, 10),
            Source('/dev/video0', 20),
            Source('/dev/video8', 10),
            Source('/dev/video2', 10),
            Source('/dev/null', 10)
        ]))

        await asyncio.wait(
            (streaming_task, switch_task),
            return_when=asyncio.FIRST_COMPLETED
        )

    finally:
        # Stop ffplay
        if ffplay:
            ffplay.terminate()
            ffout, fferr = await ffplay.communicate('q'.encode())
            print("\n".join((
                "--- FFPLAY stdout -------------------------------------",
                ffout.decode(),
                "--- FFPLAY stdout -------------------------------------",
                fferr.decode(),
                "--- FFPLAY end--- -------------------------------------"
            )))

        # Stop writing to target
        if not streaming_task.done():
            print("CLOSING: stopping the streaming task")
            streaming_task.cancel()
            await streaming_task
        print("CLOSING: streaming task stopped")

        if switch_task and not switch_task.done():
            print("CLOSING: stopping the source switching task")
            switch_task.cancel()
            await switch_task
        print("CLOSING: source switching task stopped")

        # Close the track
        print("CLOSING: closing the stream track object")
        track.stop()


if __name__ == "__main__":
    
    from components import create_logger

    create_logger(level=0).add_stream_target(level = 0)
    asyncio.run(main())
