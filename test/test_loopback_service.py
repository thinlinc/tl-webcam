import unittest
import asyncio
import logging
from pyee import EventEmitter
from typing import List
from os import listdir

from testutils import EmitterTestCase

from service.loopback import LoopbackService, LoopbackDevice
from constants import V4L2LOOPBACK_CTL, SYSDEVICE_PATH


class TestLoopbackService(unittest.IsolatedAsyncioTestCase):

    def assertCurrentDeviceSet(self):
        try:
            current_set = set(listdir(SYSDEVICE_PATH))
            service_set = set(f'device{device.index}' for device in self.service.devices.values())
            self.assertSetEqual(current_set, service_set)
        except FileNotFoundError:
            self.assertEqual(len(self.service.devices), 0)

    def assertDeviceExists(self, index: int):
        pass
    
    def assertDeviceAssigned(self, index: int, username: str):
        pass

    async def asyncSetUp(self):
        self.service = LoopbackService()
        await self.service.start()
        await asyncio.sleep(0.1)
        self.assertCurrentDeviceSet()

    async def asyncTearDown(self):
        await self.service.stop()
        self.assertEqual(len(self.service.devices), 0)
        self.assertCurrentDeviceSet()

    async def test_create(self):
        device = await self.service.get_device('matha')
        self.assertIsInstance(device, LoopbackDevice)
        self.assertEqual(self.service.devices.get('matha'), device)
        self.assertDeviceExists(device.index)
        self.assertDeviceAssigned(device.index, 'matha')
        
    async def test_create_is_idempotent(self):
        device1 = await self.service.get_device('matha')
        device2 = await self.service.get_device('matha')
        self.assertEqual(device1, device2)

    async def test_delete(self):
        device = await self.service.get_device('matha')
        removed = await self.service.remove_device('matha')
        self.assertEqual(device, removed)
        self.assertNotIn('matha', self.service.devices)

    async def test_delete_nonexistent(self):
        self.assertIsNone(await self.service.remove_device('whatever'))

    async def test_repeated_delete(self):
        await self.service.get_device('matha')
        await self.service.remove_device('matha')
        self.assertIsNone(await self.service.remove_device('matha'))


class TestLoopbackMonitorService(EmitterTestCase):

    async def asyncSetUp(self):
        self.service = LoopbackService()
        self.files = []
        await self.service.start()
        await asyncio.sleep(0.1)

    async def asyncTearDown(self):
        self.close_all_files()
        await self.service.stop()
        self.assertEqual(len(self.service.devices), 0)
    
    def open_file(self, path):
        file = open(path, mode='rb')
        self.files.append(file)
        return file
    
    def close_all_files(self):
        for file in self.files: file.close()
        self.files.clear()

    async def test_no_streamon_event_on_first_open(self):
        device = await self.service.get_device('matha')
        await asyncio.sleep(0.1)
        with self.assertEmits(
            self.service,
            LoopbackService.EV_STREAM_ON,
            count=0
        ):
            self.open_file(device.path)
            await asyncio.sleep(0.2)

    async def test_streamon_event_on_second_open(self):
        device = await self.service.get_device('matha')
        await asyncio.sleep(0.1)
        self.open_file(device.path)
        await asyncio.sleep(0.01)   # events are lots without this pause!
        with self.assertEmits(    
            self.service,
            LoopbackService.EV_STREAM_ON,
            count=1,
            args=(device,)
        ):
            self.open_file(device.path)
            await asyncio.sleep(0.2)

    async def test_no_streamon_event_on_brief_open(self):
        device = await self.service.get_device('matha')
        await asyncio.sleep(0.1)
        self.open_file(device.path)
        await asyncio.sleep(0.2)
        with self.assertEmits(
            self.service,
            LoopbackService.EV_STREAM_ON,
            count=0
        ):
            self.open_file(device.path)
            await asyncio.sleep(0.01)

    async def test_streamoff_event(self):
        device = await self.service.get_device('matha')
        await asyncio.sleep(0.1)
        self.open_file(device.path)
        await asyncio.sleep(0.01)
        file = self.open_file(device.path)
        await asyncio.sleep(0.2)
        with self.assertEmits(
            self.service,
            LoopbackService.EV_STREAM_OFF,
            count=1,
            args=(device,)
        ):
            file.close()
            await asyncio.sleep(0.01)


if __name__ == "__main__":
#    import logging
#    logging.basicConfig(level=logging.DEBUG, format='  %(asctime)s %(levelname)-7s %(name)-10s %(message)s')
#    logging.getLogger('asyncio').setLevel(logging.ERROR)
    unittest.main(verbosity=2)
