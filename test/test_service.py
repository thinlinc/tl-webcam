import unittest
import asyncio
import logging

from testutils import EmitterTestCase
from service.base import Service, ActiveService, PeriodicService

class MockService(Service):

    def __init__(self, *, load_time=0.05, unload_time=0.05, load_error=None, unload_error=None):
        super().__init__()
        self.load_time = load_time
        self.unload_time = unload_time
        self.load_error = load_error
        self.unload_error = unload_error
        self.load_counter = 0
        self.unload_counter = 0

    async def load(self):
        self.load_counter += 1
        await asyncio.sleep(self.load_time)
        if self.load_error: raise self.load_error
    
    async def unload(self):
        self.unload_counter += 1
        await asyncio.sleep(self.unload_time)
        if self.unload_error: raise self.unload_error


class MockActiveService(ActiveService):

    def __init__(self, *, run_time=1, run_error=None):
        super().__init__()
        self.run_time = run_time
        self.run_error = run_error
        self.run_counter = 0
        self.load_counter = 0
        self.unload_counter = 0

    async def load(self):
        self.load_counter += 1
        await super().load()

    async def unload(self):
        self.unload_counter += 1
        await super().unload()

    async def run(self):
        self.run_counter += 1
        await asyncio.sleep(self.run_time)
        if self.run_error: raise self.run_error


class MockPeriodicService(PeriodicService):
    def __init__(self, *, interval=1, run_time=0, run_error=None):
        super().__init__(interval)
        self.run_time = run_time
        self.run_error = run_error
        self.run_counter = 0
        self.load_counter = 0
        self.unload_counter = 0

    async def load(self):
        self.load_counter += 1
        await super().load()

    async def unload(self):
        self.unload_counter += 1
        await super().unload()

    async def run(self):
        self.run_counter += 1
        await asyncio.sleep(self.run_time)
        if self.run_error: raise self.run_error


class TestService(EmitterTestCase):

    def test_next_service_id(self):
        base_id = Service.next_service_id()
        self.assertEqual(base_id + 1, Service.next_service_id())
        self.assertEqual(base_id + 2, Service.next_service_id())
        self.assertEqual(base_id + 3, MockActiveService.next_service_id())

    def test_service_id(self):
        base_id = Service.next_service_id()+1
        services = (Service(), MockActiveService(), MockService())
        for id, service in enumerate(services):
            self.assertIsInstance(service.id, int)
            self.assertEqual(id+base_id, service.id)

    async def test_initial_state(self):
        service = MockService()
        self.assertEqual(service.state, Service.STOPPED)
        self.assertFalse(service.is_running)
        self.assertEqual(service.load_counter, 0)
        self.assertEqual(service.unload_counter, 0)

    async def test_start(self):
        service = MockService()
        start_task = asyncio.create_task(service.start())
        await asyncio.sleep(0)
        self.assertEqual(service.state, Service.LOADING)
        self.assertFalse(service.is_running)
        with self.assertLogs(service.logger, logging.INFO):
            await start_task
        self.assertEqual(service.state, Service.RUNNING)
        self.assertTrue(service.is_running)
        self.assertEqual(service.load_counter, 1)

    async def test_start_emits(self):
        service = MockService()
        with self.assertEmits(service, Service.EV_START, count=1):
            await service.start()
        
    async def test_load_error(self):
        """
        Service.start() does not leak exceptions
        """
        service = MockService(load_error=Exception())
        with self.assertLogs(service.logger, logging.ERROR):
            await service.start()
        self.assertEqual(service.state, Service.STOPPED)

    async def test_start_is_idempotent(self):
        service = MockService()
        await asyncio.gather(service.start(), service.start())
        self.assertEqual(service.state, Service.RUNNING)
        self.assertEqual(service.load_counter, 1)

    async def test_start_when_running(self):
        service = MockService()
        await service.start()
        await service.start()
        self.assertEqual(service.load_counter, 1)

    async def test_start_when_unloading(self):
        service = MockService()
        await service.start()
        end_task = asyncio.create_task(service.stop())
        await asyncio.sleep(0)
        self.assertEqual(service.state, Service.UNLOADING)
        await service.start()
        self.assertTrue(end_task.done())
        self.assertEqual(service.state, Service.RUNNING)

    async def test_stop(self):
        service = MockService()
        await service.start()
        end_task = asyncio.create_task(service.stop())
        await asyncio.sleep(0)
        self.assertEqual(service.state, Service.UNLOADING)
        self.assertFalse(service.is_running)
        with self.assertLogs(service.logger, logging.INFO):
            await end_task
        self.assertEqual(service.state, Service.STOPPED)
        self.assertFalse(service.is_running)
        self.assertEqual(service.unload_counter, 1)

    async def test_stop_emits(self):
        service = MockService()
        await service.start()
        with self.assertEmits(service, Service.EV_STOP, count=1):
            await service.stop()

    async def test_unload_error(self):
        """
        Service.stop() does not leak exceptions
        """
        service = MockService(unload_error=Exception())
        await service.start()
        with self.assertLogs(service.logger, logging.ERROR):
            await service.stop()
        self.assertEqual(service.state, Service.RUNNING)

    async def test_stop_is_idempotent(self):
        service = MockService()
        await service.start()
        await asyncio.gather(service.stop(), service.stop())
        self.assertEqual(service.state, Service.STOPPED)
        self.assertEqual(service.unload_counter, 1)

    async def test_stop_when_stopped(self):
        service = MockService()
        await service.stop()
        self.assertEqual(service.unload_counter, 0)

    async def test_stop_when_loading(self):
        service = MockService()
        start_task = asyncio.create_task(service.start())
        await asyncio.sleep(0)
        self.assertEqual(service.state, Service.LOADING)
        await service.stop()
        self.assertTrue(start_task.done())
        self.assertEqual(service.state, Service.STOPPED)



class TestActiveService(EmitterTestCase):

    async def test_service(self):
        service = MockActiveService()
        self.assertFalse(service.is_running)
        await service.start()
        self.assertTrue(service.is_running)
        await asyncio.sleep(0)  # let the runner kick in
        self.assertEqual(service.run_counter, 1)
        await service.stop()
        self.assertFalse(service.is_running)

    async def test_run_stops(self):
        service = MockActiveService(run_time=0.05)
        await service.start()
        self.assertTrue(service.is_running)
        with self.assertEmits(service, Service.EV_STOP, count=1):
            await asyncio.sleep(0.1)
        self.assertFalse(service.is_running)
        self.assertEqual(service.unload_counter, 1)
        self.assertEqual(service.state, Service.STOPPED)

    async def test_run_error(self):
        service = MockActiveService(run_time=0.05, run_error=Exception())
        await service.start()
        self.assertTrue(service.is_running)
        with self.assertLogs(service.logger, logging.ERROR):
            with self.assertEmits(service, Service.EV_STOP, count=1):
                await asyncio.sleep(0.1)
        self.assertFalse(service.is_running)
        self.assertEqual(service.unload_counter, 1)
        self.assertEqual(service.state, Service.STOPPED)




class TestPeriodicService(EmitterTestCase):

    async def test_service(self):
        try:
            service = MockPeriodicService(interval=0.05)
            self.assertFalse(service.is_running)
            await service.start()
            self.assertTrue(service.is_running)
            await asyncio.sleep(0.01)  # let the runner kick in
            self.assertEqual(service.run_counter, 1)
            await asyncio.sleep(0.26) # wait for another 5 tasks
            self.assertEqual(service.run_counter, 6)
            await service.stop()
            self.assertFalse(service.is_running)
        finally:
            await service.stop()

    async def test_run_error(self):
        try:
            service = MockPeriodicService(interval=0.05, run_error=Exception())
            await service.start()
            self.assertTrue(service.is_running)
            with self.assertLogs(service.logger, logging.ERROR):
                with self.assertEmits(service, Service.EV_STOP, count=0):
                    await asyncio.sleep(0.11)
            self.assertTrue(service.is_running)
            self.assertEqual(service.state, Service.RUNNING)
            self.assertEqual(service.run_counter, 3)
        finally:
            await service.stop()

if __name__ == "__main__":
    unittest.main(verbosity=2)
