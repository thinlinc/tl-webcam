import testutils

from service.video_monitor import VideoDevicesMonitor


def print_cameras(cams):
    print(f"Known cameras: {len(cams)}")
    for cam in cams:
        print(f"  {cam.path} - {cam.label}")


async def main():


    monitor = VideoDevicesMonitor()

    @monitor.on(VideoDevicesMonitor.EV_CAMERA_ATTACHED)
    def onadded(camera):
        print("Attached {}:\n{}".format(camera.path, camera))
        print_cameras(monitor.cameras)

    @monitor.on(VideoDevicesMonitor.EV_CAMERA_DETACHED)
    def onremoved(camera):
        print("Removed {}:\n{}".format(camera.path, camera))
        print_cameras(monitor.cameras)

    try:
        await monitor.start()
        print_cameras(monitor.cameras)
        await asyncio.sleep(3600)
    except asyncio.CancelledError:
        pass
    finally:
        await monitor.stop()
    

if __name__ == "__main__":
    import asyncio
    asyncio.run(main())

