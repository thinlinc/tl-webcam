import asyncio
import unittest
from typing import Optional

import testutils
from components.session import Session, SimpleClientSession, SimpleServerSession, RequestInfo
from components.errors import RequestFailedError

class TestSimpleSession(unittest.IsolatedAsyncioTestCase):
    
    def create_pair(self, handlers={}):
        chA, chB = testutils.create_channel('client', 'server')
        return SimpleClientSession(chA), SimpleServerSession(chB, handlers=handlers)

    async def test_notify(self):
        client, server = self.create_pair()

        cmd = b'test'
        data = b'blablabla'
        await client.notify(cmd, data)

        got = server.process(await server.channel.recv())
        self.assertEqual(cmd, got.cmd)
        self.assertEqual(data, got.data)


    async def test_request_respond(self):
        client, server = self.create_pair()

        cmd = b'test'
        data = b'blablabla'
        response = b'tested'

        future_response = asyncio.create_task(client.request(cmd, data))
        request = server.process(await server.channel.recv())
        self.assertEqual(cmd, request.cmd)
        self.assertEqual(data, request.data)

        await server.respond(request, response)
        self.assertEqual(response, await future_response)


    async def test_request_reject(self):
        client, server = self.create_pair()

        cmd = b'test'
        data = b'blablabla'

        err_code = 15
        err_msg = 'rejected'
        response = RequestFailedError(err_code, err_msg)

        future_response = asyncio.create_task(client.request(cmd, data))
        request = server.process(await server.channel.recv())
        self.assertEqual(cmd, request.cmd)
        self.assertEqual(data, request.data)

        await server.reject(request, response)
        with self.assertRaises(RequestFailedError) as ctx:
            await future_response
        got = ctx.exception
        self.assertEqual(got.code, err_code)
        self.assertEqual(str(got), err_msg)


    async def test_listen(self):

        async def stop(session: Session, data: bytes):
            await msg_log.put((b'stop', data))
            await session.close()

        async def give(session: Session, data: bytes):
            await msg_log.put((b'give', data))
            return data

        async def note(session: Session, data: bytes):
            await msg_log.put((b'note', data))

        client, server = self.create_pair(handlers={
            b'note': note,
            b'give': give,
            b'stop': stop
        })

        msg_log = asyncio.Queue()

        await client.notify(b'note', b'one')

        got = await client.request(b'give', b'two')
        self.assertEqual(got, b'two')

        await client.notify(b'stop', b'three')

        self.assertEqual(await msg_log.get(), (b'note', b'one'))
        self.assertEqual(await msg_log.get(), (b'give', b'two'))
        self.assertEqual(await msg_log.get(), (b'stop', b'three'))
        self.assertTrue(msg_log.empty())
        self.assertTrue(server.is_closed)
        # This should do nothing
        await server.close()



if __name__ == "__main__":
    unittest.main()
