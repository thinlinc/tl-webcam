import asyncio
import traceback
from asyncio import subprocess as sp
from typing import NamedTuple, List, Optional
import av

import testutils

from video.capture import CameraStreamTrack
from service.video_feeder import CameraFeeder, VideoOutputDevice
from aiortc import MediaStreamTrack

TARGET_INDEX=10

def iprint(*args, **kwargs):
    print(*args, **kwargs, flush=True)

def print_frame(frame: av.VideoFrame):
    total_size = sum(plane.buffer_size for plane in frame.planes)
    iprint(f"OUTPUT {frame.width}x{frame.height} ({frame.format.name}) - {total_size}b in {len(frame.planes)} planes")

async def wait(tenth_seconds: int):
    iprint("".ljust(tenth_seconds, "."))
    while tenth_seconds > 0:
        await asyncio.sleep(0.1)
        iprint("`", end="")
        tenth_seconds -= 1
    iprint("\n", end="")


class Source(NamedTuple):
    path: Optional[str]
    time: int = 0


class TrackDecoder(MediaStreamTrack):

    kind = "video"

    def __init__(self, track: MediaStreamTrack):
        super().__init__()
        self.source = track

    async def recv(self) -> av.VideoFrame:
        frame = await self.source.recv()
        return frame.reformat(format='yuv420p')
        

async def capturing(
        feeder: CameraFeeder,
        track: CameraStreamTrack,
        sources: List[Source]
):
    decoded = TrackDecoder(track)
    for source in sources:
        try:
            iprint(f"CAPTURE SOURCE {source.path}")
            feeder.set_track(source.path and decoded)
            await track.set_source(source.path)
            iprint(f"CAPTURE SOURCE OK")
        except FileNotFoundError as err:
            iprint(f"CAPTURE SOURCE ERROR: {err}")
        except av.FFmpegError as err:
            iprint(f"CAPTURE SOURCE FFMPEG ERROR: {err}")
        except Exception as err:
            iprint(f"CAPTURE SOURCE ERROR: {err}")
            iprint(traceback.format_exc())
        except asyncio.CancelledError:
            break
        if source.time: await wait(source.time)


async def playing(source: str, stop: asyncio.Event):
    
    async def pipe_reader(prompt: str, pipe: asyncio.StreamReader):
        try:
            print(prompt + "starts")
            while True:
                line = await pipe.readline()
                print(prompt + line.decode().strip())
        except Exception as err:
            print(prompt + repr(err))
        except asyncio.CancelledError:
            pass
        finally:
            print(prompt + "stops")
    
    ffplay = await sp.create_subprocess_exec(
        'ffplay', source, '-hide_banner', '-nostats', '-loglevel', 'error',
        stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE
    )
    stdout_reader = asyncio.create_task(pipe_reader('FFPLAY.OUT: ', ffplay.stdout))
    stderr_reader = asyncio.create_task(pipe_reader('FFPLAY.ERR: ', ffplay.stderr))
    await stop.wait()

    if not stdout_reader.done(): stdout_reader.cancel()
    if not stderr_reader.done(): stderr_reader.cancel()
    await asyncio.gather(stdout_reader, stderr_reader)
    ffplay.terminate()
    ffout, fferr = await ffplay.communicate('q'.encode())
    if ffout: print('FFPLAY.OUT: ' + ffout.decode().strip())
    if fferr: print('FFPLAY.ERR: ' + ffout.decode().strip())




async def main():
    try:
        ffplay_task = None
        target_name = f'/dev/video{TARGET_INDEX}'

        track = CameraStreamTrack(640, 480)
#        track = CameraStreamTrack(640, 480, 'yuyv422')

        # Run the feeder service
        feeder = CameraFeeder(VideoOutputDevice(target_name), width=640, height=480)
        await feeder.start()
        # wait a bit so that the first frame can be sent
        await asyncio.sleep(0.5)

        # start player
        ffplay_stop = asyncio.Event()
        ffplay_task = asyncio.create_task(playing(target_name, ffplay_stop))
        # allow ffplay to start
        await asyncio.sleep(0.5)

        # switch sources
        await asyncio.create_task(capturing(feeder, track, [
            Source('/dev/video0', 20),
            Source('/dev/video4', 20),
            Source('/dev/video0', 20),
            Source(None, 10),
            Source('/dev/video0', 20),
            Source('/dev/video8', 10),
            Source('/dev/video2', 10),
            Source('/dev/null', 10)
        ]))

    finally:
        # Stop ffplay
        if ffplay_task:
            ffplay_stop.set()
            await ffplay_task

        # Close the track
        print("CLOSING: closing the stream track object")
        track.stop()

        # Stop the feder service
        await feeder.stop()



if __name__ == "__main__":
    
    from components import create_logger

    create_logger(level=0).add_stream_target(level=0)

    asyncio.run(main())
