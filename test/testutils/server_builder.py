import sys
from os.path import dirname
PROJECT_DIR = dirname(dirname(dirname(__file__))) + "/src"
sys.path.insert(0, PROJECT_DIR)

from typing import Optional

from service.server import AbstractServer, TcpServer, UnixServer
from components.channel import Channel
from components.session import Session, SimpleClientSession, RequestFailedError


async def handle_echo(session: Session, data: bytes) -> bytes:
    return data

async def handle_vget(session: Session, data: bytes) -> bytes:
    value = session[data]
    if value is None: raise RequestFailedError(80, 'not found')
    return value
    
async def handle_vset(session: Session, data: bytes) -> None:
    key, *value = data.split(b'\n', 1)
    session[key] = value[0] if len(value) else b''


def build_tcp_server(port: Optional[int] = None) -> TcpServer:
    server = TcpServer('localhost', port)
    server.set_handlers({
        b'echo': handle_echo,
        b'vget': handle_vget,
        b'vset': handle_vset
    })
    return server

def build_unix_server(path: str) -> UnixServer:
    server = UnixServer(path)
    server.set_handlers({
        b'echo': handle_echo,
        b'vget': handle_vget,
        b'vset': handle_vset
    })
    return server
