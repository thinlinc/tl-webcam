import cv2
import numpy
import math
from typing import List, Iterator

from v4l2py import device as v4l2
from aiortc import VideoStreamTrack
from aiortc.mediastreams import MediaStreamError
from av import VideoFrame
from av.frame import Frame
   

def create_rectangle(width, height, color):
    data = numpy.zeros((height, width, 3), numpy.uint8)
    data[:, :] = color
    return data
    
def create_vertical_flag(
    width = 320,
    height = 240,
    left = (255,0,0),
    middle = (255,255,255),
    right = (0,0,255)
) -> numpy.ndarray:
    stripe_width = int(width/3)
    return numpy.hstack([
        create_rectangle(stripe_width, height, left),
        create_rectangle(width - stripe_width*2, height, middle),
        create_rectangle(stripe_width, height, right)
    ])

def create_horizontal_flag(
    width = 320,
    height = 240,
    top = (255,0,0),
    middle = (255,255,255),
    bottom = (0,0,255)
) -> numpy.ndarray:
    stripe_height = int(height/3)
    return numpy.vstack([
        create_rectangle(width, stripe_height, top),
        create_rectangle(width, height - stripe_height*2, middle),
        create_rectangle(width, stripe_height, bottom)
    ])

def create_swiss_flag(size = 320) -> numpy.ndarray:
    data = numpy.zeros((size, size, 3), numpy.uint8)
    data[:, :] = (255,0,0)
    for x in range(120, 201):
        for y in range(40, 281):
            data[x,y] = (255,255,255)
            data[y,x] = (255,255,255)
    return data


def animated_flag(
    flag: numpy.ndarray,
    width: int = 0,
    height: int = 0       
) -> List[VideoFrame]:
    flag_width = len(flag[0])
    flag_height = len(flag)
    if width == 0: width = flag_width
    if height == 0: height = int(width * flag_height / flag_width)

    # center the flag
    M = numpy.float32([
        [1, 0, (width - flag_width) / 2],
        [0, 1, (height - flag_height) / 2]
    ])
    flag = cv2.warpAffine(flag, M, (width, height))

    # compute animation
    omega = 2 * math.pi / height
    id_x = numpy.tile(numpy.array(range(width), dtype=numpy.float32), (height, 1))
    id_y = numpy.tile(
        numpy.array(range(height), dtype=numpy.float32), (width, 1)
    ).transpose()

    frames: List[numpy.ndarray] = []
    for k in range(30):
        phase = 2 * k * math.pi / 30
        map_x = id_x + 10 * numpy.cos(omega * id_x + phase)
        map_y = id_y + 10 * numpy.sin(omega * id_x + phase)
        frames.append(
            VideoFrame.from_ndarray(
                cv2.remap(flag, map_x, map_y, cv2.INTER_LINEAR),
                format = 'rgb24'
            )
        )

    return frames




class GeneratedVideoStreamTrack(VideoStreamTrack):
    def __init__(self, frames: List[VideoFrame]) -> None:
        super().__init__()
        self.__frames = frames
        self.__count = len(frames)
        self.__current = 0

    async def recv(self) -> Frame:
        pts, time_base = await self.next_timestamp()

        frame  = self.__frames[self.__current]
        frame.pts = pts
        frame.time_base = time_base
        self.__current += 1
        if self.__current == self.__count:
            self.__current = 0
        return frame
