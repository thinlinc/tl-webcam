import asyncio
import subprocess
from os.path import dirname, join
from v4l2py.device import Device, BufferType

V4L2LOOPBACK_CTL = join(dirname(dirname(dirname(__file__))), 'src', 'v4l2loopback-ctl')

class VirtualCamera(Device):
    def __init__(self, name: str = 'Test camera'):
        self.frame = None
        self.feeder = None
        result = subprocess.run(
            [V4L2LOOPBACK_CTL, 'add', '-x', '1', '-n', name],
            text=True,
            capture_output=True
        )
        result.check_returncode()
        print(f'Created {result.stdout}')
        super().__init__(result.stdout.strip())

    def __del__(self):
        if self.feeder: self.feeder.cancel()
        self.close()
        subprocess.run(
            [V4L2LOOPBACK_CTL, 'delete', str(self.filename)]
        )
        print(f'Removed {self.filename}')

    def on(self, width: int, height: int, pixel_color: int = 0):
        self.open()
        self.set_format(BufferType.VIDEO_OUTPUT, width, height, 'YUYV')
        self.frame = bytes([pixel_color] * (width*height*2))
        self.write(self.frame)
        self.feeder = asyncio.create_task(self.feed())

    def off(self):
        if self.feeder: self.feeder.cancel()

    async def feed(self):
        while True:
            try:
                self.write(self.frame)
                await asyncio.sleep(0.1)
            except BaseException as err:
                print(f'Closing the camera: {err!r}')
                break
        self.close()


if __name__ == "__main__":

    import av
    import threading

    def test_frames(path, width, height, pixel_color, done: asyncio.Event):
        try:
            with av.open(path, mode='r', format='v4l2') as cam:
                frames = cam.decode(cam.streams.video[0])
                for i in range(1,11):
                    print(f'Testing frame #1{i}')
                    frame = next(frames)
                    if frame.width != width or \
                        frame.height != height or \
                        frame.format.name != 'yuyv422':
                        print('Bad format: {frame.width}x{frame.height} ({frame.format.name})')
                        continue
                    for x in bytes(frame.planes[0]):
                        if x != pixel_color:
                            print(f'Bad pixel color: {x}')
                            break
        except Exception as err:
            print(f'Capture error: {err!r}')
        done.set()


    async def test():

        pixel_color = 87
        width = 320
        height = 240

        virt_cam = VirtualCamera()
        print(f'Created camera {virt_cam.filename}')
        virt_cam.on(width, height, pixel_color)
        
        print(f'Capturing the camera')
        done = asyncio.Event()
        thread = threading.Thread(
            target=test_frames,
            args=(str(virt_cam.filename), width, height, pixel_color, done)
        )
        thread.start()
        await done.wait()
        print(f'Closing the camera')
        virt_cam.off()
        thread.join()

    asyncio.run(test())