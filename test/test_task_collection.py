import unittest
import asyncio
from typing import List

from testutils import *
from components.task_collection import TaskCollection


class TestTaskCollection(unittest.IsolatedAsyncioTestCase):
    
    async def test_create_task(self):
        """TaskCollection.create_task() returns an instance of asyncio.Task"""
        async with TaskSpawner() as spawner:
            tasks = TaskCollection()
            self.assertEqual(len(tasks), 0)
            returned = tasks.create_task(spawner.start_coro(duration=1000))
            self.assertIsInstance(returned, asyncio.Task)
            self.assertEqual(len(tasks), 1, "the size of TaskCollection")
            await tasks.clear()

    async def test_add_task(self):
        """TaskCollection.add_task() returns the added task"""
        async with TaskSpawner() as spawner:
            tasks = TaskCollection()
            self.assertEqual(len(tasks), 0)
            task = spawner.start_task(duration=1000)
            returned = tasks.add_task(task)
            self.assertEqual(returned, task)
            self.assertEqual(len(tasks), 1, "the size of TaskCollection")
            await tasks.clear()

    async def test_remove(self):
        """TaskCollection.remove_task() removes a task without cancelling it"""
        async with TaskSpawner() as spawner:
            tasks = TaskCollection()
            task = spawner.start_task(duration=1000)
            tasks.add_task(task)
            self.assertEqual(1, len(tasks))
            removed = tasks.remove_task(task)
            self.assertEqual(0, len(tasks))
            self.assertTrue(removed)
            await asyncio.sleep(0)
            self.assertFalse(task.done())

    async def test_remove_nonexistent(self):
        """TaskCollection.remove_task() returns False if the task is not in the manager"""
        async with TaskSpawner() as spawner:
            tasks = TaskCollection()
            task = spawner.start_task(duration=1000)
            removed = tasks.remove_task(task)
            self.assertFalse(removed)
            await asyncio.sleep(0)
            self.assertFalse(task.done())

    async def test_clear(self):
        """TaskCollection.clear() cancels all running tasks"""
        async with TaskSpawner() as spawner:
            tasks = TaskCollection()
            tasks.create_task(spawner.start_coro(duration=1000))
            tasks.create_task(spawner.start_coro(duration=1000))
            await tasks.clear()
            self.assertEqual(spawner.finished, 2)
            self.assertEqual(spawner.cancelled, 2)
            self.assertEqual(len(tasks), 0)

    async def test_task_done(self):
        """Tasks are removed from the TaskCollection when done"""
        async with TaskSpawner() as spawner:
            task_mgr = TaskCollection()
            tasks: List[asyncio.Task] = [
                task_mgr.create_task(spawner.start_coro(duration=10, value=42)),
                task_mgr.create_task(spawner.start_coro(duration=10, value=Exception())),
                task_mgr.create_task(spawner.start_coro(duration=10))
            ]
            self.assertEqual(len(task_mgr), 3)
            tasks[2].cancel()
            await asyncio.gather(*tasks, return_exceptions=True)
            self.assertEqual(len(task_mgr), 0)

    async def test_as_context_manager(self):
        """TaskCollection can be used as a context manager"""
        async with TaskSpawner() as spawner:
            tasks = TaskCollection()
            async with tasks:
                for _ in range(0,3):
                    tasks.add_task(spawner.start_task(duration=1))
                self.assertEqual(3, len(tasks))
            self.assertEqual(0, len(tasks))
            self.assertEqual(3, spawner.cancelled)
            
            

if __name__ == "__main__":
    unittest.main()
