import sys
import site

# Get the default site-packages directory
default_site_packages = site.getsitepackages()[0] if hasattr(site, 'getsitepackages') else site.getusersitepackages()

print(default_site_packages)

