#!/bin/bash

TSBUILD_DIR=/ts/build
TLCLIENT_PACKAGE=thinlinc-my
TSWEBCAM_PACKAGE=tswebcam
PYTHON3_PACKAGE=python3
PA_PACKAGE=pulseaudio

# Directory structure of the repository:
# src                      source files of the package
# thinstation              installation files for a ThinStation
# thinstation/install.sh   this script
# thinstation/pulse        patch files for PulseAudio
# thinstation/python3      patch files for Python modules
# thinstation/tswebcam     configuration files for this package
SCRIPT_DIR=`dirname $(realpath $0)`
SOURCE_DIR=`dirname $SCRIPT_DIR`/src

TSWEBCAM_DIR="$TSBUILD_DIR/packages/$TSWEBCAM_PACKAGE"
TLCLIENT_DIR="$TSBUILD_DIR/packages/$TLCLIENT_PACKAGE/lib/tlclient"
PYTHON3_LIB="$TSBUILD_DIR/packages/$PYTHON3_PACKAGE/lib/python3"
PA_ETC="$TSBUILD_DIR/packages/$PA_PACKAGE/etc/pulse"

# Check that dependencies are installed
error=0
for dep in $TLCLIENT_DIR $PYTHON3_LIB $PA_ETC; do
  [ -d $dep ] || {
    echo "Missing dependency: $dep"
    error=1
  }
done
[ $error == 0 ] || {
  echo "Error: missing dependencies"
  exit 1
}

# Check if pip is available - it is in the the build environment, but not otherwise
which pip > /dev/null || { echo "Missing pip - are you in the build environment?"; exit 1; }

# Install and patch python libraries
echo "Installing python modules..."
INSTALLED_PACKAGES=`pip list --path $PYTHON3_LIB`
for MODULE in `grep -v ";" $SCRIPT_DIR/dependencies`; do
  MODULE_NAME=`echo $MODULE | cut -f 1 -d '='`
  echo -n "- $MODULE_NAME..."
  if echo $INSTALLED_PACKAGES | grep $MODULE_NAME >/dev/null 2>&1; then
    echo "already installed"
  else
    echo "installing"
    pip install -q --target $PYTHON3_LIB $MODULE
  fi
done
echo "Applying patches to python modules..."
\cp -r $SCRIPT_DIR/python3/* $PYTHON3_LIB/

# Copy python files
echo "Copying tswebcam python files"
TARGET_DIR=$TSWEBCAM_DIR/lib/tswebcam
[ -d $TARGET_DIR ] && \rm -rf $TARGET_DIR/* || mkdir -p $TARGET_DIR
for FILE in `cat $SCRIPT_DIR/files`; do
    DIR=`dirname $TARGET_DIR/$FILE`
    mkdir -p $DIR
    cp $SOURCE_DIR/$FILE $DIR
done
mkdir -p $TARGET_DIR/constants
cp $SOURCE_DIR/constants/thinstation.py $TARGET_DIR/constants/__init__.py

# Copy package files
echo "Copying tswebcam package files"
for FILE in bin build etc .dna dependencies; do
    \cp -r $SCRIPT_DIR/tswebcam/$FILE $TSWEBCAM_DIR/
done

echo "Creating symbolic links"
ln -snf  /lib/tswebcam/tswebcam-ctl.py $TSWEBCAM_DIR/bin/tswebcam-ctl
ln -snf ../tswebcam.service $TSWEBCAM_DIR/etc/systemd/system/multi-user.target.wants/tswebcam.service

# Copy the list of PA devices to be avoided
echo "Copying list of PulseAudio devices to avoid"
\cp $SCRIPT_DIR/pulse/avoid.list $PA_ETC/

# Hijack /lib/tlclient/ssh
echo "Updating tlclient ssh binary"
[ -f $TLCLIENT_DIR/ssh-tl ] || mv $TLCLIENT_DIR/ssh $TLCLIENT_DIR/ssh-tl
\cp $SCRIPT_DIR/tlclient/ssh $TLCLIENT_DIR/ssh

# That's it!
echo "Ready!"
