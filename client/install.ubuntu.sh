bin/bash

TL_DIR=/opt/thinlinc

#TSBUILD_DIR=/ts/build
#TLCLIENT_PACKAGE=thinlinc-my
#TSWEBCAM_PACKAGE=tswebcam
#PYTHON3_PACKAGE=python3
#PA_PACKAGE=pulseaudio

#SCRIPT_DIR=`dirname $(realpath $0)`
#SOURCE_DIR=`dirname $SCRIPT_DIR`/src

#TSWEBCAM_DIR="$TSBUILD_DIR/packages/$TSWEBCAM_PACKAGE"
TLCLIENT_DIR=${TL_DIR}/lib/tlclient
#PYTHON3_LIB="$TSBUILD_DIR/packages/$PYTHON3_PACKAGE/lib/python3"
#PA_ETC="$TSBUILD_DIR/packages/$PA_PACKAGE/etc/pulse"

# Temp download dir
DIR=`mktemp -d '/tmp/tl-webcam.XXXX'`
TL_WEBCAM_MASTER=tl-webcam-master
TL_WEBCAM_REPO=https://git.math.uzh.ch/thinlinc/tl-webcam/-/archive/master/
REPO_DIR=$DIR/${TL_WEBCAM_MASTER}

#
# Check that dependencies are installed
#
error=0
for II in $TLCLIENT_DIR; do
  [ -d $II ] || {
    echo "Missing dependency: $dep"
    error=1
  }
done
[ $error == 1 ] && exit 1

# Check if pip is available - it is in the the build environment, but not otherwise
which pip > /dev/null || { echo "Missing pip - are you in the build environment?"; exit 1; }

#
# Clone Repo
#
cd $DIR
wget ${TL_WEBCAM_REPO}/${TL_WEBCAM_MASTER}.zip
unzip ${TL_WEBCAM_MASTER}.zip

# If old version exist: move it away
[ -d /usr/src/${TL_WEBCAM_MASTER} ] && mv /usr/src/${TL_WEBCAM_MASTER} /usr/src/${TL_WEBCAM_MASTER}.`date "+%Y%m%d.%H%M%S"`
mv ${TL_WEBCAM_MASTER} /usr/src/${TL_WEBCAM_MASTER}


#
# Install and patch python libraries
#
echo "Installing python modules..."

PKGS=`cat $REPO_DIR/client/dependencies`
for II in $PKGS; done
  pip install -q $II
done  

echo "Applying patches to python modules..."
PYTHON3_LIB=`python3 $REPO_DIR/client/getsitepackage.py`
\cp -r $REPO_DIR/client/python3/* $PYTHON3_LIB/


-----

TARGET_DIR
mkdir -p $TARGET_DIR/constants
cp $SOURCE_DIR/constants/thinstation.py $TARGET_DIR/constants/__init__.py

# Copy package files
echo "Copying tswebcam package files"
for FILE in bin build etc .dna dependencies; do
    \cp -r $SCRIPT_DIR/tswebcam/$FILE $TSWEBCAM_DIR/
done

echo "Creating symbolic links"
ln -snf  /lib/tswebcam/tswebcam-ctl.py $TSWEBCAM_DIR/bin/tswebcam-ctl
ln -snf ../tswebcam.service $TSWEBCAM_DIR/etc/systemd/system/multi-user.target.wants/tswebcam.service

# Copy the list of PA devices to be avoided
echo "Copying list of PulseAudio devices to avoid"
\cp $SCRIPT_DIR/pulse/avoid.list $PA_ETC/

# Hijack /lib/tlclient/ssh
echo "Updating tlclient ssh binary"
[ -f $TLCLIENT_DIR/ssh-tl ] || mv $TLCLIENT_DIR/ssh $TLCLIENT_DIR/ssh-tl
\cp $SCRIPT_DIR/tlclient/ssh $TLCLIENT_DIR/ssh

# That's it!
echo "Ready!"
