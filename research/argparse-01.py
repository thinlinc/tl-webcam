"""
A program for testing the moment when an object is converted
to a string by ArgumentParser.
"""

import argparse

class DelayedString:
    def __init__(self, value: str, /, id: str):
        self.__accessed = False
        self.__got_size = False
        self.__value = value
        self.__id = id

    def __repr__(self) -> str:
        print(f"[{self.__id}]: repr called")
        return self.__value
    
    def __str__(self) -> str:
        print(f"[{self.__id}]: str called")
        return self.__value
    
    def __getitem__(self, index):
        if not self.__accessed:
            self.__accessed = True
            print(f"[{self.__id}]: accessed")
        return self.__value[index]
    
    def __len__(self):
        if not self.__got_size:
            self.__got_size = True
            print(f"[{self.__id}]: accessed")
        return len(self.__value)
    
    def __getattr__(self, name):
        return self.__value.__getattribute__(name)
        
print("A quick test for DelayedString")
rstr = "some string"
dstr = DelayedString(rstr, id='test')

assert len(rstr) == len(dstr), "checking len()"
assert rstr == str(dstr), "checking str()"
assert rstr == repr(dstr), "checking repr()"
assert rstr[4] == dstr[4], "accessing a single chanacter"
assert rstr[3:5] == dstr[3:5], "accessing a substring"
assert rstr.strip() == dstr.strip(), "called strip()"

print("Success!!")

parser = argparse.ArgumentParser(
    description=DelayedString(__doc__, id="Description"),
    epilog=DelayedString("This is all I wanted to say", id="Epilog")
)
parser.add_argument(
    "-a",  "--admin",
    help=DelayedString("returns the admin's account", id="help-admin"),
    action="store_true"
)
parser.add_argument(
    "-c", "--coder",
    help=DelayedString("returns the name of the coder", id="help-coder"),
    action="store_true"
)
args = parser.parse_args()

if args.admin:
    print("Admin's account: admin")
if args.coder:
    print("Coder's name is too hard to spell, sorry!")
