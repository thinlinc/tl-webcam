import asyncio


class AIter:
    def __init__(self, *values):
        self.__values = values
        self.__current = -1

    def __aiter__(self):
        return self
    
    async def __anext__(self):
        self.__current += 1
        if self.__current < len(self.__values):
            value = self.__values[self.__current]
            if isinstance(value, Exception):
                raise value
            else:
                return self.__values[self.__current]
        else:
            raise StopAsyncIteration

    def __len__(self):
        return len(self.__values)


async def test():
    LEN_MSG = "Expected length {}, but for {}"
    VALUE_MSG = "Expected {} as position {}, but got {}"

    values = [43,123,543,1]
    aiter = AIter(*values)

    expected_len = len(values)
    actual_len = len(aiter)
    assert expected_len == actual_len, LEN_MSG.format(expected_len, actual_len)

    current = 0
    async for value in aiter:
        assert values[current] == value, VALUE_MSG.format(
            values[current], current, value
        )
        current += 1
    assert current == len(values)


    exc = Exception('test')
    values = [43,123,exc,1]
    aiter = AIter(*values)

    try:
        current = 0
        async for value in aiter:
            assert values[current] == value, VALUE_MSG.format(
                values[current], current, value
            )
            current += 1
        assert False, "No exception has been received"
    except AssertionError as err:
        raise err
    except Exception as err:
        assert exc == err, "A wrong exception"


if __name__ == "__main__":
    asyncio.run(test())
