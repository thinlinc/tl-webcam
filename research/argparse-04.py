"""
A demonstation of using subparsers. The program takes several
named parameters and positional parameters, where the amount
of the latter is based on the value of the first one.
"""

import argparse


class customtype(argparse._SubParsersAction):
    def __call__(self, parser, namespace, value, option_string=None):
        print('calling customtype')
        super().__call__(parser, namespace, value, option_string)
        setattr(namespace, self.dest + '_dbl', value[0]+value[0])


parser = argparse.ArgumentParser(add_help=False)

parser.add_argument(
    "-u", "--user",
    help="The name of the user",
    default="(current)"
)
parser.add_argument(
    "-t", "--time",
    help="The time of the action of the user",
    type=float,
    required=True
)

main_parser = argparse.ArgumentParser(
    description=__doc__,
    parents=[parser]
)

subparsers = main_parser.add_subparsers(
    dest="action",
    metavar='action',
#    title="",
    help="Allowed commands",
    action=customtype
)

sleep_parser = subparsers.add_parser(
    "sleep",
    help="The user goes to bed",
#    parents=[parser]
)
sleep_parser.add_argument(
    "place",
    help="where should the user go to sleep?"
)
sleep_parser.add_argument(
    "duration",
    type=int,
    help="For how many hours should the user sleep?"
)

subparsers.add_parser(
    'wakeup',
    help="Description of wakeup",
#    parents=[parser]
)

eat_parser = subparsers.add_parser(
    "eat",
    help="A user will eat",
#    parents=[parser]
)
eat_parser.add_argument(
    "meal",
    help="The name of the meal to eat"
)

received = main_parser.parse_args()

print("Parameters:")
for key, value in received.__dict__.items():
    print(f"  {key}:\t{value}")
