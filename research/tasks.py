import asyncio
from typing import Dict

TASK_SUCCESS = 0
TASK_FAIL = 1
TASK_CANCEL = 2

class TaskRunner:
    def __init__(self, name: str, end_method: int = TASK_SUCCESS):
        self.stop_event = asyncio.Event()
        self.end_method = end_method
        self.name = name
        self.task = asyncio.create_task(self.run(), name=name)
        self.task.add_done_callback(self.on_task_done)

    def on_task_done(self, task: asyncio.Task):
        print("{}: done callback: Done: {}. Cancelled: {}".format(
            task.get_name(),
            task.done(),
            task.cancelled()
        ))

    def stop(self):
        if self.end_method == TASK_CANCEL:
            print(f"{self.name}: cancelling")
            self.task.cancel()
        else:
            print(f"{self.name}: about to stop")
            self.stop_event.set()

    async def run(self):
        self.stop_event.clear()
        print(f"{self.name}: task is running")
        await self.stop_event.wait()
        print(f"{self.name}: task is finishing")
        if self.end_method == TASK_FAIL:
            raise Exception(f"{self.name} task has has failed")
        else:
            print(f"{self.name}: task has stopped")
        
async def run_test(results: Dict[str, int]):
    for name, end_method in results.items():
        # Create a task runner
        runner = TaskRunner(name, end_method)
        # Wait a little bit
        await asyncio.sleep(0.5)
        # Stop and await the task
        runner.stop()
        try:
            await runner.task
            print(f"{runner.name} has ended")
        except asyncio.CancelledError:
            print(f"{runner.name} has been cancelled")
        except Exception as err:
            print(err)


async def run_test_unsafe(results: Dict[str, int]):
    for name, end_method in results.items():
        # Create a task runner
        runner = TaskRunner(name, end_method)
        # Wait a little bit
        await asyncio.sleep(0.5)
        # Stop and await the task
        runner.stop()
        while not runner.task.done():
            await asyncio.sleep(0.1)

        print(f"{runner.name} has ended")
        if runner.task.cancelled():
            print(f"{runner.name} has been cancelled")
        else:
            err = runner.task.exception()
            if err is not None: print(err)


if __name__ == "__main__":
    asyncio.run(run_test_unsafe({
        "Test A": TASK_SUCCESS,
        "Task B": TASK_CANCEL,
        "Task C": TASK_FAIL
    }))