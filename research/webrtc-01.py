"""
WebRTC streaming test #1

source: /dev/video0
target: output.mp4

Both sender and receiver are controlled by this script and messages are transfered
directly between them. The output stream is saved to an mp4 file.

Once the streaming starts, press 'q' or 'Q' to stop it.
"""

import asyncio
import aiortc
import logging

from aiortc.contrib.media import MediaPlayer, MediaRecorder
from sys import stdin, stdout

SOURCE_VIDEO="/dev/video0"
TARGET_VIDEO="output.mp4"

VIDEO_PARAMETERS = {
    "video_size": "640x480",
    "framerate": "30"
}


async def add_statechange_handlers(rtc: aiortc.RTCPeerConnection, logger: logging.Logger):

    @rtc.on('connectionstatechange')
    def on_connectionstatechange():
        logger.info("connection state is %s" % rtc.connectionState)
        if rtc.connectionState == "failed":
            asyncio.create_task(rtc.close())
    
    @rtc.on('iceconnectionstatechange')
    def on_iceconnectionstatechange():
        logger.info("ICE connection state is %s" % rtc.iceConnectionState)
        if rtc.iceConnectionState == "failed":
            asyncio.create_task(rtc.close())

    @rtc.on('icegatheringstatechange')
    def on_icegatheringstatechange():
        logger.info("ICE gathering state is %s" % rtc.iceGatheringState)

    @rtc.on('signalingstatechange')
    def on_signalingstatechange():
        logger.info("Signaling state is %s" % rtc.signalingState)

    # Call all handlers in order to log initial states
    on_connectionstatechange()
    on_iceconnectionstatechange()
    on_icegatheringstatechange()
    on_signalingstatechange()


class Publisher:
    def __init__(self, source: MediaPlayer):
        self.source = source
        self.rtc = None
        self.logger = logging.getLogger('sender')

    async def open(self):
        self.logger.info("opening a connection")
        self.rtc = aiortc.RTCPeerConnection(aiortc.RTCConfiguration([]))
        await add_statechange_handlers(self.rtc, self.logger)

    async def create_offer(self):
        self.logger.info("adding the video track")
        self.rtc.addTrack(self.source.video)
        self.logger.info("creating an offer")
        offer = await self.rtc.createOffer()

        self.logger.info("setting the local description")
        await self.rtc.setLocalDescription(offer)
        self.logger.info("returning the local description")
        return self.rtc.localDescription

    async def accept_answer(self, answer):
        self.logger.info("accepting an answer")
        await self.rtc.setRemoteDescription(answer)
        self.logger.info("the connection is prepared")

    async def close(self):
        await self.rtc.close()


class Receiver:
    def __init__(self, target: MediaRecorder):
        self.target = target
        self.rtc = None
        self.logger = logging.getLogger('receiver')

    async def open(self):
        self.logger.info("opening a connection")
        self.rtc = aiortc.RTCPeerConnection(aiortc.RTCConfiguration([]))
        await add_statechange_handlers(self.rtc, self.logger)

        @self.rtc.on("track")
        def on_track(track):
            self.logger.info("received a track: %s" % track.kind)
            if track.kind != "video": return
            self.target.addTrack(track)

            @track.on("ended")
            async def on_ended():
                self.logger.info("Track has ended: %s" % track.kind)
                await self.target.stop()


    async def create_answer(self, offer):
        self.logger.info("accepting an offer")
        await self.rtc.setRemoteDescription(offer)
        self.logger.info("remote description is set")
        await self.target.start()

        self.logger.info("prepring an answer")
        answer = await self.rtc.createAnswer()
        self.logger.info("setting the local description")
        await self.rtc.setLocalDescription(answer)

        self.logger.info("returning the local description")
        return self.rtc.localDescription

    async def close(self):
        await self.rtc.close()


async def connect_stdio():
    loop = asyncio.get_event_loop()
    astdin = asyncio.StreamReader()
    protocol = asyncio.StreamReaderProtocol(astdin)
    await loop.connect_read_pipe(lambda: protocol, stdin)
    w_transport, w_protocol = await loop.connect_write_pipe(asyncio.streams.FlowControlMixin, stdout)
    astdout = asyncio.StreamWriter(w_transport, w_protocol, astdin, loop)
    return astdin, astdout

async def main():
    
    astdin, astdout = await connect_stdio()

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    handler = logging.StreamHandler(stdout)
    handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)-6s %(name)-10s %(message)s'))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    source = Publisher(MediaPlayer(
        SOURCE_VIDEO,
        format="v4l2",
        options=VIDEO_PARAMETERS
    ))
    await source.open()

    target = Receiver(MediaRecorder("output.mp4"))
    await target.open()

    # a dirty trick to exchange ICE candidates without using a channel
    @source.rtc.on("icecandidate")
    def on_source_icecandidate(candidate):
        if candidate:
            target.logger.info("received an ICE candidate")
            target.rtc.addIceCandidate(candidate)

    @target.rtc.on("icecandidate")
    def on_target_icecandidate(candidate):
        if candidate:
            source.logger.info("received an ICE candidate")
            source.rtc.addIceCandidate(candidate)

    offer = await source.create_offer()
    answer = await target.create_answer(offer)
    await source.accept_answer(answer)

    astdout.write(b"\nStreaming. Press q to end\n\n")
    while True:
        ch = await astdin.read(1)
        if ch in (b'q', b'Q'): break

    logger.info("stopping the streams")

    await target.close()
    await source.close()

    # Allow logs to be sent to stdout
    await asyncio.sleep(0.5)

if __name__ == "__main__":
    print(__doc__)
    asyncio.run(main())

