#!/bin/bash

# Get the name of the pipe
PIPE="$1"
if [ "$1" != "" ]; then
    PIPE="$1"
else
    PIPE="./testin"
fi

# Determine if the pipe already exists and create it if not
if [ -e $PIPE ]; then
    PIPE_EXISTS=1
    echo "Using an existing pipe $PIPE"
else
    PIPE_EXISTS=
    echo "Creating a new pipe $PIPE"
    mkfifo $PIPE
fi

# Keep the pipe open
sleep infinity > $PIPE &
SLEEP_PID=$!

echo "Launching the reader"
./pipereader.sh < $PIPE

kill $SLEEP_PID
if [ -z $PIPE_EXISTS ]; then
    echo "Removing the pipe $PIPE"
    \rm $PIPE
fi

echo "Done"

