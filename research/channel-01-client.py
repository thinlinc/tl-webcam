"""
WebRTC signaling channel test #1 (client)

Connects to a TCP socket and sends a simple message
"""

import asyncio
import logging

DEFAULT_HOST=('127.0.0.1', 8888)

async def make_request(host: str, port: int, name: str):
    logger = logging.getLogger(f'{host}:{port}')
    # Open a connection
    logger.info('opening')
    reader, writer = await asyncio.open_connection(host, port)
    # Send a message
    logger.info(f"< {name}")
    writer.write(name.encode())
    await writer.drain()
    # Receive a response
    data = await reader.read(100)
    logger.info(f"> {data.decode()}")
    # Close the connection
    logger.info("closing")
    writer.close()
    await writer.wait_closed()


if __name__ == "__main__":

    from utils import parse_host, create_logger
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-a", "--addr",
        help=f"The address of the server in the format host:port, each component is optional. Default: {DEFAULT_HOST[0]}:{DEFAULT_HOST[1]}",
        type=str,
        required=False
    )
    parser.add_argument(
        "name",
        help="Name to greet",
        default="World",
        type=str,
        nargs='?'
    )
    args = parser.parse_args()
    host, port = parse_host(args.addr, default=DEFAULT_HOST)

    create_logger()

    asyncio.run(make_request(host, port, args.name))
