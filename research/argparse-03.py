"""
Custom types and actions
"""

from argparse import ArgumentParser, ArgumentTypeError, Action

def NumericString(value: str):
    allowed = '0123456789'
    if all(x in allowed for x in value):
        return value
    else:
        raise ArgumentTypeError(f"'{value}' must be a numeric string")
    
def Square(value: str):
    try:
        num = int(value)
        return num*num
    except Exception:
        raise ArgumentTypeError(f"'{value}' must be an integer")

class DictItem(object):
    def __init__(self, **values):
        self.__values = values
    def __call__(self, key: str):
        if key not in self.__values:
            raise ArgumentTypeError("'{}' must be one of {}".format(
                key, ", ".join(self.__values)
            ))
        return self.__values[key]

class CustomAction(Action):
    def __init__(self, *args, dictionary, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dictionary = dictionary

    def __call__(self, parser, namespace, values, option_string=None):
        # parser: ArgumentParser    the running parser
        # namespace: Namespace      the (partial) result of parsing
        # values: Any               the argument converted according to 'type'
        # option_string: str        the name of the option (None for positional arguments)

        print("Processing {} of type {} with values {}".format(option_string, type(values), repr(values)))
        if isinstance(values, list):
            if any(value not in self.__dictionary for value in values):
                raise ArgumentTypeError("'{}' must be one of {}".format(
                    value, ", ".join(self.__dictionary)
                ))
            processed = tuple(self.__dictionary[value] for value in values)
        elif values not in self.__dictionary:
            raise ArgumentTypeError("'{}' must be one of {}".format(
                values, ", ".join(self.__dictionary)
            ))
        else:
            processed = self.__dictionary[values]
        setattr(namespace, self.dest, processed)

if __name__ == "__main__":
    parser = ArgumentParser(description=__doc__)
    parser.add_argument(
        "-n", "--numeric",
        help="An integered tested with NumericString",
        type=NumericString
    )
    parser.add_argument(
        "-s", "--square",
        help="Converts the number to a square",
        type=Square
    )
    parser.add_argument(
        "-v", "--value",
        help="A value from a private dictionary",
        type=DictItem(a=42, b=87, xyz="The end of world is near!")
    )
    parser.add_argument(
        "-d", "--dict",
        help="One value from a private dictionary",
        action=CustomAction,
        dictionary=dict(one=1, two=2, three=3)
    )
    parser.add_argument(
        "-t", "--two-dict",
        help="Two values from a private dictionary",
        action=CustomAction,
        nargs=2,
        dictionary=dict(one=1, two=2, three=3)
    )

    args = vars(parser.parse_args())
    max_key_length = max( len(key) for key in args )
    for key, value in args.items():
        print(key + ':'.ljust(max_key_length - len(key) + 2) + str(value))
