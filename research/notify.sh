#!/bin/sh

echo "Waiting for $1 to stop"
RC=$(wait "$1")
echo "$1 has stopped with code $RC."

