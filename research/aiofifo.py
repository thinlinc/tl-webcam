import asyncio
import aiofiles
from os import mkfifo, unlink

FIFO_NAME = "testfifo"

async def main():
    running = True

    async def print_dots():
        while running:
            print('.', end="", flush=True)
            await asyncio.sleep(1)

    dot_task = asyncio.create_task(print_dots())

    while running:
        try:
            async with aiofiles.open(FIFO_NAME, "r") as fifo:
                print("OPENED")
                async for line in fifo:
                    print(line.strip())
                    if line.strip() == 'quit':
                        running = False
                        break
            print("CLOSED")
        except Exception as err:
            print("EXCEPTION: " + repr(err))
    
    await dot_task

if __name__ == "__main__":
    mkfifo(FIFO_NAME)
    asyncio.run(main())
    unlink(FIFO_NAME)
