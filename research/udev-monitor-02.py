import asyncio
from pyudev import Context, Monitor, MonitorObserver, Device
from sys import stdin

from utils import connect_stdio

def print_event(device: Device):
    print('{0.action}: {0.device_path}, {0.device_node}'.format(device))
    print(device)


async def main():

    loop = asyncio.get_event_loop()
    data = asyncio.Queue()

    def handle_event(device: Device):
        asyncio.run_coroutine_threadsafe(data.put(device), loop)

    context = Context()
    monitor = Monitor.from_netlink(context)
    monitor.filter_by('video4linux')

    observer = MonitorObserver(monitor, callback=handle_event)
    observer.start()

    async def print_task():
        try:
            while True:
                await asyncio.sleep(1)
                print('.', end="", flush=True)
        except asyncio.CancelledError:
            pass

    async def print_event_task():
        while True:
            event = await data.get()
            if event == 'stop':
                break
            else:
                print_event(event)


    print("Monitoring devices. Type 'q' to stop")
    printing_task = asyncio.create_task(print_task())
    event_task = asyncio.create_task(print_event_task())

    astdin, astdout = await connect_stdio()
    while await astdin.read(1) != b'q':
        pass
    printing_task.cancel()
    observer.stop()
    await data.put('stop')
#    await printing_task
    await event_task
    print("Monitoring has been stopped.")


asyncio.run(main())
