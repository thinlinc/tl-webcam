"""
Uses inotify to detect when a video device is accessed. When the access
is very quick then it is assumed that the devices is only probed. Otherwise
it is assumed that an application tried to capture the video stream.
"""

import sys
import asyncio
import glob
from pathlib import Path
from asyncinotify import Inotify, Mask

class OpenModeDetector:

    __instance_id: int = 0
    @classmethod
    def instance_id(cls) -> int:
        cls.__instance_id += 1
        return cls.__instance_id

    def __init__(self, event, loop: asyncio.AbstractEventLoop = None):
        self.id = self.instance_id()
        self.loop = loop or asyncio.get_event_loop()
        self.opened = False
        self.streaming = False
        self.event = event
        self.time = 0

    def open(self, delay: float = 0.1):
        if self.opened:
            print(f"[{self.id}] {self.event.path} ERROR: already opened")
            return
        self.opened = True
        self.streaming = False
        self.time = self.loop.time()
        self.timer = self.loop.call_later(delay, self.notify_streaming)

    def close(self):
        if not self.opened:
            print(f"[{self.id}] {self.event.path}: ERROR closed but was not opened")
            return
        self.opened = False
        duration = str(int((self.loop.time() - self.time)*1000)).rjust(4, '0')
        duration = duration[:-3] + "." + duration[-3:]
        if self.streaming:
            print(f"[{self.id}] {self.event.path}: closed after {duration} seconds")
            self.streaming = False
        elif not self.streaming:
            self.timer.cancel()
            print(f"[{self.id}] {self.event.path}: probed for {duration} seconds")

    def notify_streaming(self):
        self.streaming = True
        print(f"[{self.id}] {self.event.path}: capturing")


async def main(pattern):
    print(f"Monitoring {pattern}")
    timers = dict()
    loop = asyncio.get_event_loop()
    with Inotify() as inotify:
        for path in glob.glob(pattern):
            print(f"- found {path}")
            inotify.add_watch(path, Mask.OPEN | Mask.CLOSE)
        print(f"Processing events")

        async for event in inotify:
            timer = timers.get(event.path)
            if timer is None:
                timer = OpenModeDetector(event, loop)
                timers[event.path] = timer
            if event.mask == Mask.OPEN:
                timer.open()
            else:
                timer.close()


path = sys.argv[1] if len(sys.argv) > 1 else '/dev/video*'
loop = asyncio.new_event_loop()
try:
    loop.run_until_complete(main(path))
except KeyboardInterrupt:
    print("Shutting down")
    loop.run_until_complete(loop.shutdown_asyncgens())
    loop.close()
