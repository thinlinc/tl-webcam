"""
A collection of useful functions
"""
import re
import socket
import logging
import asyncio

from typing import Tuple, Optional, Union
from sys import stdout, stdin


def parse_host(param: Optional[str], /, default: Tuple[str, int]):
    if not param: return default

    addr = param.split(':', 1)
    if addr[0] == 'unix':
        host = 'sock' if len(addr) == 1 else addr[1]
        return host, -1
    elif addr[0]:
        ip_match = re.match(r"(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})", addr[0])
        if ip_match is None or any((int(n) in range(0,256) for n in ip_match.groups())):
            host = socket.gethostbyname(addr[0])
        else:
            host = addr[0]
    else:
        host = default[0]

    port = default[1] if len(addr) == 1 else int(addr[1])

    return host, port

def addr2hostport(addr: Union[str,Tuple[str,int]]) -> str:
    return addr if isinstance(addr, str) else f"{addr[0]}:{addr[1]}"


def create_logger(name_size=10):

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    handler = logging.StreamHandler(stdout)
    handler.setFormatter(logging.Formatter(f'%(asctime)s %(levelname)-7s %(name)-{name_size}s %(message)s'))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    return logger


async def connect_stdio():
    loop = asyncio.get_event_loop()
    astdin = asyncio.StreamReader()
    protocol = asyncio.StreamReaderProtocol(astdin)
    await loop.connect_read_pipe(lambda: protocol, stdin)
    w_transport, w_protocol = await loop.connect_write_pipe(asyncio.streams.FlowControlMixin, stdout)
    astdout = asyncio.StreamWriter(w_transport, w_protocol, astdin, loop)
    return astdin, astdout


def run(*tasks, **menu):
    loop = asyncio.get_event_loop()
    logger = create_logger()
    astdin, astdout = loop.run_until_complete(connect_stdio())
    atasks = [loop.create_task(task) for task in tasks]
    if len(menu) == 0:
        astdout.write(b"Type 'q' to exit\n")
        loop.run_until_complete(astdout.drain())
        while True:
            ch = loop.run_until_complete(astdin.read(1))
            if ch == b'q':
                for atask in atasks:
                    atask.cancel()
                break
    else:
        astdout.write(b"Options\n")
        for key, item in menu.items():
            astdout.write(f"\t{key} - {item[0]}\n".encode())
        loop.run_until_complete(astdout.drain())
        exit = False
        while not exit:
            ch = loop.run_until_complete(astdin.read(1)).decode()
            if ch in menu:
                action = menu[ch][1]
                exit = action(*atasks)

    async def safe_await(task):
        try:
            await task
        except asyncio.CancelledError:
            pass
        except Exception as err:
            logger.exception(err)

    safe_tasks = [safe_await(atask) for atask in atasks]
    loop.run_until_complete(asyncio.gather(*safe_tasks))
    loop.close()




if __name__ == "__main__":

    from sys import argv

    ACTION_STOP = 'stop'
    ACTION_STATUS = 'status'
    
    class TaskCtrl:
        def __init__(self):
            self.event: asyncio.Event = asyncio.Event()
            self.action: Optional[str] = None

        def set(self, action: str):
            self.action = action
            self.event.set()

        async def wait(self) -> str:
            self.event.clear()
            await self.event.wait()
            return self.action


    async def test_task_a(controller: TaskCtrl):
        logger = logging.getLogger('task-a')
        logger.info('running')
        loop = asyncio.get_event_loop()
        start_time = loop.time()
        try:
            while True:
                action = await controller.wait()
                if action == ACTION_STOP:
                    break
                elif action == ACTION_STATUS:
                    running_time = loop.time() - start_time
                    logger.info(f'active for {running_time} seconds')
        finally:
            logger.info('stopped')

    async def test_task_b(controller: TaskCtrl):
        logger = logging.getLogger('task-b')
        logger.info('running')
        loop = asyncio.get_event_loop()
        start_time = loop.time()
        try:
            while True:
                action = await controller.wait()
                if action == ACTION_STOP:
                    break
                elif action == ACTION_STATUS:
                    running_time = loop.time() - start_time
                    logger.info(f'active for {running_time} seconds')
        except asyncio.CancelledError:
            logger.info('cancelled')
        finally:
            logger.info('stopped')



    # A basic test

    controller_a = TaskCtrl()
    controller_b = TaskCtrl()

    def stop_both(task_a, task_b) -> bool:
        controller_a.set(ACTION_STOP)
        controller_b.set(ACTION_STOP)
        return True

    def status_query(controller: TaskCtrl):
        def action(*tasks):
            controller.set(ACTION_STATUS)
            return False
        return action

    if len(argv) > 1 and argv[1] in ("--menu", "-m"):
        run(
            test_task_a(controller_a),
            test_task_b(controller_b),
            a = ('check status of task A', status_query(controller_a)),
            b = ('check status of task B', status_query(controller_b)),
            q = ('quit', stop_both)
        )
    else:
        run(
            test_task_a(controller_a),
            test_task_b(controller_b)
        )
