import asyncio
import aiofiles
from io import IOBase

from typing import Dict, Optional, Callable

from utils import TaskManager


class Server:
    def __init__(self, /, handlers: Dict[str, Callable], protocol):
        self.handlers = handlers
        self.connections = TaskManager()
        self.__server_task: Optional[asyncio.Task] = None
        self.__protocol = protocol

    @property
    def serving(self):
        return self.__server_task and not self.__server_task.done()

    def accept_connection(self, reader, writer):
        self.connections.create_task(self.__handle_connection(reader, writer))

    async def __handle_connection(
            self,
            reader: asyncio.StreamReader,
            writer: asyncio.StreamWriter
        ):
        channel = self.__protocol(reader, writer)
        try:
            await channel.authenticate()
            while not channel.is_closing:
                request, *args = await channel.recv()
                handler = self.handlers.get(request)
                if handler is None:
                    raise Exception("uknown request", request)
                result = handler(channel, *args)
                await channel.send(result)
        except asyncio.CancelledError:
            pass
        except Exception:
            pass
        await channel.close()

    async def serve(self):
        pass

    def start_serving(self):
        if self.serving: return
        self.__server_task = asyncio.create_task(self.serve())

    def stop_serving(self):
        self.__server_task.cancel()

    async def wait_stopped(self):
        if self.serving:
            await self.__server_task


# Unix socket server

class UnixServer(Server):
    def __init__(self, path: str, /, handlers, protocol):
        super().__init__(handlers=handlers, protocol=protocol)
        self.path = path

    async def serve(self):
        server = await asyncio.start_unix_server(self.accept_connection, self.path)
        with server:
            await server.serve_forever()



# Inet socket server

class InetServer(Server):
    def __init__(self, host: str, port: int, /, handlers, protocol):
        super().__init__(handlers=handlers, protocol=protocol)
        self.host = host
        self.port = port

    async def serve(self):
        server = await asyncio.start_server(self.accept_connection, self.host, self.port)
        with server:
            await server.serve_forever()



class PipeWrapper:

    def __init__(self, pipe):
        if isinstance(pipe, IOBase):
            self.pipe = pipe
            self.path = None
            self.__must_close = False
        elif isinstance(pipe, str):
            self.pipe = None
            self.path = pipe
            self.__must_close = True
        else:
            raise TypeError('the argument must be a string of a stream object')

    async def open(self, mode):
        if self.pipe is None:
            self.pipe = await aiofiles.open(self.path, mode)
        return self.pipe

    async def close(self):
        if self.__must_close and self.pipe:
            pipe, self.pipe = self.pipe, None
            await pipe.close()


    
class PipeServer(Server):
    def __init__(self, inpipe, outpipe, /, handler, protocol):
        super().__init__(handlers=handler, protocol=protocol)
        self.__inpipe = PipeWrapper(inpipe)
        self.__outpipe = PipeWrapper(outpipe)

    async def serve(self):
        while True:
            # Prepare pipes
            input = await self.__inpipe.open('rb')
            output = await self.__outpipe.open('wb')
            # Create asynchronous pipe reader and writer
            loop = asyncio.get_event_loop()
            reader = asyncio.StreamReader()
            r_protocol = asyncio.StreamReaderProtocol(reader)
            await loop.connect_read_pipe(lambda: r_protocol, input)
            w_transport, w_protocol = await loop.connect_write_pipe(asyncio.streams.FlowControlMixin, output)
            writer = asyncio.StreamWriter(w_transport, w_protocol, reader, loop)
            # Start listening
            self.accept_connection(reader, writer)
            await writer.wait_closed()
