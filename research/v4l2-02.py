"""
V4L2 loopback feeding test #1

target: /dev/video10

Generates a moving flag animation and sends it to a video loopback device
"""

import asyncio

import cv2
import numpy
import math

import fcntl
from v4l2py import device as v4l2
import time

from typing import List, Dict, Tuple, Optional, BinaryIO

DEFAULT_VIDEO_INDEX=10

class VideoEncoder:
    name = "(unknown format)"

    @staticmethod
    def pixel_format(
        width,
        height,
        pix_fmt=v4l2.raw.V4L2_PIX_FMT_RGB24,
        colorspace=v4l2.raw.V4L2_COLORSPACE_JPEG
    ):
        format = v4l2.raw.v4l2_format()
        format.type = v4l2.raw.V4L2_BUF_TYPE_VIDEO_OUTPUT
        format.fmt.pix.pixelformat = pix_fmt
        format.fmt.pix.width = width
        format.fmt.pix.height = height
        format.fmt.pix.field = v4l2.raw.V4L2_FIELD_NONE
        format.fmt.pix.colorspace = colorspace
        return format

    @staticmethod
    def encode(rgb: numpy.ndarray) -> bytes:
        pass


class RGB24encoder(VideoEncoder):
    name = 'RGB24'

    @staticmethod
    def pixel_format(width, height):
        format = super(RGB24encoder, RGB24encoder).pixel_format(
            width,
            height,
            pix_fmt = v4l2.raw.V4L2_PIX_FMT_RGB24,
            colorspace = v4l2.raw.V4L2_COLORSPACE_SRGB
        )
        format.fmt.pix.bytesperline = width * 3
        format.fmt.pix.sizeimage = width * height * 3
        return format

    @staticmethod
    def encode(rgb: numpy.ndarray) -> bytes:
        return rgb.tobytes()


class YUYVencoder(VideoEncoder):
    """
    Packed 4:2:2 YUV

    +-------------+-------------+----
    | Y0 U0 Y1 V0 | Y2 U2 Y3 V2 | ...
    +-------------+-------------+----
    Y component: 8 bits/pixel
    U component: 4 bits/pixel (average of two)
    V component: 4 bits/pixel (average of two)

    Total size in bytes: pixels * 2
    """

    name = 'YUV422'

    @staticmethod
    def pixel_format(width, height):
        format = super(YUYVencoder, YUYVencoder).pixel_format(
            width,
            height,
            pix_fmt = v4l2.raw.V4L2_PIX_FMT_YUYV,
            colorspace = v4l2.raw.V4L2_COLORSPACE_JPEG
        )
        format.fmt.pix.bytesperline = width * 2
        format.fmt.pix.sizeimage = width * height * 2
        return format

    @staticmethod
    def encode(rgb: numpy.ndarray) -> bytes:
        height = len(rgb)
        width = len(rgb[0])
        buff: numpy.ndarray = numpy.zeros(height*width*2, dtype=numpy.uint8)
        imgrey = rgb[:,:,0] * 0.299 + rgb[:,:,1] * 0.587 + rgb[:,:,2] * 0.114
        Pb = rgb[:,:,0] * -0.168736 + rgb[:,:,1] * -0.331264 + rgb[:,:,2] * 0.5
        Pr = rgb[:,:,0] * 0.5 + rgb[:,:,1] * -0.418688 + rgb[:,:,2] * -0.081312

        cursor = 0
        for y in range(0, height):
            for x in range(0, width, 2):
                buff[cursor] = imgrey[y, x]
                buff[cursor+1] = 0.5 * (Pb[y, x] + Pb[y, x+1]) + 128
                buff[cursor+2] = imgrey[y, x+1]
                buff[cursor+3] = 0.5 * (Pr[y, x] + Pr[y, x+1]) + 128
                cursor += 4

        return buff.tobytes()


class YVYUencoder(VideoEncoder):
    """
    Packed 4:2:2 YVU

    +-------------+-------------+----
    | Y0 V0 Y1 U0 | Y2 V2 Y3 U2 | ...
    +-------------+-------------+----
    Y component: 8 bits/pixel
    U component: 4 bits/pixel (average of two)
    V component: 4 bits/pixel (average of two)

    Total size in bytes: pixels * 2
    """

    name = 'YVU422'

    @staticmethod
    def pixel_format(width, height):
        format = super(YVYUencoder, YVYUencoder).pixel_format(
            width,
            height,
            pix_fmt = v4l2.raw.V4L2_PIX_FMT_YVYU,
            colorspace = v4l2.raw.V4L2_COLORSPACE_JPEG
        )
        format.fmt.pix.bytesperline = width * 2
        format.fmt.pix.sizeimage = width * height * 2
        return format

    @staticmethod
    def encode(rgb: numpy.ndarray) -> bytes:
        height = len(rgb)
        width = len(rgb[0])
        buff: numpy.ndarray = numpy.zeros(height*width*2, dtype=numpy.uint8)
        imgrey = rgb[:,:,0] * 0.299 + rgb[:,:,1] * 0.587 + rgb[:,:,2] * 0.114
        Pb = rgb[:,:,0] * -0.168736 + rgb[:,:,1] * -0.331264 + rgb[:,:,2] * 0.5
        Pr = rgb[:,:,0] * 0.5 + rgb[:,:,1] * -0.418688 + rgb[:,:,2] * -0.081312

        cursor = 0
        for y in range(0, height):
            for x in range(0, width, 2):
                buff[cursor] = imgrey[y, x]
                buff[cursor+1] = 0.5 * (Pr[y, x] + Pr[y, x+1]) + 128
                buff[cursor+2] = imgrey[y, x+1]
                buff[cursor+3] = 0.5 * (Pb[y, x] + Pb[y, x+1]) + 128
                cursor += 4

        return buff.tobytes()


class YUV420encoder(VideoEncoder):
    """
    Planar 4:2:0 YUV

    +--------------+--------------+--------------+
    | Y0 Y1 Y2 ... | U0 U1 U2 ... | V0 V1 V2 ... |
    +--------------+--------------+--------------+
    Y component: 8 bits/pixel
    U component: 2 bits/pixel (average of 2x2)
    V component: 2 bits/pixel (average of 2x2)

    Total size in bytes: pixels * 1.5
    """

    name = "YUV420"

    @staticmethod
    def pixel_format(width, height):
        format = super(YUV420encoder, YUV420encoder).pixel_format(
            width,
            height,
            pix_fmt = v4l2.raw.V4L2_PIX_FMT_YUV420,
            colorspace = v4l2.raw.V4L2_COLORSPACE_JPEG
        )
        bytesperline = width + (width >> 1)
        format.fmt.pix.bytesperline = bytesperline
        format.fmt.pix.sizeimage = bytesperline * height
        return format

    @staticmethod
    def encode(rgb: numpy.ndarray) -> bytes:
        height = len(rgb)
        width = len(rgb[0])
        buff: numpy.ndarray = numpy.zeros((int)(height*width*3/2), dtype=numpy.uint8)
        imgrey = rgb[:,:,0] * 0.299 + rgb[:,:,1] * 0.587 + rgb[:,:,2] * 0.114
        Pb = rgb[:,:,0] * -0.168736 + rgb[:,:,1] * -0.331264 + rgb[:,:,2] * 0.5
        Pr = rgb[:,:,0] * 0.5 + rgb[:,:,1] * -0.418688 + rgb[:,:,2] * -0.081312

        cursor = 0
        # Y component
        for y in range(0, height):
            for x in range(0, width):
                buff[cursor] = imgrey[y, x]
                cursor += 1
        # U component
        for y in range(0, height, 2):
            for x in range(0, width, 2):
                buff[cursor] = 0.25*(Pb[y, x] + Pb[y, x+1] + Pb[y+1, x] + Pb[y+1, x+1]) + 128
                cursor += 1
        # V component
        for y in range(0, height, 2):
            for x in range(0, width, 2):
                buff[cursor] = 0.25*(Pr[y, x] + Pr[y, x+1] + Pr[y+1, x] + Pr[y+1, x+1]) + 128
                cursor += 1

        return buff.tobytes()


class FrameIterator:
    def __init__(self, frames: Tuple[bytes], fps=30):
        self.count = 0
        self.frames = frames
        self.fps = fps
        self._interval = 1.0 / fps
        self.last_frame_id = 0
        self._task = None
        self.new_frame = asyncio.Event()

    @property
    def running(self):
        return self._task is not None

    def start(self):
        self._task = asyncio.create_task(self.update_frame_counter())

    def stop(self):
        self._task.cancel()

    async def update_frame_counter(self):
        self.count = 0
        self.last_frame_id = 0
        start_timestamp = time.time()
        next_frame_time = start_timestamp
        try:
            while True:
                next_frame_time = start_timestamp + (self.count+1) * self._interval
                await asyncio.sleep(next_frame_time - time.time())
                self.count += 1
                self.last_frame_id = self.count % len(self.frames)
                self.new_frame.set()
        except Exception:
            self.new_frame.set()
        self._task = None

    async def recv(self):
        if self.running:
            await self.new_frame.wait()
            self.new_frame.clear()
        return self.frames[self.last_frame_id]


def create_rectangle(width, height, color):
    data = numpy.zeros((height, width, 3), numpy.uint8)
    data[:, :] = color
    return data
    
def create_vertical_flag(
    width = 320,
    height = 240,
    left = (255,0,0),
    middle = (255,255,255),
    right = (0,0,255)
) -> numpy.ndarray:
    stripe_width = int(width/3)
    return numpy.hstack([
        create_rectangle(stripe_width, height, left),
        create_rectangle(width - stripe_width*2, height, middle),
        create_rectangle(stripe_width, height, right)
    ])

def create_horizontal_flag(
    width = 320,
    height = 240,
    top = (255,0,0),
    middle = (255,255,255),
    bottom = (0,0,255)
) -> numpy.ndarray:
    stripe_height = int(height/3)
    return numpy.vstack([
        create_rectangle(width, stripe_height, top),
        create_rectangle(width, height - stripe_height*2, middle),
        create_rectangle(width, stripe_height, bottom)
    ])

def create_swiss_flag(size = 320) -> numpy.ndarray:
    data = numpy.zeros((size, size, 3), numpy.uint8)
    data[:, :] = (255,0,0)
    for x in range(120, 201):
        for y in range(40, 281):
            data[x,y] = (255,255,255)
            data[y,x] = (255,255,255)
    return data


class FlagVideo:
    """
    A video track that returns an animated flag.
    """

    def __init__(
        self,
        name: str,
        flag: numpy.ndarray,
        width: int = 0,
        height: int = 0
    ):
        print(f"Generating {name}")
        flag_width = len(flag[0])
        flag_height = len(flag)
        if width == 0: width = flag_width
        if height == 0: height = int(width * flag_height / flag_width)

        print(f"  flag size:  {flag_width}x{flag_height}")
        print(f"  movie size: {width}x{height}")

        self.name = name
        self.counter = 0
        self.width = width
        self.height = height
        self.encoded = dict()

        # center the flag
        M = numpy.float32([
            [1, 0, (width - flag_width) / 2],
            [0, 1, (height - flag_height) / 2]
        ])
        flag = cv2.warpAffine(flag, M, (width, height))

        # compute animation
        omega = 2 * math.pi / height
        id_x = numpy.tile(numpy.array(range(width), dtype=numpy.float32), (height, 1))
        id_y = numpy.tile(
            numpy.array(range(height), dtype=numpy.float32), (width, 1)
        ).transpose()

        self.frames: List[numpy.ndarray] = []
        for k in range(30):
            phase = 2 * k * math.pi / 30
            map_x = id_x + 10 * numpy.cos(omega * id_x + phase)
            map_y = id_y + 10 * numpy.sin(omega * id_x + phase)
            self.frames.append(
                cv2.remap(flag, map_x, map_y, cv2.INTER_LINEAR)
            )

    def encode(self, encoder: Optional[VideoEncoder] = None) -> Tuple[bytes]:
        if encoder is None:
            encoder = RGB24encoder
        frames = self.encoded.get(encoder.name)
        if frames is None:
            print(f"Encoding {self.name} into {encoder.name}")
            frames = tuple( (encoder.encode(f) for f in self.frames) )
            self.encoded[encoder.name] = frames
        return frames


class MovieController:
    def __init__(self, device: BinaryIO, flags):
        self.device = device
        self.flags: Dict[str, FlagVideo] = flags
        self.current_encoder = RGB24encoder
        self.current_movie: Optional[str] = None
        self.current_frames: Optional[Tuple(bytes)]
        self.current_frame = 0
        self.max_frame = 0
        self._fps = 0
        self._current_frame = 0
        self._start_time = 0
        self._interval = 0
        self.animation_task: Optional[asyncio.Task] = None

    @property
    def fps(self):
        return self._fps
    
    @fps.setter
    def fps(self, value):
        self._fps = value
        self._current_frame = 0
        self._start_time = time.time()
        self._interval = 1.0 / value

    def start(self, fps=30):
        if self.animation_task is None: return
        if self.current_movie is None:
            self.set_movie(next(iter(self.flags)))
        self.animation_task = asyncio.create_task(self.run_animation(fps))

    def stop(self):
        if self.animation_task and not self.animation_task.done():
            self.animation_task.cancel()
        self.animation_task = None

    async def run_animation(self, fps=30):
        try:
            self.fps = fps
            print(f"Sending frames at {self._fps} FPS (every {self._interval} sec)")
            self._current_frame = 0
            current_frame_id = 0
            start_time = time.time()
            while True:
                if self.max_frame > 0:
                    if current_frame_id >= self.max_frame:
                        current_frame_id = 0
                    frame = self.current_frames[current_frame_id]
                    written = self.device.write(frame)
                    print('*' if written == len(frame) else '!', end="", flush=True)
                    current_frame_id += 1
                self.current_frame += 1
                await asyncio.sleep(
                    (start_time + self.current_frame * self._interval) - time.time()
                )
        except asyncio.CancelledError:
            pass
        except Exception as err:
            print(f"Animation error: {err}")


    def set_movie(self, name: str, encoder: Optional[VideoEncoder] = None):
        movie = self.flags.get(name)
        if movie is None: return
        if encoder is not None: self.current_encoder = encoder
        self.current_movie = name
        width = movie.width
        height = movie.height
        print(f"Image shape:\n\twidth: {width}\n\theight: {height}")
        format = self.current_encoder.pixel_format(width, height)
        print("set format result: ", end="")
        result = fcntl.ioctl(self.device, v4l2.raw.VIDIOC_S_FMT, format)
        print(result)
        self.current_frames = movie.encode(self.current_encoder)
        self.max_frame = len(self.current_frames)
        

if __name__=="__main__":

    import argparse
    from utils import run

    Encoders = {
        'rgb24': RGB24encoder,      # fine for Chrome
        'yuv420': YUV420encoder,    # fine for Chrome
        'yuyv': YUYVencoder,        # fine for Chrome
        'yvyu': YVYUencoder         # bad
    }

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "--target",
        help="The index of a device to be written to.",
        default=DEFAULT_VIDEO_INDEX,
        type=int
    )
    parser.add_argument(
        "--pix_fmt",
        help="Pixel format.",
        default="rgb24",
        choices=list(Encoders)
    )
    args = parser.parse_args()

    print(__doc__)
    print("User --help or -h for help.\n\n")

    devIndex = args.target
    devName = f"/dev/video{devIndex}"
    encoder: VideoEncoder = Encoders.get(args.pix_fmt, RGB24encoder)

    print(f"Using encoder {encoder}")

    device = v4l2.fopen(devName, rw=True)
    caps = v4l2.read_info(device.fileno())
    print(f"Device info: {caps}")
    print("Accessed directly:")
    print(f"- physical caps: {caps.device_capabilities}")
    print(f"- capabilities: {caps.capabilities}")
    print(f"- driver: {caps.driver}")

    flags = {
        "french" : FlagVideo(
            "French flag",
            create_vertical_flag(320, 240, (0,0,255), (255,255,255), (255,0,0)),
            width=640,
            height=480
        ),
        "austrian" : FlagVideo(
            "Austrian flag",
            create_horizontal_flag(320, 240, (255,0,0), (255, 255, 255), (255, 0, 0)),
            width=640,
            height=480
        ),
        "swiss" : FlagVideo(
            "Swiss flag",
            create_swiss_flag(size=320),
            width=640,
            height=640
        )
    }
    # Pre-encode
    for movie in flags.values():
        movie.encode(encoder)
#        movie.encode(RGB24encoder)
#        movie.encode(YUV420encoder)
#        movie.encode(YUYVencoder)

    movie = MovieController(device, flags)
#    movie.set_movie("french")

    run(
        movie.run_animation(),
        f = ("French flag", lambda task: movie.set_movie('french', encoder) and False),
        a = ("Austrian flag", lambda task: movie.set_movie('austrian', encoder) and False),
        s = ("Swiss flag", lambda task: movie.set_movie('swiss', encoder) and False),
        q = ("quit", lambda task: task.cancel() or True)
    )
    device.close()
