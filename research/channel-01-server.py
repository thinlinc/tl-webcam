"""
WebRTC signaling channel test #1 (server)

Opens a TCP socket and responds 'Hello' to all incoming messages.
Usage:
   channel-01-server.py [addr][:port]
By default connects to 127.0.0.1:8888
"""

import asyncio
import logging
from utils import addr2hostport

DEFAULT_HOST = ('127.0.0.1', 8888)

async def handle_message(
    reader: asyncio.StreamReader,
    writer: asyncio.StreamWriter
):
    # Create a logger
    addr = addr2hostport(writer.get_extra_info('peername'))
    logger = logging.getLogger(addr)
    logger.info('detected client')
    # Read message
    data = await reader.read(100)
    message = data.decode()
    logger.info(f"> {message}")
    # Respond to the message
    response = f"Hello {message}!"
    logger.info(f"< {response}")
    writer.write(response.encode())
    await writer.drain()
    # Close the connection
    logger.info("closing")
    writer.close()
    await writer.wait_closed()


async def serve(host: str, port: int):
    # Open the requested port
    logger = logging.getLogger('server')
    logger.info(f'Opening {host}:{port}')
    server = await asyncio.start_server(handle_message, host, port)
    # Show served ports
    addrs = ', '.join(
        addr2hostport(sock.getsockname()) for sock in server.sockets
    )
    # Start listening
    async with server:
        logger.info(f"Serving on {addrs}")
        await server.serve_forever()


if __name__ == "__main__":

    from utils import parse_host, run
    from sys import argv

    host, port = DEFAULT_HOST if len(argv) < 2 \
                 else parse_host(argv[1], default=DEFAULT_HOST)

    run(serve(host, port))
