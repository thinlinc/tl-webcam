import av
import v4l2py
import v4l2py.device

def copy_mjpeg(source_file='/dev/video0', target_index=6):

    with av.open(
        file=source_file,
        mode='r',
        format='v4l2',
        options={
            "input_format": "mjpeg",
            "video_size": "640x480"
        }
    ) as camera:
        with v4l2py.Device.from_id(target_index) as target:
            target.set_format(
                v4l2py.device.BufferType.VIDEO_OUTPUT,
                640, 480, "MJPG"
            )
            for packet in camera.demux(camera.streams.video[0]):
                print(f"packet size {packet.size}")
                target.write(packet)


def copy_yuv422(source_file='/dev/video0', target_index=6):

    with av.open(
        file=source_file,
        mode='r',
        format='v4l2',
        options={
            "input_format": "rawvideo",
            "video_size": "640x480"
        }
    ) as camera:
        with v4l2py.Device.from_id(target_index) as target:
            target.set_format(
                v4l2py.device.BufferType.VIDEO_OUTPUT,
                640, 480, "YUYV"
            )
            for frame in camera.decode(camera.streams.video[0]):
                frame_size = sum(plane.buffer_size for plane in frame.planes)
                print(f"{frame.width}x{frame.height} ({frame.format.name}) - {frame_size} in {len(frame.planes)} planes")
                target.write(frame.planes[0])
            

def decode_mjpeg(source_file='/dev/video0', target_index=6):

    codec = av.CodecContext.create('rawvideo', "w")
    codec.width = 640
    codec.height = 480
    codec.pix_fmt = "yuv420p"
#    codec.framerate = fractions.Fraction(30, 1)
#    codec.time_base = fractions.Fraction(1, 30)
    codec.open()

    with av.open(
        file='/dev/video0',
        mode='r',
        format='v4l2',
        options={
            "input_format": "mjpeg",
            "video_size": "1280x720"
        }
    ) as camera:
        with v4l2py.Device.from_id(6) as target:
            target.set_format(
                v4l2py.device.BufferType.VIDEO_OUTPUT,
                640, 480, "YU12"
            )
            for id, frame in enumerate(camera.decode(camera.streams.video[0])):
                frame_size = sum(plane.buffer_size for plane in frame.planes)
                print(f"#{id}: {frame.width}x{frame.height} ({frame.format.name}) - {frame_size} in {len(frame.planes)} planes")
                for packet in codec.encode(frame):
                    target.write(packet)

    codec.close()


if __name__ == "__main__":
    decode_mjpeg()
