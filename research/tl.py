#!/opt/thinlinc/libexec/python3

from sys import argv
from os import getuid
from pwd import getpwuid

from thinlinc import hiveconf as hc, sessioninfo as tls

username = argv[1] if len(argv) > 1 else getpwuid(getuid()).pw_name
print(f"Loading session for f{username}")

#print("Loading configuration", end="...")
conf = hc.open_hive("/opt/thinlinc/etc/thinlinc.hconf")
#print("Done")
#print(conf)

#print("Obtaining session info", end="...")
info = tls.get_public_sessioninfo(conf, username, tls.GPS_DONTCARE, tls.GPS_DONTCARE)
#print("Done")
if len(info) == 0:
    print("No session detected")
else:
    for key, value in info[0].items():
        print(f"  {key}\t{value}")
