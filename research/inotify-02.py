"""
Uses inotify to detect when a video device is accessed. When the access
is very quick then it is assumed that the devices is only probed. Otherwise
it is assumed that an application tried to capture the video stream.
"""

import time
import sys
import asyncio
import glob
from pathlib import Path
from asyncinotify import Inotify, Mask, Event as InotifyEvent

start_time = time.time()

class OpenModeDetector:

    __instance_id: int = 0
    @classmethod
    def instance_id(cls) -> int:
        cls.__instance_id += 1
        return cls.__instance_id

    def __init__(self, event: InotifyEvent, loop: asyncio.AbstractEventLoop = None):
        self.id = self.instance_id()
        self.loop = loop or asyncio.get_event_loop()
        self.opened = 0
        self.streaming = False
        self.event = event
        self.time = 0
        self.start_time = start_time

    def log(self, msg: str):
        t = time.time() - self.start_time
        print(f"[{self.id}@{t:.2f}] {self.event.path}: {msg}")

    def open(self, delay: float = 0.1):
        if self.opened == 0:
            self.log("opened for the first time")
            self.streaming = False
            self.time = self.loop.time()
            self.timer = self.loop.call_later(delay, self.notify_streaming)
        else:
            self.log(f"opened for the {self.opened+1} time")
        self.opened += 1

    def close(self):
        if self.opened == 0:
            self.log("already closed!!!")
            return
        self.opened -= 1
        if self.opened == 0:
            duration = str(int((self.loop.time() - self.time)*1000)).rjust(4, '0')
            duration = duration[:-3] + "." + duration[-3:]
            if self.streaming:
                self.log(f"closed after {duration} seconds")
                self.streaming = False
            elif not self.streaming:
                self.timer.cancel()
                self.log(f"probed for {duration} seconds")
        else:
            self.log(f"closed, still opened {self.opened} times")

    def notify_streaming(self):
        self.streaming = True
        self.log("capturing")


async def main(pattern):
    print(f"Monitoring {pattern}")
    timers = dict()
    loop = asyncio.get_event_loop()
    with Inotify() as inotify:
        for path in glob.glob(pattern):
            print(f"- found {path}")
            inotify.add_watch(path, Mask.OPEN | Mask.CLOSE)
        print(f"Processing events")

        async for event in inotify:
            timer = timers.get(event.path)
            if timer is None:
                timer = OpenModeDetector(event, loop)
                timers[event.path] = timer
            if event.mask == Mask.OPEN:
                timer.open()
            else:
                timer.close()


path = sys.argv[1] if len(sys.argv) > 1 else '/dev/video*'
loop = asyncio.new_event_loop()
try:
    loop.run_until_complete(main(path))
except KeyboardInterrupt:
    print("Shutting down")
    loop.run_until_complete(loop.shutdown_asyncgens())
    loop.close()
