"""
WebRTC streaming test #5 (sender)

source: /dev/video0

"""

import asyncio
import aiortc
import socket
import logging
import re
from typing import Tuple, Optional
from sys import stdout, argv

from aiortc.contrib.media import MediaPlayer

HOST='127.0.0.1'
PORT=8888

SOURCE_VIDEO="/dev/video0"
VIDEO_PARAMETERS = {
    "video_size": "640x480",
    "framerate": "30"
}

ACTION_ERROR = "error"
ACTION_START = "start"
ACTION_STOP = "stop"
ACTION_OFFER = "offer"
ACTION_ANSWER = "answer"


def addr2hostport(addr: Tuple[str,int]) -> str:
    return f"{addr[0]}:{addr[1]}"


class ChannelException(Exception):
    pass

class ChannelClosedError(ChannelException):
    pass

class ChannelAuthenticationFailed(ChannelException):
    pass

class ChannelProtocolError(ChannelException):
    pass

class ChannelIncompleteMessage(ChannelException):
    def __init__(self, expected, read, command):
        super().__init__()
        self.expected = expected
        self.read = read
        self.command = command


class Channel:
    def __init__(
        self,
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter
    ):
        addr = addr2hostport(writer.get_extra_info('peername'))
        self.reader = reader
        self.writer = writer
        self.logger = logging.getLogger(f'{addr[0]}:{addr[1]}')

    @property
    def is_closed(self):
        return self.writer is None or self.writer.is_closing()

    async def close(self):
        if self.is_closed(): return
        writer = self.writer
        self.writer = None
        self.reader = None
        writer.close()
        await writer.wait_closed()
        self.logger.info("x")

    async def __read(self, size: int, initial: bool = False) -> bytes:
        if self.reader is None: raise ChannelClosedError()
        try:
            return await self.reader.readexactly(size)
        except asyncio.IncompleteReadError as err:
            read = len(err.partial)
            if initial and read == 0:
                raise ChannelClosedError()
            else:
                raise ChannelIncompleteMessage(size, read)
        except ConnectionResetError:
            raise ChannelClosedError()
    
    async def recv(self) -> Tuple[str, str]:
        header = await self.__read(8, True)
        if header[0:4] != b'RWP1':
            raise ChannelProtocolError("missing header")
        size = int.from_bytes(header[4:], 'little')
        message = (await self.__read(size)).decode().split(':',1)
        if len(message) < 2:
            raise ChannelProtocolError("missing command")
        command, data = message
        self.logger.info(f'< {command}')
        return command, data

    async def send(self, command: str, data: str):
        self.logger.info(f'> {command}')
        if self.is_closed: raise ChannelClosedError()
        encoded = f"{command}:{data}".encode()
        size = len(encoded)
        self.writer.write(b"RWP1")
        self.writer.write(size.to_bytes(4, 'little'))
        self.writer.write(encoded)
        await self.writer.drain()


class Publisher:

    def __init__(self, source: MediaPlayer):
        self.source = source
        self.rtc = None
        self.logger = logging.getLogger('pub')

    def open(self):
        self.logger.info("opening a connection")
        self.rtc = rtc = aiortc.RTCPeerConnection()

        @rtc.on('connectionstatechange')
        async def on_connectionstatechange():
            self.logger.info("connection state is %s" % rtc.connectionState)
            if rtc.connectionState == "failed":
                await rtc.close()


    async def create_offer(self):
        self.logger.info("adding the video track")
        self.rtc.addTrack(self.source.video)
        self.logger.info("creating an offer")
        offer = await self.rtc.createOffer()

        self.logger.info("setting and returning the local description")
        await self.rtc.setLocalDescription(offer)
        return self.rtc.localDescription

    async def accept_answer(self, answer):
        self.logger.info("accepting an answer")
        await self.rtc.setRemoteDescription(answer)
        self.logger.info("the connection is prepared")

    async def close(self):
        self.source.video.stop()
        await self.rtc.close()


class StreamingProcess:
    def __init__(self, publisher: Publisher, channel: Channel):
        self.__queue = asyncio.Queue()
        self.__task = None
        self.__stop = asyncio.Event()
        self.__stopped = asyncio.Event()
        self.streaming = False
        self.publisher = publisher
        self.channel = channel

    async def pass_message(self, message):
        return await self.__queue.put(message)

    def start(self) -> bool:
        if self.__task is None or self.__task.done():
            self.__task = asyncio.create_task(self.__run())
            return True
        else:
            return False

    async def __run(self):
        try:
            self.__stopped.clear()
            self.publisher.open()
            offer = await self.publisher.create_offer()
            await self.channel.send(ACTION_OFFER, offer.sdp)
            command, data = await self.__queue.get()
            if command != ACTION_ANSWER:
                raise Exception(f"unexpected message: {command}")
            await self.publisher.accept_answer(
                asyncio.RTCSessionDescription(type="answer", sdp=data)
            )
            self.streaming = True
            await self.__stop.wait()
        except asyncio.CancelledError:
            pass
        except Exception:
            pass
        finally:
            self.streaming = False
            await self.publisher.close()
            self.__stopped.set()

    def stop(self):
        if self.streaming:
            self.__stop.set()
        elif not (self.__task is None or self.__task.done()):
            self.__task.cancel()

    async def wait_stopped(self):
        await self.__stopped.wait()



class Server:
    def __init__(
        self,
        publisher: Publisher,
        token: Optional[str] = None
    ):
        self.server = None
        self.publisher = publisher
        self.token: Optional[str] = token
        self.connected = False
        self.logger = logging.getLogger('server')

    async def run(self, host: str, port: int):
        self.server = await asyncio.start_server(self.handle_connection, host, port)
        addrs = ', '.join(addr2hostport(sock.getsockname()) for sock in self.server.sockets)
        self.logger.info(f"serving on {addrs}")

        async with self.server:
            await self.server.serve_forever()

        self.server = None
        self.logger.info(f"stopped serving on {addrs}")
    
    async def handle_connection(self,
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter
    ):
        host, port = writer.get_extra_info('peername')
        self.logger.info(f"incoming connection from {host}:{port}")
        if self.connected:
            self.logger.warning(f"cannot server {host}:{port} - already serving")
        else:
            self.connected = True
            channel = Channel(reader, writer)
            streaming = StreamingProcess(self.publisher, channel)
            try:
                while not channel.is_closed:
                    command, data = await channel.recv()
                    if command == ACTION_START:
                        if not streaming.start():
                            await channel.send(ACTION_ERROR, "already streaming")
                    elif command == ACTION_STOP:
                        streaming.stop()
                    else:
                        await streaming.pass_message((command, data))
            except Exception:
                pass
            self.connected = False
            streaming.stop()
            await streaming.wait_stopped()
        writer.close()
        await writer.wait_closed()


async def main():

    host = HOST
    port = PORT

    # Parse the optional argument
    if len(argv) > 1:

        if argv[1].startswith('-'):
            print(__doc__)
            exit(0)

        addr = argv[1].split(':', 1)
        if addr[0]:
            ip_match = re.match(r"(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})", addr[0])
            if ip_match is None or any((int(n) in range(0,256) for n in ip_match.groups())):
                host = socket.gethostbyname(addr[0])
            else:
                host = addr[0]
        if len(addr) > 1:
            port = int(addr[1])

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    handler = logging.StreamHandler(stdout)
    handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)-6s %(name)-10s %(message)s'))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    server = Server(
        Publisher(MediaPlayer(
            SOURCE_VIDEO,
            format="v4l2",
            options=VIDEO_PARAMETERS
        ))
    )
    await server.run(host, port)



if __name__ == "__main__":
    print(__doc__)
    asyncio.run(main())
