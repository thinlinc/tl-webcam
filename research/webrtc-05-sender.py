"""
WebRTC streaming test #5 (sender)

source: /dev/video0

"""

import asyncio
import aiortc
import socket
import logging
import re
from typing import Tuple, Optional
from sys import stdout, argv

from aiortc.contrib.media import MediaPlayer

HOST='127.0.0.1'
PORT=8888

SOURCE_VIDEO="/dev/video0"
VIDEO_PARAMETERS = {
    "video_size": "640x480",
    "framerate": "30"
}

ACTION_ERROR = "error"
ACTION_START = "start"
ACTION_STOP = "stop"
ACTION_OFFER = "offer"
ACTION_ANSWER = "answer"


def addr2hostport(addr: Tuple[str,int]) -> str:
    return f"{addr[0]}:{addr[1]}"


class ChannelException(Exception):
    pass

class ChannelClosedError(ChannelException):
    pass

class ChannelAuthenticationFailed(ChannelException):
    pass

class ChannelProtocolError(ChannelException):
    pass

class ChannelIncompleteMessage(ChannelException):
    def __init__(self, expected, read, command):
        super().__init__()
        self.expected = expected
        self.read = read
        self.command = command


class Channel:
    def __init__(
        self,
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter
    ):
        addr = addr2hostport(writer.get_extra_info('peername'))
        self.reader = reader
        self.writer = writer
        self.logger = logging.getLogger(f'{addr[0]}:{addr[1]}')

    @property
    def is_closed(self):
        return self.writer is None or self.writer.is_closing()

    async def close(self):
        if self.is_closed: return
        writer = self.writer
        self.writer = None
        self.reader = None
        writer.close()
        await writer.wait_closed()
        self.logger.info("x")

    async def __read(self, size: int, initial: bool = False) -> bytes:
        if self.reader is None: raise ChannelClosedError()
        try:
            return await self.reader.readexactly(size)
        except asyncio.IncompleteReadError as err:
            read = len(err.partial)
            if initial and read == 0:
                raise ChannelClosedError()
            else:
                raise ChannelIncompleteMessage(size, read)
        except ConnectionResetError:
            raise ChannelClosedError()
    
    async def recv(self) -> Tuple[str, str]:
        header = await self.__read(8, True)
        if header[0:4] != b'RWP1':
            raise ChannelProtocolError("missing header")
        size = int.from_bytes(header[4:], 'little')
        message = (await self.__read(size)).decode().split(':',1)
        if len(message) < 2:
            raise ChannelProtocolError("missing command")
        command, data = message
        self.logger.info(f'< {command}')
        return command, data

    async def send(self, command: str, data: str):
        self.logger.info(f'> {command}')
        if self.is_closed: raise ChannelClosedError()
        encoded = f"{command}:{data}".encode()
        size = len(encoded)
        self.writer.write(b"RWP1")
        self.writer.write(size.to_bytes(4, 'little'))
        self.writer.write(encoded)
        await self.writer.drain()


class Publisher:

    def __init__(self, source: MediaPlayer):
        self.source = source
        self.rtc = None
        self.logger = logging.getLogger('pub')

    def open(self):
        self.logger.info("opening a connection")
        self.rtc = rtc = aiortc.RTCPeerConnection()

        @rtc.on('connectionstatechange')
        async def on_connectionstatechange():
            self.logger.info("connection state is %s" % rtc.connectionState)
            if rtc.connectionState == "failed":
                await rtc.close()
        
        @rtc.on('icecandidate')
        async def on_ice_candidate(candidate):
            self.logger.info("Found an ICE candidate")


    async def create_offer(self):
        self.logger.info("adding the video track")
        self.rtc.addTrack(self.source.video)
        self.logger.info("creating an offer")
        offer = await self.rtc.createOffer()

        self.logger.info("setting and returning the local description")
        await self.rtc.setLocalDescription(offer)
        return self.rtc.localDescription

    async def accept_answer(self, answer):
        self.logger.info("accepting an answer")
        await self.rtc.setRemoteDescription(answer)
        self.logger.info("the connection is prepared")

    async def close(self):
        self.source.video.stop()
        await self.rtc.close()



class Server:
    
    def __init__(self, device_path: str, options: dict):
        self.connected = False
        self.device_path = device_path
        self.options = options
        self.logger = logging.getLogger('server')

    async def run(self, host: str, port: int):
        server = await asyncio.start_server(self.handle_connection, host, port)
        addrs = ', '.join(addr2hostport(sock.getsockname()) for sock in server.sockets)
        self.logger.info(f"serving on {addrs}")
        async with server:
            await server.serve_forever()

    async def handle_connection(
        self,
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter
    ):
        host, port = writer.get_extra_info('peername')
        self.logger.info(f"incoming connection from {host}:{port}")
        if self.connected:
            self.logger.warning(f"cannot server {host}:{port} - already serving")
            return
        self.connected = True
        publisher = None
        channel = Channel(reader, writer)
        
        while not channel.is_closed:
            try:
                command, data = await channel.recv()
                if command != ACTION_START:
                    raise Exception(f"unexpected command: {command}")

                publisher = Publisher(MediaPlayer(
                    self.device_path,
                    format="v4l2",
                    options=self.options
                ))
                publisher.open()
                offer = await publisher.create_offer()
                await channel.send(ACTION_OFFER, offer.sdp)

                command, data = await channel.recv()
                if command != ACTION_ANSWER:
                    raise Exception(f"unexpected command: {command}")
                await publisher.accept_answer(
                    aiortc.RTCSessionDescription(data, "answer")
                )

                command, data = await channel.recv()
                if command != ACTION_STOP:
                    raise Exception(f"unexpected command: {command}")

            except ChannelException as err:
                self.logger.error(err)
                await channel.close()
            except Exception as err:
                self.logger.error(err)
            finally:
                if publisher:
                    await publisher.close()
                    publisher = None

        self.logger.info(f"connection from {host}:{port} has been closed")
        self.connected = False


async def main():

    host = HOST
    port = PORT

    # Parse the optional argument
    if len(argv) > 1:

        if argv[1].startswith('-'):
            print(__doc__)
            exit(0)

        addr = argv[1].split(':', 1)
        if addr[0]:
            ip_match = re.match(r"(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})", addr[0])
            if ip_match is None or any((int(n) in range(0,256) for n in ip_match.groups())):
                host = socket.gethostbyname(addr[0])
            else:
                host = addr[0]
        if len(addr) > 1:
            port = int(addr[1])

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    handler = logging.StreamHandler(stdout)
    handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)-7s %(name)-10s %(message)s'))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    server = Server(SOURCE_VIDEO, VIDEO_PARAMETERS)
    await server.run(host, port)


if __name__ == "__main__":
    print(__doc__)
    asyncio.run(main())
