"""
WebRTC signaling channel test #2 (server)

Opens a TCP socket and responds 'Hello' to all incoming messages.
The messages are encoded following a simple protocol that provides
the size of the encoded message.

Usage:
   channel-02-server.py [addr][:port]
By default connects to 127.0.0.1:8888
"""

import asyncio
import logging
from typing import Tuple

from utils import addr2hostport

DEFAULT_HOST = ('127.0.0.1', 8888)

async def recv(reader: asyncio.StreamReader) -> Tuple[str, int]:
    read = 0
    header = b''
    try:
        header = await reader.readexactly(4)
        read = 4
        if header != b'RWP1': return repr(header), -2
        size = int.from_bytes(await reader.readexactly(4), 'little')
    except asyncio.IncompleteReadError as err:
        read += len(err.partial)
        return ('', 0) if read == 0 else (header + err.partial, -1)
    return (await reader.readexactly(size)).decode(), size

def send(writer: asyncio.StreamWriter, message: str):
    encoded = message.encode()
    size = len(encoded)
    writer.write(b"RWP1")
    writer.write(size.to_bytes(4, 'little'))
    writer.write(encoded)

async def handle_message(
    reader: asyncio.StreamReader,
    writer: asyncio.StreamWriter
):
    # Create a logger
    logger = logging.getLogger(
        addr2hostport(writer.get_extra_info('peername'))
    )
    logger.info('detected client')
    while True:
        try:
            # Read message
            message, size = await recv(reader)
        except ConnectionResetError:
            size = 0
        except Exception as e:
            logger.error(e)
            size = -3
        
        if size > 0:
            logger.info(f"> {message}")        
            # Respond to the message
            response = f"Hello {message}!"
            logger.info(f"< {response}")
            try:
                send(writer, response)
                await writer.drain()        # This may raise
            except ConnectionResetError:
                logger.error('connection lost while sending the response')
                return
            except Exception as err:
                logger.exception(err)
                break
        elif size == 0:
            break
        elif size == -1:
            logger.warning(f"protocol error: incomplete header {message}")
            break
        elif size == -2:
            logger.warning(f"protocol error: header {message} instead of b'RWP1'")
            break

    # Close the connection
    if not writer.is_closing():
        logger.info("closing")
        writer.close()
    await writer.wait_closed()
    logger.info("closed")


async def serve(host: str, port: int):
    # Open the requested port
    logger = logging.getLogger('server')
    logger.info(f'Opening {host}:{port}')
    server = await asyncio.start_server(handle_message, host, port)
    # Show served ports
    addrs = ', '.join(
        addr2hostport(sock.getsockname()) for sock in server.sockets
    )
    # Start listening
    async with server:
        logger.info(f"Serving on {addrs}")
        await server.serve_forever()


if __name__ == "__main__":

    from utils import parse_host, run
    from sys import argv

    host, port = DEFAULT_HOST if len(argv) < 2 \
                 else parse_host(argv[1], default=DEFAULT_HOST)

    run(serve(host, port))
