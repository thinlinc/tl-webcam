import asyncio
import logging
from random import random
from typing import Set, Optional, Tuple

class Service:
    def __init__(self, name: str = ""):
        self.name = name
        self.task: Optional[asyncio.Task] = None

    @property
    def running(self) -> bool:
        return bool(self.task) and not self.task.done()

    async def set_up(self):
        pass

    async def run(self):
        pass

    async def tear_down(self):
        pass

    async def start(self):
        if self.running: return
        await self.set_up()
        self.task = asyncio.create_task(self.run())

    async def stop(self):
        if not self.running: return
        try:
            # Putting inside try-finally blocks ensures that the task
            # is cancelled in case tear_down() raises an exception.
            # The exception bubbles then outside.
            await self.tear_down()
        finally:
            task = self.task
            self.task = None
            if not task.done(): task.cancel()
            try:
                await task
            except BaseException:
                pass



class App:
    def __init__(self):
        self.services: Set[Service] = set()
        self.running = False
        self.logger = logging.getLogger('app')
    
    def add_service(self, service: Service):
        self.services.add(service)
        if self.running: service.start()

    async def start(self):

        async def start_service(service: Service):
            try:
                self.logger.info(f"Starting service '{service.name}'")
                await service.start()
                self.logger.info(f"Service '{service.name}' has started")
            except Exception as err:
                self.logger.exception(f"Service '{service.name}' failed to start: {err}", exc_info=err)

        if self.running: return
        self.running = True
        await asyncio.gather(
            *(start_service(service) for service in self.services)
        )

    async def run(self, time: int):
        while time > 0:
            print("x", end="", flush=True)
            await asyncio.sleep(1)
            time -= 1

    async def stop(self):

        async def stop_service(service: Service):
            try:
                self.logger.info(f"Stopping service '{service.name}'")
                await service.stop()
                self.logger.info(f"Service '{service.name}' has been stopped")
            except Exception as err:
                self.logger.exception(f"Service '{service.name}' returned a failure on stop: {err}", exc_info=err)

        if not self.running: return
        self.running = False
        await asyncio.gather(
            *(stop_service(service) for service in self.services)
        )


# Testing part

class CustomService(Service):
    def __init__(self,
                 name: str,
                 fails_after: int = 0,
                 fails_on_start: bool = False,
                 fails_on_stop: bool = False):
        super().__init__(name)
        self.fail_time = fails_after
        self.fails_on_start = fails_on_start
        self.fails_on_stop = fails_on_stop
    
    async def set_up(self):
        await asyncio.sleep(random())
        if self.fails_on_start: raise Exception("I don't want to start")

    async def tear_down(self):
        await asyncio.sleep(random())
        if self.fails_on_stop: raise Exception("I don't want to stop")

    async def run(self):
        if self.fail_time:
            await asyncio.sleep(self.fail_time)
            print(f"Quitting! [{self.name}]")
            raise Exception("I'm tired!")
        else:
            loop = asyncio.get_event_loop()
            blocker = loop.create_future()
            await blocker


if __name__ == "__main__":

    from utils import create_logger

    create_logger()

    services: Tuple[Service] = (
        CustomService("Nice service"),
        CustomService("Lazy service", fails_after=2.5),
        CustomService("Sleepy service", fails_on_start=True),
        CustomService("Overeager service", fails_on_stop=True)
    )

    def check_services_running(*expected_states):
        print("Checking the running state of services:")
        for service, state in zip(services, expected_states):
            if (state != service.running):
                current = "running" if service.running else "stopped"
                expected = "running" if state else "stopped"
                print(f"\t{service.name} is {current} while expected to be {expected}")

    app = App()
    for service in services:
        app.add_service(service)
    check_services_running(False, False, False, False)

    async def main():
        print("\n*** Starting the application ***")
        await app.start()
        check_services_running(True, True, False, True)

        print("\n*** Running the application for 5 seconds ***")
        await app.run(5)
        check_services_running(True, False, False, True)
        
        print("\n*** Stopping the application ***")
        await app.stop()
        check_services_running(False, False, False, False)
        print("\n*** Finished! ***")

    asyncio.run(main())