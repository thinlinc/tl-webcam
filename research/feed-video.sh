#!/bin/bash

DEVICE_NR=11
if [ "$1" != "" ]; then
  DEVICE_NR="$1"
fi

echo "/dev/video0 -> /dev/video$DEVICE_NR"

ffmpeg -framerate 30 -input_format mjpeg -f v4l2 -i /dev/video0 -vcodec rawvideo -pix_fmt yuv420p -f v4l2 "/dev/video$DEVICE_NR" -hide_banner


